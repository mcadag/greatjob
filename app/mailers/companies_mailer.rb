class CompaniesMailer < ActionMailer::Base
  default from: "famous.dennis@famous.ph"

  def invite(user, deny, accept, comapny_name, creator)
    @user = user
    @deny = deny
    @accept = accept
    @comapny_name = comapny_name
    @creator = creator
    mail(:to => "#{user.name} <#{user.email}>", :subject => "You have been invited to #{comapny_name}.")
  end
end
