require "spec_helper"

describe StoredTestimoniesController do
  describe "routing" do

    it "routes to #index" do
      get("/stored_testimonies").should route_to("stored_testimonies#index")
    end

    it "routes to #new" do
      get("/stored_testimonies/new").should route_to("stored_testimonies#new")
    end

    it "routes to #show" do
      get("/stored_testimonies/1").should route_to("stored_testimonies#show", :id => "1")
    end

    it "routes to #edit" do
      get("/stored_testimonies/1/edit").should route_to("stored_testimonies#edit", :id => "1")
    end

    it "routes to #create" do
      post("/stored_testimonies").should route_to("stored_testimonies#create")
    end

    it "routes to #update" do
      put("/stored_testimonies/1").should route_to("stored_testimonies#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/stored_testimonies/1").should route_to("stored_testimonies#destroy", :id => "1")
    end

  end
end
