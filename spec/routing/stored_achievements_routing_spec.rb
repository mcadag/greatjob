require "spec_helper"

describe StoredAchievementsController do
  describe "routing" do

    it "routes to #index" do
      get("/stored_achievements").should route_to("stored_achievements#index")
    end

    it "routes to #new" do
      get("/stored_achievements/new").should route_to("stored_achievements#new")
    end

    it "routes to #show" do
      get("/stored_achievements/1").should route_to("stored_achievements#show", :id => "1")
    end

    it "routes to #edit" do
      get("/stored_achievements/1/edit").should route_to("stored_achievements#edit", :id => "1")
    end

    it "routes to #create" do
      post("/stored_achievements").should route_to("stored_achievements#create")
    end

    it "routes to #update" do
      put("/stored_achievements/1").should route_to("stored_achievements#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/stored_achievements/1").should route_to("stored_achievements#destroy", :id => "1")
    end

  end
end
