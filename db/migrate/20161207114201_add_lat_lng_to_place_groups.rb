class AddLatLngToPlaceGroups < ActiveRecord::Migration
  def change
    add_column :place_groups, :lat, :float
    add_column :place_groups, :lng, :float
  end
end
