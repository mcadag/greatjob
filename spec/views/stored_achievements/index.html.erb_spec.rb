require 'spec_helper'

describe "stored_achievements/index" do
  before(:each) do
    assign(:stored_achievements, [
      stub_model(StoredAchievement,
        :name => "Name"
      ),
      stub_model(StoredAchievement,
        :name => "Name"
      )
    ])
  end

  it "renders a list of stored_achievements" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
