class Video < ActiveRecord::Base
	attr_accessible :payload, :user_id, :resource_type, :resource_id, 
					:name, :description, :comments_count, :views_count, 
					:like_count, :stars, :title, :created_by
	acts_as_likeable
	belongs_to :user
    has_one :nominee
	belongs_to :interest
	belongs_to :award
	has_many :testimony
	belongs_to :goal
	has_many :comments
	belongs_to :competition

	has_attached_file :payload,
		:storage => :s3,
		:s3_permissions => :private,
		:s3_credentials => "#{Rails.root}/config/aws.yml",
		:styles => {
    		:mobile => {:geometry => "400x300", 
    					:format => 'mp4'},
    		:thumb => { :geometry => "400x400",
    					:format => 'jpg', 
    					:time => 1, 
    					:convert_options => "-auto-orient"}
  		}, 
  		:processors => [:ffmpeg],
  		:use_timestamp => false
	do_not_validate_attachment_file_type :payload

	def payload_url
		payload.url(:original)
	end

	def payload_thumb_url
		payload.url(:thumb)
	end

	def payload_mobile_url
		payload.url(:mobile)
	end

	def payload_swf_url
		payload.url(:swf)
	end

	def comments_count
		videoComments = CompetitionVideoComment.where(video_id: self.id)
	    comments = Comment.where(video_id: self.id)
	    return videoComments.count + comments.count
	end

	def like_count 
  		likers_count = Like.where(:likeable_type => 'Video' , :likeable_id => self.id ).count
  		return likers_count
	end 

	scope :allowed, where(:permission => 'allowed')
	scope :latest, order('created_at DESC')
	scope :my, ->(*args) { where(:user_id => args.first) }
	scope :testimony, -> (*args) { where(:resource_id => args.first, :resource_type => 'testimony')}
	scope :interests, -> (*args) { where(:resource_id => args.first, :resource_type => 'skill')}
	scope :awards, -> (*args) { where(:resource_id => args.first, :resource_type => 'achievement')}

end
