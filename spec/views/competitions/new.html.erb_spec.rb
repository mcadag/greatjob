require 'spec_helper'

describe "competitions/new" do
  before(:each) do
    assign(:competition, stub_model(Competition,
      :name => "MyString",
      :user_id => 1,
      :company_id => 1,
      :description => "MyString",
      :prize_id => 1
    ).as_new_record)
  end

  it "renders new competition form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", competitions_path, "post" do
      assert_select "input#competition_name[name=?]", "competition[name]"
      assert_select "input#competition_user_id[name=?]", "competition[user_id]"
      assert_select "input#competition_company_id[name=?]", "competition[company_id]"
      assert_select "input#competition_description[name=?]", "competition[description]"
      assert_select "input#competition_prize_id[name=?]", "competition[prize_id]"
    end
  end
end
