class AddActivityRefToComments < ActiveRecord::Migration
  def change
     add_column :comments, :activity_id, :integer , foreign_key: true
     add_index :comments, :activity_id
  end
end
