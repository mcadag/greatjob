class RemoveDatesFromStoredUsers < ActiveRecord::Migration
  def up
  	remove_column :stored_users, :training_start
  	remove_column :stored_users, :training_end
  end

  def down
  end
end
