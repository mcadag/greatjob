require "spec_helper"

describe CompetitionVideoCommentsController do
  describe "routing" do

    it "routes to #index" do
      get("/competition_video_comments").should route_to("competition_video_comments#index")
    end

    it "routes to #new" do
      get("/competition_video_comments/new").should route_to("competition_video_comments#new")
    end

    it "routes to #show" do
      get("/competition_video_comments/1").should route_to("competition_video_comments#show", :id => "1")
    end

    it "routes to #edit" do
      get("/competition_video_comments/1/edit").should route_to("competition_video_comments#edit", :id => "1")
    end

    it "routes to #create" do
      post("/competition_video_comments").should route_to("competition_video_comments#create")
    end

    it "routes to #update" do
      put("/competition_video_comments/1").should route_to("competition_video_comments#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/competition_video_comments/1").should route_to("competition_video_comments#destroy", :id => "1")
    end

  end
end
