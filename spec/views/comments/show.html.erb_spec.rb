require 'spec_helper'

describe "comments/show" do
  before(:each) do
    @comment = assign(:comment, stub_model(Comment,
      :user_id => 1,
      :nominee_id => 2,
      :video_id => 3,
      :message => "Message",
      :details => "Details",
      :description => "Description",
      :approved => false,
      :private => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/Message/)
    rendered.should match(/Details/)
    rendered.should match(/Description/)
    rendered.should match(/false/)
    rendered.should match(/false/)
  end
end
