class ChangeNullToNotNullInVideos < ActiveRecord::Migration
  def up
  	change_column :videos, :payload_file_name,    :string,   :null => false
    change_column :videos, :payload_file_size,    :integer,  :null => false
    change_column :videos, :payload_content_type, :string,   :null => false
    change_column :videos, :payload_updated_at,   :datetime, :null => false    
  end

  def down
  end
end
