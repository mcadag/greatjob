
class PrizesController < ApplicationController
  before_filter :set_prize, only: [:show, :edit, :update, :destroy]
  respond_to :json

  def index
    @prizes = Prize.all
    respond_with(@prizes)
  end

  def show
    respond_with(@prize)
  end

  def new
    @prize = Prize.new
    respond_with(@prize)
  end

  def edit
  end

  def create
    @prize = Prize.new(params.require(:prize).permit!)
    @prize.save
    respond_with(@prize)
  end

  def update
    @prize.update_attributes(params.require(:prize).permit!)
    respond_with(@prize)
  end

  def destroy
    @prize.destroy
    respond_with(@prize)
  end

  private
    def set_prize
      @prize = Prize.find(params[:id])
    end
end
