class Award < ActiveRecord::Base
	include PublicActivity::Model
  tracked
	belongs_to :user
  	attr_accessible :description, :name, :user_id, :video_id, :permission
  	
  	scope :allowed, where(:permission => 'allowed')
	scope :latest, order('created_at DESC')
	scope :my, ->(*args) { where(:user_id => args.first) }
end
