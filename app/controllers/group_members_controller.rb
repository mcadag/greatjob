class GroupMembersController < ApplicationController

  has_scope :place_group_id

  # GET /group_members
  # GET /group_members.json
  def index
    @group_members = apply_scopes(GroupMember).all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @group_members }
    end
  end

  def group_action
    
    @group_member = GroupMember.where(user_id: current_user.id, place_group_id: params[:place_group_id]).first
    
    if @group_member.present?
        @group_member.destroy
        puts "destroying"
    else
      @group_member = GroupMember.new(user_id: current_user.id, place_group_id: params[:place_group_id])
      @group_member.save!
      puts "saving"
    end

    @place_group = PlaceGroup.find(params[:place_group_id])
    
    respond_to do |format|
      format.js
    end

  end

  def confirm
    @group_member = GroupMember.find(params[:id])
    respond_to do |format|
        if @group_member.update_attributes!(confirmed: true)
          format.html # new.html.erb
          format.js
          format.json { render json: @group_member }
        end
    end
  end

  # GET /group_members/1
  # GET /group_members/1.json
  def show
    @group_member = GroupMember.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @group_member }
    end
  end

  # GET /group_members/new
  # GET /group_members/new.json
  def new
    @group_member = GroupMember.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @group_member }
    end
  end

  # GET /group_members/1/edit
  def edit
    @group_member = GroupMember.find(params[:id])
  end

  # POST /group_members
  # POST /group_members.json
  def create
    @group_member = GroupMember.new(group_member_params)
    @place_group = @group_member.place_group
    respond_to do |format|
      if @group_member.save
        format.html { redirect_to @group_member, notice: 'Group member was successfully created.' }
        format.js
        format.json { render json: @group_member, status: :created, location: @group_member }
      else
        format.html { render action: "new" }
        format.js 
        format.json { render json: @group_member.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /group_members/1
  # PATCH/PUT /group_members/1.json
  def update
    @group_member = GroupMember.find(params[:id])

    respond_to do |format|
      if @group_member.update_attributes(group_member_params)
        format.html { redirect_to @group_member, notice: 'Group member was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @group_member.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /group_members/1
  # DELETE /group_members/1.json
  def destroy
    @group_member = GroupMember.find(params[:id])
    @place_group = @group_member.place_group
    @group_member.destroy

    respond_to do |format|
      format.js
      format.html { redirect_to group_members_url }
      format.json { head :no_content }
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def group_member_params
      params.require(:group_member).permit(:place_group, :user, :place_group_id, :user_id, :confirmed)
    end
end
