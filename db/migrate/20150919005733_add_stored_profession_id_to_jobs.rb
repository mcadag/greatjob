class AddStoredProfessionIdToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :stored_profession_id, :integer
    add_index :jobs, :stored_profession_id
  end
end
