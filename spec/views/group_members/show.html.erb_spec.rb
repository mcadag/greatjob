require 'spec_helper'

describe "group_members/show" do
  before(:each) do
    @group_member = assign(:group_member, stub_model(GroupMember,
      :place_group => nil,
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
  end
end
