class CreateCompanyTestimonies < ActiveRecord::Migration
  def change
    create_table :company_testimonies do |t|
      t.string :name
      t.string :description
      t.integer :user_id
      t.integer :company_id
      t.integer :video_id
      t.integer :created_by
      t.boolean :allowed
      t.boolean :private
      t.string :permission

      t.timestamps
    end
  end
end
