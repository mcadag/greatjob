class AddVideoToComments < ActiveRecord::Migration
  def change
  	add_attachment :comments, :video
  end
end
