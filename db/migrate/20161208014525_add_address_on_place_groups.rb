class AddAddressOnPlaceGroups < ActiveRecord::Migration
   def change
    add_column :place_groups, :address, :string
  end
end
