require "spec_helper"

describe GroupMembersController do
  describe "routing" do

    it "routes to #index" do
      get("/group_members").should route_to("group_members#index")
    end

    it "routes to #new" do
      get("/group_members/new").should route_to("group_members#new")
    end

    it "routes to #show" do
      get("/group_members/1").should route_to("group_members#show", :id => "1")
    end

    it "routes to #edit" do
      get("/group_members/1/edit").should route_to("group_members#edit", :id => "1")
    end

    it "routes to #create" do
      post("/group_members").should route_to("group_members#create")
    end

    it "routes to #update" do
      put("/group_members/1").should route_to("group_members#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/group_members/1").should route_to("group_members#destroy", :id => "1")
    end

  end
end
