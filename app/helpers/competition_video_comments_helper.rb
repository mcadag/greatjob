module CompetitionVideoCommentsHelper

	def competitionVideoComments(video_id)
		competition_comments = CompetitionVideoComment.where(video_id: video_id).order('created_at DESC')
		comments = []
		competition_comments.each do |comment|
      		user = User.where(id: comment.user_id)
      		if user.count >= 1
        		comment[:user] = user.as_json(:methods => [:avatar_url], :only => [:name, :id])
        		comment[:video_url] = :video_url
        		comment[:video_thumb_url] = :video_thumb_url
        		comment[:video_mobile_url] = :video_mobile_url
        		comments << comment          
      		end
    	end
    	return comments
	end
end
