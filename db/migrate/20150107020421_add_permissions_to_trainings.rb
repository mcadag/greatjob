class AddPermissionsToTrainings < ActiveRecord::Migration
  def change
  	add_column :trainings, :permission, :string
  	add_column :awards, :permission, :string
  	add_column :goals, :permission, :string
  	add_column :interests, :permission, :string
  	add_column :testimonies, :permission, :string
  	add_column :professions, :permission, :string
  end
end
