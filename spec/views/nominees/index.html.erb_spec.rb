require 'spec_helper'

describe "nominees/index" do
  before(:each) do
    assign(:nominees, [
      stub_model(Nominee,
        :user_id => 1,
        :competition_id => 2,
        :video_id => 3
      ),
      stub_model(Nominee,
        :user_id => 1,
        :competition_id => 2,
        :video_id => 3
      )
    ])
  end

  it "renders a list of nominees" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
