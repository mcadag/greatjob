class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :description
      t.string :type
      t.string :name

      t.timestamps
    end
    add_attachment :documents, :payload
  end
end
