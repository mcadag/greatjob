class Activity < PublicActivity::Activity

  	has_many :comments
    has_one :video
    has_many :likes
  	attr_accessible
    acts_as_likeable

  	def comment_count
  		if self.trackable_type == 'Testimony'
        testimony = Testimony.where(id: self.trackable_id).first
  			videoComments = Comment.where(video_id: testimony.video_id).count
  			return videoComments
      end
  	end

    def liker_count
      return self.likers(User).count
    end

 #  	def comments_count
	# 	videoComments = CompetitionVideoComment.where(video_id: self.id)
	#     comments = Comment.where(video_id: self.id)
	#     return videoComments.count + comments.count
	# end

end
