class AddPlaceGroupToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :place_group_id, :integer , foreign_key: true
    add_index :activities, :place_group_id
  end
end
