require 'spec_helper'

describe "jobs/show" do
  before(:each) do
    @job = assign(:job, stub_model(Job,
      :position => "Position",
      :company_id => 1,
      :details => "Details",
      :user_id => 2,
      :description => "Description",
      :avatar => "",
      :poster => "",
      :banner => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Position/)
    rendered.should match(/1/)
    rendered.should match(/Details/)
    rendered.should match(/2/)
    rendered.should match(/Description/)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
  end
end
