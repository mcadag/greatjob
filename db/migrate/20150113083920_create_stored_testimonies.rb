class CreateStoredTestimonies < ActiveRecord::Migration
  def change
    create_table :stored_testimonies do |t|
      t.string :name

      t.timestamps
    end
  end
end
