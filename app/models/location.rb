class Location < ActiveRecord::Base
  attr_accessible :address, :city, :lat, :lon, :name, :town
  belongs_to :job
end
