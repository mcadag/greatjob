class CreateFaqs < ActiveRecord::Migration
  def change
    create_table :faqs do |t|
      t.string :category
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
