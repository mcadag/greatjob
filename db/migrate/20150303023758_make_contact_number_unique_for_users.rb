class MakeContactNumberUniqueForUsers < ActiveRecord::Migration
  def up
  	change_column :users, :contact_number, :string, :unique => true
  end

  def down
  end
end
