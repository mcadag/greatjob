class CreateCompanyCultures < ActiveRecord::Migration
  def change
    create_table :company_cultures do |t|
      t.string :name
      t.integer :company_id
      t.integer :user_id

      t.timestamps
    end
  end
end
