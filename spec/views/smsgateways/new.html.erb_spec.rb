require 'spec_helper'

describe "smsgateways/new" do
  before(:each) do
    assign(:smsgateway, stub_model(Smsgateway).as_new_record)
  end

  it "renders new smsgateway form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", smsgateways_path, "post" do
    end
  end
end
