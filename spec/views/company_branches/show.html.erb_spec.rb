require 'spec_helper'

describe "company_branches/show" do
  before(:each) do
    @company_branch = assign(:company_branch, stub_model(CompanyBranch,
      :name => "Name",
      :comapny_id => 1,
      :branch_id => 2,
      :user_id => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/3/)
  end
end
