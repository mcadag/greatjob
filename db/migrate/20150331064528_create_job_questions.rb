class CreateJobQuestions < ActiveRecord::Migration
  def change
    create_table :job_questions do |t|
      t.string :question
      t.integer :job_id

      t.timestamps
    end
  end
end
