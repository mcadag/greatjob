class AddStoredCompanyCultureIdToCompanyCulture < ActiveRecord::Migration
  def change
  	add_column :company_cultures, :stored_company_culture_id, :integer
  end
end
