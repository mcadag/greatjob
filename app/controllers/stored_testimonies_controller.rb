
class StoredTestimoniesController < ApplicationController
  # GET /stored_testimonies
  # GET /stored_testimonies.json
  def index
    @stored_testimonies = StoredTestimony.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stored_testimonies }
    end
  end

  # GET /stored_testimonies/1
  # GET /stored_testimonies/1.json
  def show
    @stored_testimony = StoredTestimony.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stored_testimony }
    end
  end

  # GET /stored_testimonies/new
  # GET /stored_testimonies/new.json
  def new
    @stored_testimony = StoredTestimony.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stored_testimony }
    end
  end

  # GET /stored_testimonies/1/edit
  def edit
    @stored_testimony = StoredTestimony.find(params[:id])
  end

  # POST /stored_testimonies
  # POST /stored_testimonies.json
  def create
    @stored_testimony = StoredTestimony.new(stored_testimony_params)

    respond_to do |format|
      if @stored_testimony.save
        format.html { redirect_to @stored_testimony, notice: 'Stored testimony was successfully created.' }
        format.json { render json: @stored_testimony, status: :created, location: @stored_testimony }
      else
        format.html { render action: "new" }
        format.json { render json: @stored_testimony.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stored_testimonies/1
  # PATCH/PUT /stored_testimonies/1.json
  def update
    @stored_testimony = StoredTestimony.find(params[:id])

    respond_to do |format|
      if @stored_testimony.update_attributes(stored_testimony_params)
        format.html { redirect_to @stored_testimony, notice: 'Stored testimony was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stored_testimony.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stored_testimonies/1
  # DELETE /stored_testimonies/1.json
  def destroy
    @stored_testimony = StoredTestimony.find(params[:id])
    @stored_testimony.destroy

    respond_to do |format|
      format.html { redirect_to stored_testimonies_url }
      format.json { head :no_content }
    end
  end

  def search
    @stored_testimony = StoredTestimony.where("name LIKE ?", "%"+params[:q]+"%").all
    if @stored_testimony.count >= 1
      render json: @stored_testimony.as_json(:only => [:name])
    else
      render :json => {:message => "No testimony Found with params:" + params[:q], :status => 404}
    end
  end


  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def stored_testimony_params
      params.require(:stored_testimony).permit(:name)
    end
end
