class AdjectivesController < ApplicationController
  # GET /adjectives
  # GET /adjectives.json
  def index
    @adjectives = Adjective.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @adjectives }
    end
  end

  # GET /adjectives/1
  # GET /adjectives/1.json
  def show
    @adjective = Adjective.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @adjective }
    end
  end

  # GET /adjectives/new
  # GET /adjectives/new.json
  def new
    @adjective = Adjective.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @adjective }
    end
  end

  # GET /adjectives/1/edit
  def edit
    @adjective = Adjective.find(params[:id])
  end

  # POST /adjectives
  # POST /adjectives.json
  def create
    @adjective = Adjective.new(adjective_params)

    respond_to do |format|
      if @adjective.save
        format.html { redirect_to @adjective, notice: 'Adjective was successfully created.' }
        format.json { render json: @adjective, status: :created, location: @adjective }
      else
        format.html { render action: "new" }
        format.json { render json: @adjective.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /adjectives/1
  # PATCH/PUT /adjectives/1.json
  def update
    @adjective = Adjective.find(params[:id])

    respond_to do |format|
      if @adjective.update_attributes(adjective_params)
        format.html { redirect_to @adjective, notice: 'Adjective was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @adjective.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adjectives/1
  # DELETE /adjectives/1.json
  def destroy
    @adjective = Adjective.find(params[:id])
    @adjective.destroy

    respond_to do |format|
      format.html { redirect_to adjectives_url }
      format.json { head :no_content }
    end
  end

  def search
    @adjectives = Adjective.where("name LIKE ?", "%"+params[:q]+"%").all
    if @adjectives.count >= 1
      render json: @adjectives.as_json(:only => [:name])
    else
      render :json => {:message => "No Adjectives Found with params:" + params[:q], :status => 200}
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def adjective_params
      params.require(:adjective).permit()
    end
end