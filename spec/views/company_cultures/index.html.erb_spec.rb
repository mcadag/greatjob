require 'spec_helper'

describe "company_cultures/index" do
  before(:each) do
    assign(:company_cultures, [
      stub_model(CompanyCulture,
        :name => "Name",
        :company_id => 1,
        :user_id => 2
      ),
      stub_model(CompanyCulture,
        :name => "Name",
        :company_id => 1,
        :user_id => 2
      )
    ])
  end

  it "renders a list of company_cultures" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
