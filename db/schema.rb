# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20161212011800) do

  create_table "activities", :force => true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.boolean  "is_viewed",      :default => false
    t.integer  "comment_count"
    t.integer  "liker_count"
    t.integer  "place_group_id"
  end

  add_index "activities", ["owner_id", "owner_type"], :name => "index_activities_on_owner_id_and_owner_type"
  add_index "activities", ["place_group_id"], :name => "index_activities_on_place_group_id"
  add_index "activities", ["recipient_id", "recipient_type"], :name => "index_activities_on_recipient_id_and_recipient_type"
  add_index "activities", ["trackable_id", "trackable_type"], :name => "index_activities_on_trackable_id_and_trackable_type"

  create_table "adjectives", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "applicants", :force => true do |t|
    t.integer  "user_id"
    t.integer  "job_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "awards", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.integer  "video_id"
    t.string   "description"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "permission"
    t.integer  "created_by_id"
  end

  create_table "candidate_pre_qualification_resources", :force => true do |t|
    t.integer  "candidate_id"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.integer  "promoted_resource_id"
    t.string   "promoted_resource_type"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "candidate_proggresses", :force => true do |t|
    t.boolean  "apply",             :default => false
    t.boolean  "initial_interview", :default => false
    t.boolean  "shotlisted",        :default => false
    t.boolean  "final_interview",   :default => false
    t.boolean  "hired",             :default => false
    t.boolean  "rejected",          :default => false
    t.integer  "cancidate_id"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
  end

  create_table "candidates", :force => true do |t|
    t.integer  "user_id"
    t.integer  "job_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "application_type"
  end

  create_table "comments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "nominee_id"
    t.integer  "video_id"
    t.string   "message"
    t.string   "details"
    t.string   "description"
    t.boolean  "approved"
    t.boolean  "private"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.boolean  "has_video"
    t.integer  "candidate_id"
    t.integer  "company_id"
    t.integer  "activity_id"
  end

  add_index "comments", ["activity_id"], :name => "index_comments_on_activity_id"
  add_index "comments", ["candidate_id"], :name => "index_comments_on_candidate_id"
  add_index "comments", ["company_id"], :name => "index_comments_on_company_id"

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.string   "details"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "payload_file_name"
    t.string   "payload_content_type"
    t.integer  "payload_file_size"
    t.datetime "payload_updated_at"
    t.string   "location"
    t.string   "website"
    t.string   "email"
    t.string   "mobile_number"
    t.boolean  "is_corporate"
  end

  create_table "company_branches", :force => true do |t|
    t.string   "name"
    t.integer  "company_id"
    t.integer  "branch_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "company_cultures", :force => true do |t|
    t.integer  "company_id"
    t.integer  "user_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.integer  "stored_company_culture_id"
  end

  create_table "company_dream_jobs", :force => true do |t|
    t.string   "name"
    t.integer  "company_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "company_employees", :force => true do |t|
    t.integer  "user_id"
    t.integer  "company_id"
    t.boolean  "current"
    t.boolean  "verified"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "company_testimonies", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "user_id"
    t.integer  "company_id"
    t.integer  "video_id"
    t.integer  "created_by"
    t.boolean  "allowed"
    t.boolean  "private"
    t.string   "permission"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "competition_qualifications", :force => true do |t|
    t.string   "name"
    t.integer  "competition_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "competition_video_comments", :force => true do |t|
    t.string   "name"
    t.integer  "competition_id"
    t.integer  "user_id"
    t.string   "message"
    t.integer  "nominee_id"
    t.string   "details"
    t.string   "description"
    t.boolean  "approved"
    t.boolean  "is_private"
    t.boolean  "has_video"
    t.integer  "video_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "payload_file_name"
    t.string   "payload_content_type"
    t.integer  "payload_file_size"
    t.datetime "payload_updated_at"
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
  end

  create_table "competitions", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.integer  "company_id"
    t.string   "description"
    t.integer  "prize_id"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "year"
    t.string   "location"
    t.string   "superlative"
    t.string   "poster_file_name"
    t.string   "poster_content_type"
    t.integer  "poster_file_size"
    t.datetime "poster_updated_at"
    t.boolean  "is_private"
  end

  create_table "counters", :force => true do |t|
    t.integer  "nominee_id"
    t.integer  "votes"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "devices", :force => true do |t|
    t.integer  "user_id"
    t.string   "unique_identifier"
    t.string   "platform"
    t.boolean  "active"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "unique_id"
  end

  create_table "documents", :force => true do |t|
    t.string   "description"
    t.string   "category"
    t.string   "name"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "payload_file_name"
    t.string   "payload_content_type"
    t.integer  "payload_file_size"
    t.datetime "payload_updated_at"
    t.integer  "user_id"
    t.integer  "uploaded_by"
    t.boolean  "is_public"
    t.string   "permissions"
    t.string   "poster_file_name"
    t.string   "poster_content_type"
    t.integer  "poster_file_size"
    t.datetime "poster_updated_at"
  end

  create_table "educational_attainments", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "faqs", :force => true do |t|
    t.string   "category"
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "features", :force => true do |t|
    t.string   "name"
    t.string   "feature_type"
    t.text     "content"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "follows", :force => true do |t|
    t.string   "follower_type"
    t.integer  "follower_id"
    t.string   "followable_type"
    t.integer  "followable_id"
    t.datetime "created_at"
  end

  add_index "follows", ["followable_id", "followable_type"], :name => "fk_followables"
  add_index "follows", ["follower_id", "follower_type"], :name => "fk_follows"

  create_table "gender_and_ages", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "goals", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.string   "position"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "permission"
    t.string   "superlative"
    t.string   "adjective"
    t.string   "location"
  end

  create_table "group_members", :force => true do |t|
    t.integer  "place_group_id"
    t.integer  "user_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.boolean  "confirmed",      :default => false
  end

  add_index "group_members", ["place_group_id"], :name => "index_group_members_on_place_group_id"
  add_index "group_members", ["user_id"], :name => "index_group_members_on_user_id"

  create_table "images", :force => true do |t|
    t.string   "description"
    t.string   "type"
    t.string   "name"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "interests", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "permission"
    t.integer  "video_id"
    t.integer  "created_by_id"
  end

  create_table "interview_schedules", :force => true do |t|
    t.integer  "user_id"
    t.integer  "job_id"
    t.datetime "schedule"
    t.integer  "candidate_id"
    t.boolean  "accept"
    t.boolean  "reschedule"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "job_description_details", :force => true do |t|
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "job_id"
  end

  create_table "job_questions", :force => true do |t|
    t.string   "question"
    t.integer  "job_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "job_questions_answers", :force => true do |t|
    t.string   "answer"
    t.integer  "job_id"
    t.integer  "user_id"
    t.integer  "job_question_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.string   "video_answer_file_name"
    t.string   "video_answer_content_type"
    t.integer  "video_answer_file_size"
    t.datetime "video_answer_updated_at"
  end

  create_table "job_recommendation_settings", :force => true do |t|
    t.string   "referal_bonus"
    t.integer  "candidates"
    t.string   "step"
    t.integer  "job_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "job_work_experiences", :force => true do |t|
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "job_id"
    t.integer  "work_experience_id"
    t.integer  "source_id"
  end

  create_table "jobs", :force => true do |t|
    t.string   "position"
    t.integer  "company_id"
    t.string   "details"
    t.integer  "user_id"
    t.string   "description"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "poster_file_name"
    t.string   "poster_content_type"
    t.integer  "poster_file_size"
    t.datetime "poster_updated_at"
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
    t.boolean  "video_answer"
    t.string   "city"
    t.string   "country"
    t.string   "salary"
    t.string   "license"
    t.integer  "stored_profession_id"
    t.string   "location"
    t.string   "application_type"
  end

  add_index "jobs", ["stored_profession_id"], :name => "index_jobs_on_stored_profession_id"

  create_table "jobs_qualifications", :force => true do |t|
    t.string   "name"
    t.integer  "job_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "job_name"
  end

  create_table "keys", :force => true do |t|
    t.string   "name"
    t.string   "value"
    t.string   "other"
    t.string   "flag_type"
    t.boolean  "flag_value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "likes", :force => true do |t|
    t.string   "liker_type"
    t.integer  "liker_id"
    t.string   "likeable_type"
    t.integer  "likeable_id"
    t.datetime "created_at"
  end

  add_index "likes", ["likeable_id", "likeable_type"], :name => "fk_likeables"
  add_index "likes", ["liker_id", "liker_type"], :name => "fk_likes"

  create_table "locations", :force => true do |t|
    t.string   "name"
    t.string   "city"
    t.string   "town"
    t.float    "lat"
    t.float    "lon"
    t.string   "address"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "location"
    t.string   "website"
    t.string   "email"
    t.string   "mobile_number"
  end

  create_table "mentions", :force => true do |t|
    t.string   "mentioner_type"
    t.integer  "mentioner_id"
    t.string   "mentionable_type"
    t.integer  "mentionable_id"
    t.datetime "created_at"
  end

  add_index "mentions", ["mentionable_id", "mentionable_type"], :name => "fk_mentionables"
  add_index "mentions", ["mentioner_id", "mentioner_type"], :name => "fk_mentions"

  create_table "nominee_testimonies", :force => true do |t|
    t.integer  "nominee_id"
    t.integer  "video_id"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "user_id"
  end

  create_table "nominees", :force => true do |t|
    t.integer  "user_id"
    t.integer  "competition_id"
    t.integer  "video_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "creator_id"
  end

  create_table "notifications", :force => true do |t|
    t.integer  "user_id"
    t.text     "message"
    t.string   "type"
    t.integer  "topic_id"
    t.boolean  "checked"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "notifications", ["user_id"], :name => "index_notifications_on_user_id"

  create_table "place_groups", :force => true do |t|
    t.string   "name"
    t.string   "place_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "user_id"
    t.float    "lat"
    t.float    "lng"
    t.integer  "group_members_count"
    t.string   "address"
  end

  create_table "prizes", :force => true do |t|
    t.string   "name"
    t.float    "amount"
    t.string   "details"
    t.integer  "competition_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "professions", :force => true do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.string   "job_title"
    t.string   "job_description"
    t.string   "other_details"
    t.integer  "company_id"
    t.integer  "user_id"
    t.string   "other"
    t.boolean  "current_job"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "permission"
    t.string   "location"
  end

  create_table "qualifications", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "rails_admin_histories", :force => true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      :limit => 2
    t.integer  "year",       :limit => 8
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], :name => "index_rails_admin_histories"

  create_table "ratings", :force => true do |t|
    t.float    "rate"
    t.integer  "candidate_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "user_id"
  end

  add_index "ratings", ["candidate_id"], :name => "index_ratings_on_candidate_id"
  add_index "ratings", ["user_id"], :name => "index_ratings_on_user_id"

  create_table "referrals", :force => true do |t|
    t.integer  "company_id"
    t.integer  "job_id"
    t.integer  "created_by"
    t.string   "created_to"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "referrals", ["company_id"], :name => "index_referrals_on_company_id"
  add_index "referrals", ["job_id"], :name => "index_referrals_on_job_id"

  create_table "requirements", :force => true do |t|
    t.string   "name"
    t.integer  "job_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "settings", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "smsgateways", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "stored_achievements", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "stored_company_cultures", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "stored_professions", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "stored_testimonies", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "stored_trainings", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "stored_users", :force => true do |t|
    t.string   "last_name"
    t.string   "first_name"
    t.string   "middle_initial"
    t.string   "training"
    t.string   "mobile_number"
    t.string   "brgy"
    t.string   "town"
    t.string   "province"
    t.string   "country"
    t.string   "gender"
    t.string   "education"
    t.string   "email"
    t.integer  "age"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "training_provider"
    t.string   "provider_address"
  end

  create_table "subscriptions", :force => true do |t|
    t.integer  "stored_profession_id"
    t.integer  "user_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "subscriptions", ["stored_profession_id"], :name => "index_subscriptions_on_stored_profession_id"
  add_index "subscriptions", ["user_id"], :name => "index_subscriptions_on_user_id"

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       :limit => 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], :name => "taggings_idx", :unique => true
  add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", :force => true do |t|
    t.string  "name"
    t.integer "taggings_count", :default => 0
  end

  add_index "tags", ["name"], :name => "index_tags_on_name", :unique => true

  create_table "testimonies", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.integer  "video_id"
    t.string   "description"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "user_email"
    t.string   "user_name"
    t.integer  "created_by_id"
    t.string   "permission"
    t.integer  "place_group_id"
  end

  add_index "testimonies", ["place_group_id"], :name => "index_testimonies_on_place_group_id"

  create_table "trainings", :force => true do |t|
    t.string   "name"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "company_name"
    t.string   "description"
    t.string   "details"
    t.string   "other_details"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "user_id"
    t.string   "permission"
  end

  create_table "user_job_recommendations", :force => true do |t|
    t.integer  "recommended_by_id"
    t.integer  "user_id"
    t.integer  "job_id"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.boolean  "accepted",          :default => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                       :default => "",    :null => false
    t.string   "encrypted_password",          :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",               :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.string   "name"
    t.string   "authentication_token"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "address"
    t.string   "contact_number"
    t.integer  "facebook_id"
    t.date     "birthday"
    t.boolean  "is_admin"
    t.string   "featured_video_file_name"
    t.string   "featured_video_content_type"
    t.integer  "featured_video_file_size"
    t.datetime "featured_video_updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "gender"
    t.integer  "star_points",                 :default => 0
    t.boolean  "employer",                    :default => false
    t.boolean  "employment_availability",     :default => true
    t.string   "provider"
    t.string   "uid"
    t.float    "lat"
    t.float    "lng"
    t.string   "about"
    t.integer  "testimonies_count"
    t.boolean  "is_following",                :default => false
    t.string   "facebook_image"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",           :default => 0
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["invitation_token"], :name => "index_users_on_invitation_token", :unique => true
  add_index "users", ["invitations_count"], :name => "index_users_on_invitations_count"
  add_index "users", ["invited_by_id"], :name => "index_users_on_invited_by_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

  create_table "videos", :force => true do |t|
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.string   "payload_file_name",                   :null => false
    t.string   "payload_content_type",                :null => false
    t.integer  "payload_file_size",                   :null => false
    t.datetime "payload_updated_at",                  :null => false
    t.string   "resource_type"
    t.integer  "resource_id"
    t.integer  "user_id"
    t.string   "title"
    t.integer  "goal_id"
    t.integer  "stars"
    t.integer  "testimonies"
    t.integer  "nominated_by"
    t.integer  "training_id"
    t.integer  "nominee_id"
    t.integer  "interest_id"
    t.integer  "comment_id"
    t.integer  "like_count"
    t.integer  "views_count"
    t.integer  "comments_count"
    t.string   "description"
    t.string   "name"
    t.integer  "created_by",                          :null => false
    t.integer  "likers_count",         :default => 0
  end

  create_table "welcomes", :force => true do |t|
    t.string   "index"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "work_experiences", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "workers", :force => true do |t|
    t.string   "region"
    t.string   "province"
    t.string   "district"
    t.string   "municipality"
    t.string   "provider"
    t.string   "TBP_id"
    t.string   "address"
    t.string   "type_of_institution"
    t.string   "provider_classification"
    t.string   "qualification"
    t.string   "COPR_number"
    t.string   "delivery_mode"
    t.string   "student_id_number"
    t.string   "last_name"
    t.string   "first_name"
    t.string   "middle_initial"
    t.string   "mobile_number"
    t.string   "email"
    t.string   "street_address"
    t.string   "barangay"
    t.string   "home_municipality"
    t.string   "gender"
    t.string   "birth_date"
    t.string   "age"
    t.string   "civil_status"
    t.string   "education"
    t.string   "scholarship"
    t.string   "client_type"
    t.string   "date_started"
    t.string   "date_ended"
    t.string   "reason"
    t.string   "assesment_result"
    t.string   "employment_date"
    t.string   "name_of_employer"
    t.string   "address_of_employer"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

end
