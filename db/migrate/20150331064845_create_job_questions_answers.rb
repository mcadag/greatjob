class CreateJobQuestionsAnswers < ActiveRecord::Migration
  def change
    create_table :job_questions_answers do |t|
      t.string :answer
      t.integer :job_id
      t.integer :user_id
      t.integer :job_question_id

      t.timestamps
    end
    add_attachment :job_questions_answers, :video_answer
  end
end
