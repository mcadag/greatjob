class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.string :city
      t.string :town
      t.float :lat
      t.float :lon
      t.string :address

      t.timestamps
    end
  end
end
