class CompetitionsController < ApplicationController
    include CompetitionsHelper
    include UsersHelper
    include NomineesHelper
    include VideosHelper
  before_filter :set_competition, only: [:show, :edit, :update, :destroy]
  before_filter :require_login, only: [:create, :edit, :update, :destroy]
  respond_to :html
  helper_method :get_video_for_competition, :get_nominees_for_competition

  def index
    @competitions = []
    if !params[:q].nil?
      competitions = Competition.where("name like ? OR location like ? OR superlative like ? OR year like ? ", 
                                                                      "%" + params[:q] + "%", 
                                                                      "%" + params[:q] + "%",
                                                                      "%" + params[:q] + "%",
                                                                      "%" + params[:q] + "%").shuffle
    else
      competitions = Competition.all.shuffle
    end
    
    competitions.each do |competition|
      @competitions << competitionJSONResponse(competition)
    end
    respond_to do |format|
      format.html
    end
  end

  def show
    competition = Competition.find(params[:id])
    nominees = Nominee.where(competition_id: params[:id])
    @competition = competitionJSONResponse(competition)    
    @nominees = []
    nominees.each do | nominee |
      video = Video.where(id: nominee.video_id)
      nominated_user = User.select("name").where(id: nominee.user_id).limit(1)
      nominated_by_user = User.select("name").where(id: nominee.creator_id).limit(1)
      nominee[:nominated_user_name] = nominated_user.pluck(:name)[0]
      nominee[:nominated_by_user] = nominated_by_user.pluck(:name)[0]
      nominee[:video_thumb_url] = "https://ccrnrcrc.files.wordpress.com/2012/03/profile-placeholder.gif"
      @nominees << nominee
    end
      set_meta_tags :og => {
        :title => 'Vote for me to be the ' + @competition[:title],
        :description => 'Join me on Famous.ph', 
        :url => competition_url(params[:id])
      }
    if @nominees.count == 0
      redirect_to competitions_path, notice: "There are no nominations at the moment. do you want to nominate a friend?"
    else
      respond_to do |format|
        format.html
      end
    end
  end

  def new
    @competition = Competition.new
    respond_with(@competition)
  end

  def edit
  end

  def create
    @competition = Competition.new
    @competition.name = params[:competition][:name]
    @competition.location = params[:competition][:location]
    @competition.year = params[:competition][:year]
    @competition.superlative = params[:competition][:superlative]
    @competition.start_date = params[:competition][:start_date]
    @competition.end_date = params[:competition][:end_date]
    @competition.company_id = params[:competition][:company_id]
    @competition.user_id = params[:competition][:user_id]
    @competition.prize_id = params[:competition][:prize_id]
    @competition.poster = params[:competition][:poster]
    @competition.save

    params[:qualification][:name].split(/, ?/).each do | qualification |
      @competition_qualification = CompetitionQualification.new
      @competition_qualification.name = qualification
      @competition_qualification.competition_id = @competition.id
      @competition_qualification.save
    end

     @competition[:title] = competitionTitle(@competition)
    render :json => competitionJSONResponse(@competition)
  end

  def update
    @competition.update_attributes(params.require(:competition).permit!)
    render :json => @competition.as_json()
  end

  def destroy
    @competition.destroy
    render :json => @competition.as_json()
  end

  # refactor later
  def suggested
    results = []
    suggested = Competition.where(['end_date > ?', DateTime.now]).order("prize_id DESC")
    suggested.each do |competition|
      if QualificationsStringForUserID(@user.id).any?
        results << competitionJSONResponse(competition)
      else
        results << competitionJSONResponse(competition)
      end
    end
    render :json => results.as_json
  end

  def top
    results = []
    trending_competitions = Competition.where(['end_date > ?', DateTime.now]).order("prize_id DESC")
    trending_competitions.each do |competition|
      results << competitionJSONResponse(competition)
    end

    render :json => results.as_json
  end

  def search
    results = []
    search_results = Competition.includes(:competitionQualifications).where("name like ? OR location like ? OR superlative like ? OR year like ? ", 
                                                                      "%" + params[:name] + "%", 
                                                                      "%" + params[:location] + "%",
                                                                      "%" + params[:superlative] + "%",
                                                                      "%" + params[:year] + "%")
    search_results.each do |competition|
      words = []
      params_qualifications = params[:qualifications].split(",")
      params_qualifications.each do |word|
        words.push(word)
      end

      competitionTitle(competition)
      
      if words.any?
        results << competitionJSONResponse(competition) if words.any? { |i| competitionQualificationsString(competition).include?(i) }
      else
        results << competitionJSONResponse(competition)
      end
    end
      render json: results.as_json()
  end 

  def nominees
    @nominees = []
    nominees = Nominee.where(competition_id: params[:id])
    nominees.each do |nominee|

      @nominees << nomineeJSONResponse(nominee)
    end
    respond_to do |format|
      format.html
    end  
  end

  def get_video_for_competition(competition)
    nom = (Nominee.joins(:counter).where(competition_id: competition.id).order("votes DESC").limit(1))
    video = Video.where(id: nom.pluck(:video_id))
    competition[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
    if video.exists?
      pre_vid = JSON.parse(competition[:video])
      return pre_vid["payload_mobile_url"]
    else
      return
    end
  end

  def get_nominees_for_competition(competition)
    @nominees = []
    nominees.each do | nominee |
      video = Video.where(id: nominee.video_id)
      nominated_user = User.select("name").where(id: nominee.user_id).limit(1)
      nominated_by_user = User.select("name").where(id: nominee.creator_id).limit(1)
      nominee[:nominated_user_name] = nominated_user.pluck(:name)[0]
      nominee[:nominated_by_user] = nominated_by_user.pluck(:name)[0]
      nominee[:video_thumb_url] = "https://ccrnrcrc.files.wordpress.com/2012/03/profile-placeholder.gif"
      @nominees << nominee
    end
    if @nominees.count == 0
      redirect_to competitions_path, notice: "There are no nominations at the moment. do you want to nominate a friend?"
    else
      return @nominees
    end
  end

  private
    def set_competition
      @competition = Competition.find(params[:id])
    end

    def authenticate_token!
      auth_user = User.where(authentication_token: params[:authentication_token])
      @user = auth_user[0]
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

    def star_count
      comments = Competition.where()
    end

    def require_login
      unless current_user
        redirect_to new_user_registration_url, :alert => "Please login or signup"
      end
    end
end
