class CreateStoredUsers < ActiveRecord::Migration
  def change
    create_table :stored_users do |t|
      t.string :last_name
      t.string :first_name
      t.string :middle_initial
      t.string :training
      t.date :training_start
      t.date :training_end
      t.integer :mobile_number, :limit => 8
      t.string :brgy
      t.string :town
      t.string :province
      t.string :country
      t.string :gender
      t.string :education
      t.string :education
      t.string :email
      t.integer :age

      t.timestamps
    end
  end
end
