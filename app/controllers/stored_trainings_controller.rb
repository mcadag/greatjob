
class StoredTrainingsController < ApplicationController
  # GET /stored_trainings
  # GET /stored_trainings.json
  def index
    @stored_trainings = StoredTraining.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stored_trainings }
    end
  end

  # GET /stored_trainings/1
  # GET /stored_trainings/1.json
  def show
    @stored_training = StoredTraining.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stored_training }
    end
  end

  # GET /stored_trainings/new
  # GET /stored_trainings/new.json
  def new
    @stored_training = StoredTraining.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stored_training }
    end
  end

  # GET /stored_trainings/1/edit
  def edit
    @stored_training = StoredTraining.find(params[:id])
  end

  # POST /stored_trainings
  # POST /stored_trainings.json
  def create
    @stored_training = StoredTraining.new(stored_training_params)

    respond_to do |format|
      if @stored_training.save
        format.html { redirect_to @stored_training, notice: 'Stored training was successfully created.' }
        format.json { render json: @stored_training, status: :created, location: @stored_training }
      else
        format.html { render action: "new" }
        format.json { render json: @stored_training.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stored_trainings/1
  # PATCH/PUT /stored_trainings/1.json
  def update
    @stored_training = StoredTraining.find(params[:id])

    respond_to do |format|
      if @stored_training.update_attributes(stored_training_params)
        format.html { redirect_to @stored_training, notice: 'Stored training was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stored_training.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stored_trainings/1
  # DELETE /stored_trainings/1.json
  def destroy
    @stored_training = StoredTraining.find(params[:id])
    @stored_training.destroy

    respond_to do |format|
      format.html { redirect_to stored_trainings_url }
      format.json { head :no_content }
    end
  end

  def search
    @stored_training = StoredTraining.where("name LIKE ?", "%"+params[:q]+"%").all
    if @stored_training.count >= 1
      render json: @stored_training.as_json(:only => [:name])
    else
      render :json => {:message => "No training Found with params:" + params[:q], :status => 404}
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def stored_training_params
      params.require(:stored_training).permit(:name)
    end
end
