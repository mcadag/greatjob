require 'spec_helper'

describe "nominees/show" do
  before(:each) do
    @nominee = assign(:nominee, stub_model(Nominee,
      :user_id => 1,
      :competition_id => 2,
      :video_id => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/3/)
  end
end
