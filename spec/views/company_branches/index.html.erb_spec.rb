require 'spec_helper'

describe "company_branches/index" do
  before(:each) do
    assign(:company_branches, [
      stub_model(CompanyBranch,
        :name => "Name",
        :comapny_id => 1,
        :branch_id => 2,
        :user_id => 3
      ),
      stub_model(CompanyBranch,
        :name => "Name",
        :comapny_id => 1,
        :branch_id => 2,
        :user_id => 3
      )
    ])
  end

  it "renders a list of company_branches" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
