module Api
  class AwardsController < ApplicationController
    before_filter :set_award, only: [:show, :edit, :update, :destroy]
    before_filter :authenticate_token!, :only => [:create, :update, :destroy, :approve, :reject]
    respond_to :json

    def index
    awards = []
    results = Award.my(params[:user_id]).latest
    results.each do |award|
        video = Video.awards(award.id)
        if video.count >= 1
          award[:video] = video.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
          award[:facebook_share_url] = user_award_url(award.user_id, award.id)
          awards << award          
        end
    end
      respond_with(awards)
    end
    
    def show
      @award = Award.find(params[:id])
      render json: @award
    end
  
    def new
      @award = Award.new
      respond_with(@award)
    end
  
    def edit
    end
  
    def create
      award = Award.new
      award.name = params[:award][:name]
      award.description = params[:award][:description]
      award.user_id = params[:user_id]
      award.created_by_id = @user.id
      if @user.id == params[:user_id]
        award.permission = "allow"
      end
      if award.save
        User.update_counters @user.id, star_points: 5
        video = Video.new
        video.payload = params[:video][:payload]
        video.name = params[:video][:title]
        video.resource_type = "achievement"
        video.stars = 0
        video.testimonies = 0
        video.like_count = 0
        video.views_count = 0
        video.comments_count = 0
        video.description = params[:video][:description]
        video.created_by = @user.id
        video.resource_id = award.id
        if video.save
          award.update_attributes(:video_id => video.id )
          creator = User.where(id: @user.id).limit(1)
          uploaded_to_user = User.where(id: award.user_id).limit(1)
          video_a = Video.where(id: video.id).limit(1)
          if creator[0].id != params[:user_id]
            NotificationsMailer.new_award_added(uploaded_to_user[0], creator[0], award, video_a[0].payload.url, video_a[0].payload(:thumb)).deliver
          end
          recipient = User.where(id: params[:user_id]).first
          render :json=> award.as_json(:user_id=>award.user_id, :status=>201, :message=>"Award Added"), :status=>201
        end
      end
    end
  
    def update
      award.update_attributes(params.require(:award).permit!)
      render :json => award.as_json(:status => 201, :message => "Updated"), status => 201
    end
  
    def destroy
      @video = Video.awards(@award.id)
      video_id = @award.video_id
      if @award.user_id == @user.id
        @award.destroy
        @video.destroy(video_id)
        render :json => {:message => "Deleted"}
      else
        render :json => {:message => "You are not authorized to delete the resource"}, :status => :bad_request
      end
      
    end
  
    def approve
      @award = Award.find(params[:award_id])
      @award.update_attributes(permission: "allow")
      render :json => @award.as_json(:status => 201, :message => "Updated"), status => 201
    end

    def reject
      @award = Award.find(params[:award_id])
      @award.update_attributes(permission: "denied")
      render :json => @award.as_json(:status => 201, :message => "Updated"), status => 201
    end

    private
      def set_award
        @award = Award.find(params[:id])
      end

      def authenticate_token!
        auth_user = User.where(authentication_token: params[:authentication_token])
        @user = auth_user[0]
        if auth_user.count >= 1
          true
        else
          render :json => {:message => "your authentication token is not valid"}
      end
    end
  end
end