require "spec_helper"

describe CompanyTestimoniesController do
  describe "routing" do

    it "routes to #index" do
      get("/company_testimonies").should route_to("company_testimonies#index")
    end

    it "routes to #new" do
      get("/company_testimonies/new").should route_to("company_testimonies#new")
    end

    it "routes to #show" do
      get("/company_testimonies/1").should route_to("company_testimonies#show", :id => "1")
    end

    it "routes to #edit" do
      get("/company_testimonies/1/edit").should route_to("company_testimonies#edit", :id => "1")
    end

    it "routes to #create" do
      post("/company_testimonies").should route_to("company_testimonies#create")
    end

    it "routes to #update" do
      put("/company_testimonies/1").should route_to("company_testimonies#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/company_testimonies/1").should route_to("company_testimonies#destroy", :id => "1")
    end

  end
end
