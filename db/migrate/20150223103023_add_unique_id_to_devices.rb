class AddUniqueIdToDevices < ActiveRecord::Migration
  def change
  	add_column :devices, :unique_id, :string
  end
end
