require 'spec_helper'

describe "trainings/edit" do
  before(:each) do
    @training = assign(:training, stub_model(Training,
      :name => "MyString",
      :company_name => "MyString",
      :description => "MyString",
      :details => "MyString",
      :other_details => "MyString"
    ))
  end

  it "renders the edit training form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", training_path(@training), "post" do
      assert_select "input#training_name[name=?]", "training[name]"
      assert_select "input#training_company_name[name=?]", "training[company_name]"
      assert_select "input#training_description[name=?]", "training[description]"
      assert_select "input#training_details[name=?]", "training[details]"
      assert_select "input#training_other_details[name=?]", "training[other_details]"
    end
  end
end
