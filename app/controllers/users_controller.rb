class UsersController < ApplicationController

  include NomineesHelper
  include UsersHelper
  include CompaniesHelper
  # before_filter :authenticate_token_for_user!, :only => [:show, :me, :search, :update, :active_nominations, :inactive_nominations, :follow, :unfollow, :following, :followers, :latest]
  before_filter :set_followable!, :only =>[:follow, :unfollow, :following, :followers, :followingCompanies]
  before_filter :set_user, :only => [:edit]
  before_filter :require_login, :only => [:show]
  respond_to :html
  	
  def show
    @user = current_user
    @testimonials =  @user.testimonies.includes(:video)
  end

  def edit
  end

  	# def show
    #     @results = []
    # 		@user = User.find_by_id(params[:id])
    #     gon.current_user = @user
    #     @goal = Goal.where(user_id: params[:id]).first
    #     @skills = Interest.where(user_id: params[:id]).all

    #     @skills.each do |skills|
    #       @video = Video.where(resource_id: skills.id, resource_type: 'skill')
    #         if @video.exists?
    #           skills[:video] = @video.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])[0]
    #           @results << skills
    #         end
    #     end

    #     @professions = Profession.where(user_id: params[:id]).all
    #     @trainings = Training.where(user_id: params[:id]).all
    #     set_meta_tags :og => {
    #       :title => "Famous.ph recommends",
    #       :description => 'Get recommended by Famous.ph registering today!', 
    #       :url => user_url(params[:id]),
    #       :type => "website",
    #       :image => @user[:avatar_url]
    #     }
    #     respond_to do |format|
    #       format.html
    #       format.json { render :json => user.as_json(:methods => [:avatar_url, :featured_video_url, :featured_video_thumb_url], :except => [:authentication_token]) }
    #     end
    #   end

  def me
    user = User.where(authentication_token: params[:authentication_token])
    render :json=> user.as_json(:methods => [:avatar_url, :featured_video_url, :featured_video_thumb_url])
  end


      def update
        user = User.find_by_id(params[:id])
        user.update_attributes(params.require(:user).permit!)
        render :json=> user.as_json(:auth_token=>user.authentication_token, :email=>user.email, 
                                    :methods => [:avatar_url, :featured_video_url, :featured_video_thumb_url], :address=>user.address, 
                                    :contact_number=>user.contact_number), :status=>201
      end

      def search
        results = []
        users = User.where("email LIKE ? OR name LIKE ? OR contact_number LIKE ?", "%" + params[:q] + "%", "%" + params[:q] + "%", "%" + params[:q] + "%")
        users.each do |user|
          @user = user
          goal = Goal.where(user_id: user.id).first
          if goal
            @user[:title] = user.name + ", " + goal.position
            @user[:goal] = goal.position
          else
            @user[:title] = user.name
            @user[:goal] = nil
          end
          @user[:avatar_url] = @user.as_json(:methods => [:avatar_url])
          results << @user
        end
        render :json => results.as_json(:except => [:authentication_token])
      end

      def active_nominations
        nominees = []
        results = Nominee.where(user_id: @user.id).order('created_at DESC')
        results.each do |nominee|
          competition = Competition.where(id: nominee.competition_id)
          com = competition[0]
          now = DateTime.now
          if (com.start_date < now && now < com.end_date) && !userNominationsJSONResponse(nominee, com).nil?
            nominee[:status] = "Active"
            nominees << userNominationsJSONResponse(nominee, com)
          end
        end
        render :json => nominees.as_json(:include => { :video => {:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url]}})
      end

      def inactive_nominations
        nominees = []
        results = Nominee.where(user_id: @user.id).order('created_at DESC')
        results.each do |nominee|
          competition = Competition.where(id: nominee.competition_id)
          com = competition[0]
          now = DateTime.now
          if (com.start_date < now && now > com.end_date) && !video.empty?
            nominee[:status] = "Inactive"
            nominees << userNominationsJSONResponse(nominee, com)
          end
        end
        render :json => nominees.as_json(:include => { :video => {:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url]}})
      end

      def follow
        followed = @user.follow!(@follow_user)
        if followed
          @user.create_activity action: 'follow', parameters: {title: titleForFollowingActivity(@user, @follow_user, "is now following"),
                                                                  resource_type: 'follow',
                                                                  resource_id: followed
                                                            }, recipient: @follow_user, owner: @user
          render :json => {:message => "followed"}
        else
          render :json => {:message => "Cannot follow user"}
        end
      end

      def unfollow
        unfollowed = @user.unfollow!(@follow_user)
        if unfollowed
          @user.create_activity action: 'unfollow', parameters: {title: titleForFollowingActivity(@user, @follow_user, "unfollowed"),
                                                                  resource_type: 'unfollow',
                                                                  resource_id: unfollowed
                                                            }, recipient: @follow_user, owner: @user
          render :json => {:message => "unfollowed"}
        else
          render :json => {:message => "Cannot unfollow user"}
        end

      end

      def following
        results = []
        followees = @follow_user.followees(User)
        followees.each do | user |
          results << UsersJSONResponseForWithNoAuth(user)
        end
        render :json => results
      end

      def followers
        results = []
        followers = @user.followers(User)
        followers.each do | user |
          results << UsersJSONResponseForWithNoAuth(user)
        end
        render :json => results
      end

      def followingCompanies
        results = []
        q = @follow_user.followees(Company)
        q.each do |comapny|
          results << CompanyJSONResponse(comapny)
        end
        render :json => results
      end

      def activities
        @activities = PublicActivity::Activity.where(recipient_id: params[:user_id]).order('updated_at DESC')
        render :json => @activities
      end

      def latest
        results = []
        users = User.select("name, id, avatar_file_name, avatar_updated_at").where("avatar_content_type IS NOT NULL").order('created_at DESC').page(1).per(10)
        users.each do |user|
          @user = user
          goal = Goal.where(user_id: user.id).first
          if goal
            @user[:title] = user.name + ", " + goal.position
            @user[:goal] = goal.position
          else
            @user[:title] = user.name
            @user[:goal] = nil
          end

          @user[:avatar_url] = @user.as_json(:methods => [:avatar_url])
          results << @user if @user.avatar.exists?
        end
        render :json => results
      end

      def pending_permissions
        # @results = []
        # @testimonies = Testimony.pending.latest
        # @testimonies.each do |testimony|
        #   @results[:testimonies] << testimony
        # end

        # respond_to do |format|
        #   format.html
        # end
      end

      def job_recommendations
        @recommendations = UserJobRecommendations.where(user_id: current_user.id).all
      
        respond_to do |format|
          format.html
        end 
      end

      def recommend_user
        @recommendation = UserJobRecommendations.where(user_id: current_user.id, job_id: params[:job_id]).first
        @recommendation.update_attribute(accepted, params[:accepted])
        respond_to do |format|
          format.html
        end 
      end

		private

        def set_user
          @user = User.find(params[:id])
        end

        def set_followable!
          follow_user = User.where(id: params[:user_id])
          @follow_user = follow_user[0]
        end

        def authenticate_token_for_user!
          auth_user = User.where(authentication_token: params[:authentication_token])
          @user = auth_user[0]
          if auth_user.count >= 1
            true
          else
            render :json => {:message => "your authentication token is not valid"}
          end
        end

        def require_login
          # unless current_user
          #   redirect_to new_user_registration_url, :alert => "Please login or signup"
          # end
        end
		end