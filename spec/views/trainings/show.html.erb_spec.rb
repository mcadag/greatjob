require 'spec_helper'

describe "trainings/show" do
  before(:each) do
    @training = assign(:training, stub_model(Training,
      :name => "Name",
      :company_name => "Company Name",
      :description => "Description",
      :details => "Details",
      :other_details => "Other Details"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Company Name/)
    rendered.should match(/Description/)
    rendered.should match(/Details/)
    rendered.should match(/Other Details/)
  end
end
