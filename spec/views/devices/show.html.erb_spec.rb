require 'spec_helper'

describe "devices/show" do
  before(:each) do
    @device = assign(:device, stub_model(Device,
      :user_id => 1,
      :unique_indetifier => "Unique Indetifier",
      :platform => "Platform",
      :active => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Unique Indetifier/)
    rendered.should match(/Platform/)
    rendered.should match(/false/)
  end
end
