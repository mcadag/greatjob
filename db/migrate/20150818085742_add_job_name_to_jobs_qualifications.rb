class AddJobNameToJobsQualifications < ActiveRecord::Migration
  def change
    add_column :jobs_qualifications, :job_name, :string
  end
end
