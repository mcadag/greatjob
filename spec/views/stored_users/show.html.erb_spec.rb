require 'spec_helper'

describe "stored_users/show" do
  before(:each) do
    @stored_user = assign(:stored_user, stub_model(StoredUser,
      :last_name => "Last Name",
      :first_name => "First Name",
      :middle_initial => "Middle Initial",
      :training => "Training",
      :mobile_number => 1,
      :brgy => "Brgy",
      :town => "Town",
      :province => "Province",
      :country => "Country",
      :gender => "Gender",
      :education => "Education",
      :education => "Education",
      :email => "Email",
      :age => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Last Name/)
    rendered.should match(/First Name/)
    rendered.should match(/Middle Initial/)
    rendered.should match(/Training/)
    rendered.should match(/1/)
    rendered.should match(/Brgy/)
    rendered.should match(/Town/)
    rendered.should match(/Province/)
    rendered.should match(/Country/)
    rendered.should match(/Gender/)
    rendered.should match(/Education/)
    rendered.should match(/Education/)
    rendered.should match(/Email/)
    rendered.should match(/2/)
  end
end
