require "spec_helper"

describe StoredCompanyCulturesController do
  describe "routing" do

    it "routes to #index" do
      get("/stored_company_cultures").should route_to("stored_company_cultures#index")
    end

    it "routes to #new" do
      get("/stored_company_cultures/new").should route_to("stored_company_cultures#new")
    end

    it "routes to #show" do
      get("/stored_company_cultures/1").should route_to("stored_company_cultures#show", :id => "1")
    end

    it "routes to #edit" do
      get("/stored_company_cultures/1/edit").should route_to("stored_company_cultures#edit", :id => "1")
    end

    it "routes to #create" do
      post("/stored_company_cultures").should route_to("stored_company_cultures#create")
    end

    it "routes to #update" do
      put("/stored_company_cultures/1").should route_to("stored_company_cultures#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/stored_company_cultures/1").should route_to("stored_company_cultures#destroy", :id => "1")
    end

  end
end
