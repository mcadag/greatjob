class JobQuestionsAnswer < ActiveRecord::Base
  attr_accessible :answer, :job_id, :job_question_id, :user_id, :video_answer

  	has_attached_file :video_answer,
		:storage => :s3,
		:s3_permissions => :private,
		:s3_credentials => "#{Rails.root}/config/aws.yml",
		:styles => {
    		:mobile => {:geometry => "400x300", 
    					:format => 'mp4'},
    		:thumb => { :geometry => "400x700",
    					:format => 'jpg', 
    					:time => 6, 
    					:convert_options => "-auto-orient"}
  		}, 
  		:processors => [:ffmpeg],
  		:use_timestamp => false
	do_not_validate_attachment_file_type :video_answer

	def video_answer_url
		video_answer.url(:original)
	end

	def video_answer_thumb_url
		video_answer.url(:thumb)
	end

	def video_answer_mobile_url
		video_answer.url(:mobile)
	end
end
