class CreateCompetitionVideoComments < ActiveRecord::Migration
  def change
    create_table :competition_video_comments do |t|
      t.string :name
      t.integer :competition_id
      t.integer :user_id
      t.string :message
      t.integer :nominee_id
      t.string :details
      t.string :description
      t.boolean :approved
      t.boolean :is_private
      t.boolean :has_video
      t.integer :video_id

      t.timestamps
    end
    add_attachment :competition_video_comments, :payload
  end
end
