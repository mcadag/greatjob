require 'spec_helper'

describe "group_members/index" do
  before(:each) do
    assign(:group_members, [
      stub_model(GroupMember,
        :place_group => nil,
        :user => nil
      ),
      stub_model(GroupMember,
        :place_group => nil,
        :user => nil
      )
    ])
  end

  it "renders a list of group_members" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
