class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :user_id
      t.integer :nominee_id
      t.integer :video_id
      t.string :message
      t.string :details
      t.string :description
      t.boolean :approved
      t.boolean :private

      t.timestamps
    end
  end
end
