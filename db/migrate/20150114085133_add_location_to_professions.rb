class AddLocationToProfessions < ActiveRecord::Migration
  def change
    add_column :professions, :location, :string
  end
end
