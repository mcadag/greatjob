require 'spec_helper'

describe "company_dream_jobs/new" do
  before(:each) do
    assign(:company_dream_job, stub_model(CompanyDreamJob,
      :name => "MyString",
      :comapny_id => 1,
      :user_id => 1
    ).as_new_record)
  end

  it "renders new company_dream_job form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", company_dream_jobs_path, "post" do
      assert_select "input#company_dream_job_name[name=?]", "company_dream_job[name]"
      assert_select "input#company_dream_job_comapny_id[name=?]", "company_dream_job[comapny_id]"
      assert_select "input#company_dream_job_user_id[name=?]", "company_dream_job[user_id]"
    end
  end
end
