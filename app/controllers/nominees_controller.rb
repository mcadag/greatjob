
class NomineesController < ApplicationController
  include NomineesHelper
  include CompetitionsHelper
  before_filter :set_nominee, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_token!, :only => [:create, :update, :destroy, :ranking]
  respond_to :json, :html
  
  def index
    @results = []
    @nominees = Nominee.where(competition_id: params[:competition_id])
    @nominees.each do |nominee|
      nom = (Nominee.joins(:counter).where(competition_id: params[:competition_id]).order("votes DESC").limit(1))
      video = Video.where(id: nominee.video_id).limit(1)
      user = User.where(id: nominee.user_id).limit(1)
      nominee[:user] = user.as_json()
      nominee[:video] = video.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
      @results << nominee
    end
    respond_to do |format|
      format.html
    end
  end

  def show
      @nominee = Nominee.find(params[:id])
      @user = User.where(id: @nominee.user_id)
      video = Video.where(id: @nominee.video_id)
      @nominee[:video] = video.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
      @nominee[:payload_mobile_url] = get_video_for_nominee(@nominee)
      @nominee[:payload_thumb_url] = get_thumb_for_nominee(@nominee)
      @nominee[:payload_swf_url] = get_swf_for_nominee(@nominee)
      @competition = Competition.where(id: @nominee.competition_id)
      @competition[0][:title] = @competition[0].superlative.to_s() + " " + @competition[0].name.to_s() + " in  " + @competition[0].location.to_s() + " for " + @competition[0].year.to_s()
      set_meta_tags :og => {
        :title => "Please make me famous as the " + @competition[0][:title],
        :description => 'Vote for me on famous.ph', 
        :url => competition_nominee_url(@competition[0].id),
        :type => "video",
        :image => @nominee[:payload_thumb_url],
        :video => @nominee[:payload_swf_url],
      }

      respond_to do |format|
        format.html
      end
  end

  def new
    @nominee = Nominee.new
    respond_with(@nominee)
  end

  def edit
  end

  def create
    @nominee = Nominee.new
    @nominee.competition_id = params[:competition_id]
    @nominee.user_id = params[:user_id]
    @nominee.creator_id = @user.id
    nom = Nominee.where(user_id: params[:user_id], competition_id: @nominee.competition_id)

    if nom.count >= 1
      render :json => {result: "failed", message: "already nominated"}
    else
      @nominee.save
      @vote = Counter.new
      @vote.nominee_id = @nominee.id
      @vote.votes = 0
      @vote.save
      if params[:from_file].to_s == "yes"
        @nominee.update_attributes(:video_id => params[:video_id] )
        @video_id = params[:video_id]
      else
        video = Video.new
        video.payload = params[:video][:payload]
        video.resource_type = params[:video][:resource_type]
        video.title = params[:video][:title]
        video.name = params[:video][:name]
        video.description = params[:video][:description]
        video.stars = 0
        video.testimonies = 0
        video.comments_count = 0
        video.views_count = 0
        video.like_count = 0
        video.nominated_by = @user.id 
        video.user_id = params[:user_id]
        video.created_by = @user.id
        video.resource_id = @nominee.id
        if video.save
          @video_id = video.id
          @nominee.update_attributes(:video_id => video.id )
          if params[:video][:resource_type].to_s == "testimony"
            @testimony = Testimony.new
            @testimony.name = video.name
            @testimony.description = video.description
            @testimony.user_id = params[:user_id]
            @testimony.created_by_id = @user.id
            @testimony.video_id = @nominee.video_id
            @testimony.save
            video.update_attributes(:resource_id => @testimony.id)
          elsif params[:video][:resource_type].to_s == "achievement"
            award = Award.new
            award.name = video.name
            award.description = video.description
            award.user_id = params[:user_id]
            award.video_id = @nominee.video_id
            award.save
            video.update_attributes(:resource_id => award.id)
          elsif params[:video][:resource_type].to_s == "skill"
            interest = Interest.new
            interest.name = video.name
            interest.description = video.description
            interest.user_id = params[:user_id]
            interest.video_id = @nominee.video_id
            interests.save
            video.update_attributes(:resource_id => interest.id)
          end
        end
      end
      
      competition = Competition.where(id: params[:competition_id]).limit(1)
      competition_title = competition[0].superlative.to_s() + " " + competition[0].name.to_s() + " in  " + competition[0].location.to_s() + " for " + competition[0].year.to_s()
      nomination_creator = User.where(id: @user.id).limit(1)
      auth_user = User.where(id: @nominee.user_id).limit(1)
      video_a = Video.where(id: @video_id).limit(1)
      video_to_be_attached = video_a.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url], :only => [:id])
      NotificationsMailer.nominated(auth_user[0], nomination_creator[0], competition, competition_title, video_to_be_attached[0]).deliver
      render :json => {result: "success", user: auth_user[0].name, video: video_to_be_attached[0]}  
    end
  end

  def update
    @nominee.update_attributes(params.require(:nominee).permit!)
    respond_with(@nominee)
  end

  def destroy
    @nominee.destroy
    respond_with(@nominee)
  end

  def ranking
    results = []
    nominees = Nominee.joins(:counter).where(competition_id: params[:competition_id] ).order("votes DESC, created_at ASC")

    nominees.each do |nominee|
      nom = (Nominee.joins(:counter).where(competition_id: params[:competition_id]).order("votes DESC").limit(1))
      votes = Counter.where(nominee_id: nominee.id)
      video = Video.where(id: nominee.video_id).first
      comments = CompetitionVideoComment.where(video_id: nom.pluck(:video_id), competition_id: params[:competition_id])
      nominee_name = User.select("name").where(id: nominee.user_id).limit(1)
      nominee_creator = User.select("name").where(id: nominee.creator_id).limit(1)
      nominee[:nominee_id] = nom.pluck(:nominee_id)[0]
      nominee[:nominee_name] = nominee_name.pluck(:name)[0]
      nominee[:stars_count] = votes.pluck(:votes)[0]
      nominee[:nominated_by_name] = nominee_creator.pluck(:name)[0]
      nominee[:comments_count] = comments.count

      results << nominee
    end
    render :json => results.as_json(:include => { 
                                          :video => {:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url]}
                                          })
  end

    def get_video_for_nominee(nominee)
      video = Video.where(id: nominee.video_id)
      nominee[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
      if video.exists?
        pre_vid = JSON.parse(nominee[:video])
        return pre_vid["payload_mobile_url"]
      else
        return
      end
    end

    def get_swf_for_nominee(nominee)
      video = Video.where(id: nominee.video_id)
      nominee[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_swf_url])
      if video.exists?
        pre_vid = JSON.parse(nominee[:video])
        return pre_vid["payload_swf_url"]
      else
        return
      end
    end

    
    def get_thumb_for_nominee(nominee)
      video = Video.where(id: nominee.video_id)
      nominee[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
      if video.exists?
        pre_vid = JSON.parse(nominee[:video])
        return pre_vid["payload_thumb_url"]
      else
        return
      end
    end


  private
    def set_nominee
      @nominee = Nominee.find(params[:id])
    end
    def authenticate_token!
      auth_user = User.select("id").where(authentication_token: params[:authentication_token])
      if auth_user.count >= 1
        @user = auth_user[0]
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end
  end
