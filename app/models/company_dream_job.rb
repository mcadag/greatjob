class CompanyDreamJob < ActiveRecord::Base
  include PublicActivity::Model
  tracked
  attr_accessible :company_id, :name, :user_id
  belongs_to :companies
end
