require 'spec_helper'

describe "trainings/index" do
  before(:each) do
    assign(:trainings, [
      stub_model(Training,
        :name => "Name",
        :company_name => "Company Name",
        :description => "Description",
        :details => "Details",
        :other_details => "Other Details"
      ),
      stub_model(Training,
        :name => "Name",
        :company_name => "Company Name",
        :description => "Description",
        :details => "Details",
        :other_details => "Other Details"
      )
    ])
  end

  it "renders a list of trainings" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Company Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Details".to_s, :count => 2
    assert_select "tr>td", :text => "Other Details".to_s, :count => 2
  end
end
