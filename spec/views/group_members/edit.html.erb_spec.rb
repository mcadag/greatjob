require 'spec_helper'

describe "group_members/edit" do
  before(:each) do
    @group_member = assign(:group_member, stub_model(GroupMember,
      :place_group => nil,
      :user => nil
    ))
  end

  it "renders the edit group_member form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", group_member_path(@group_member), "post" do
      assert_select "input#group_member_place_group[name=?]", "group_member[place_group]"
      assert_select "input#group_member_user[name=?]", "group_member[user]"
    end
  end
end
