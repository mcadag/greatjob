require "spec_helper"

describe CompanyDreamJobsController do
  describe "routing" do

    it "routes to #index" do
      get("/company_dream_jobs").should route_to("company_dream_jobs#index")
    end

    it "routes to #new" do
      get("/company_dream_jobs/new").should route_to("company_dream_jobs#new")
    end

    it "routes to #show" do
      get("/company_dream_jobs/1").should route_to("company_dream_jobs#show", :id => "1")
    end

    it "routes to #edit" do
      get("/company_dream_jobs/1/edit").should route_to("company_dream_jobs#edit", :id => "1")
    end

    it "routes to #create" do
      post("/company_dream_jobs").should route_to("company_dream_jobs#create")
    end

    it "routes to #update" do
      put("/company_dream_jobs/1").should route_to("company_dream_jobs#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/company_dream_jobs/1").should route_to("company_dream_jobs#destroy", :id => "1")
    end

  end
end
