require 'spec_helper'

describe "stored_professions/edit" do
  before(:each) do
    @stored_profession = assign(:stored_profession, stub_model(StoredProfession,
      :name => "MyString"
    ))
  end

  it "renders the edit stored_profession form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stored_profession_path(@stored_profession), "post" do
      assert_select "input#stored_profession_name[name=?]", "stored_profession[name]"
    end
  end
end
