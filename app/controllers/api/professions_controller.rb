module Api
  class ProfessionsController < ApplicationController
  before_filter :set_profession, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_token!, :only => [:create, :update, :destroy]
  respond_to :json

  def index
    profession_result = []
    @professions = Profession.where(user_id: params[:user_id]).latest.order('start_date DESC')
    @professions.each do |profession|
      if !profession.location.nil?
        profession[:title] = profession.job_title.to_s() + " at " + profession.location.to_s()
        else
        profession[:title] = profession.job_title.to_s()  
      end
      profession_result << profession
    end
    render :json=> profession_result.as_json
  end

  def show
    respond_with(@profession)
  end

  def new
    @profession = Profession.new
    respond_with(@profession)
  end

  def edit
  end

  def create
    profession = Profession.new
    profession.user_id = params[:user_id]
    profession.job_title = params[:profession][:job_title]
    profession.location = params[:profession][:location]
    profession.job_description = params[:profession][:job_description]
    profession.other_details = params[:profession][:other_details]
    profession.start_date = params[:profession][:start_date]
    profession.end_date = params[:profession][:end_date]
    profession.current_job = params[:profession][:current_job]
    profession.other = params[:profession][:other]
    profession.company_id = params[:profession][:company_id]

    if profession.save
      profession_result = profession
      company = Company.where("name like ?", "%" + profession.location + "%")
    end

    profession_result[:title] = profession.job_title.to_s() + " at " + profession.location.to_s()
    render :json=> profession_result.as_json
  end

  def update
    @profession.update_attributes(params.require(:profession).permit!)
    @profession[:title] = @profession.job_title.to_s() + " at " + @profession.location.to_s()
    render :json=> @profession.as_json(:user_id=>@profession.user_id, :status=>201), :status=>201
  end

  def destroy
    @profession.destroy
    render :json => {:message => "Deleted"}
  end

  def permission
    profession = Profession.where(id: params[:profession_id])
    profession.update_attributes(params[:profession])
    render :json => profession.as_json(:status => 201, :message => "Permission is set"), status => 201
  end

  private
    def set_profession
      @profession = Profession.find(params[:id])
    end
    
    def authenticate_token!
      auth_user = User.where(authentication_token: params[:authentication_token])
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end
  end
end

