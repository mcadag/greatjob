class RemoveNameFromCompanyCulture < ActiveRecord::Migration
  def up
    remove_column :company_cultures, :name
  end

  def down
    add_column :company_cultures, :name, :string
  end
end
