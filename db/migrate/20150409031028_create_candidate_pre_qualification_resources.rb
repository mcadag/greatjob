class CreateCandidatePreQualificationResources < ActiveRecord::Migration
  def change
    create_table :candidate_pre_qualification_resources do |t|
      t.integer :candidate_id
      t.integer :resource_id
      t.string :resource_type
      t.integer :promoted_resource_id
      t.string :promoted_resource_type

      t.timestamps
    end
  end
end
