require 'spec_helper'

describe "stored_achievements/new" do
  before(:each) do
    assign(:stored_achievement, stub_model(StoredAchievement,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new stored_achievement form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stored_achievements_path, "post" do
      assert_select "input#stored_achievement_name[name=?]", "stored_achievement[name]"
    end
  end
end
