module Api
class StoredCompanyCulturesController < ApplicationController
  # GET /stored_company_cultures
  # GET /stored_company_cultures.json
  def index
    @stored_company_cultures = StoredCompanyCulture.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stored_company_cultures }
    end
  end

  # GET /stored_company_cultures/1
  # GET /stored_company_cultures/1.json
  def show
    @stored_company_culture = StoredCompanyCulture.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stored_company_culture }
    end
  end

  # GET /stored_company_cultures/new
  # GET /stored_company_cultures/new.json
  def new
    @stored_company_culture = StoredCompanyCulture.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stored_company_culture }
    end
  end

  # GET /stored_company_cultures/1/edit
  def edit
    @stored_company_culture = StoredCompanyCulture.find(params[:id])
  end

  # POST /stored_company_cultures
  # POST /stored_company_cultures.json
  def create
    @stored_company_culture = StoredCompanyCulture.new(stored_company_culture_params)

    respond_to do |format|
      if @stored_company_culture.save
        format.html { redirect_to @stored_company_culture, notice: 'Stored company culture was successfully created.' }
        format.json { render json: @stored_company_culture, status: :created, location: @stored_company_culture }
      else
        format.html { render action: "new" }
        format.json { render json: @stored_company_culture.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stored_company_cultures/1
  # PATCH/PUT /stored_company_cultures/1.json
  def update
    @stored_company_culture = StoredCompanyCulture.find(params[:id])

    respond_to do |format|
      if @stored_company_culture.update_attributes(stored_company_culture_params)
        format.html { redirect_to @stored_company_culture, notice: 'Stored company culture was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stored_company_culture.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stored_company_cultures/1
  # DELETE /stored_company_cultures/1.json
  def destroy
    @stored_company_culture = StoredCompanyCulture.find(params[:id])
    @stored_company_culture.destroy

    respond_to do |format|
      format.html { redirect_to stored_company_cultures_url }
      format.json { head :no_content }
    end
  end

  def search
    @stored_company_culture = StoredCompanyCulture.where("name LIKE ?", "%"+params[:q]+"%").all
    if @stored_company_culture.count >= 1
      render json: @stored_company_culture.as_json(:only => [:name])
    else
      render :json => {:message => "No company culture Found with params:" + params[:q], :status => 200}
    end
  end


  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def stored_company_culture_params
      params.require(:stored_company_culture).permit(:name)
    end
end
end