module Api
  class KeysController < ApplicationController
  # before_filter :authenticate_token!, :only => [:create, :update, :destroy]
  # before_filter :authorize_user!, :only => [:create, :update, :destroy]
  respond_to :json
  # GET /keys
  # GET /keys.json
  def index
    @keys = Key.all

    respond_to do |format|
      format.json { render json: @keys }
    end
  end

  # GET /keys/1
  # GET /keys/1.json
  def show
    @key = Key.find_by_name(params[:name])

    respond_to do |format|
      format.json { render json: @key }
    end
  end

  # GET /keys/new
  # GET /keys/new.json
  def new
    @key = Key.new

    respond_to do |format|
      format.json { render json: @key }
    end
  end

  # GET /keys/1/edit
  def edit
    @key = Key.find(params[:id])
  end

  # POST /keys
  # POST /keys.json
  def create
    @key = Key.new(params.require(:key).permit!)

    respond_to do |format|
      if @key.save
        format.json { render json: @key, status: :created }
      else
        format.json { render json: @key.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /keys/1
  # PUT /keys/1.json
  def update
    @key = Key.find(params[:id])

    respond_to do |format|
      if @key.update_attributes(params[:key])
        format.json { head :no_content }
      else
        format.json { render json: @key.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /keys/1
  # DELETE /keys/1.json
  def destroy
    @key = Key.find(params[:id])
    @key.destroy

    respond_to do |format|
      format.json { head :no_content }
    end
  end
  private

    def authenticate_token!
      if User.find_by_id(params[:user_id]).nil?
        render :json => {:message => "User does not exist."}, :success => false, :status => 422
      elsif User.find_by_id(params[:user_id]).authentication_token == params[:authentication_token]
        true
      else
        render :json => {:message => "Missing correct authentication token."}, :success => false, :status => 401
      end
    end

    def authorize_user!
      if Room.find(params[:id]).user_id == params[:user_id].to_i
        true
      else
        render :json => {:message => "User not authorized to change this content"}, :success => false, :status => 401
      end
    end
end
end