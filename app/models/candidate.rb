class Candidate < ActiveRecord::Base
  include PublicActivity::Model
  tracked
  attr_accessible :job_id, :user_id
  belongs_to :user
  belongs_to :job
  has_many :ratings
  has_many :comments
end