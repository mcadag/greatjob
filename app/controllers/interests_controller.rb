  class InterestsController < ApplicationController
    before_filter :set_interest, only: [:show, :edit, :update, :destroy]
    before_filter :require_login, :only => [:index, :edit, :update, :destroy]
    respond_to :json, :html
    
    def index
      @results = []
      @interests = Interest.where(user_id: params[:user_id]).latest
      @interests.each do |interest|
        @video = Video.where(resource_id: interest.id, resource_type: 'skill')
        if @video.exists?
          interest[:video] = @video.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])[0]
          @results << interest
        end
      end

      gon.current_user_interests = @results

      respond_to do |format|
        format.html
        format.json { render :json => @results.as_json() }
      end
    end

    def show
      @interest = Interest.find(params[:id])
      @video = Video.testimony(@interest.video_id)
      @user = User.where(id: @interest.user_id)
      @interest[:payload_mobile_url] = get_video_for_interest(@interest)
      @interest[:payload_thumb_url] = get_thumb_for_interest(@interest)
      @interest[:payload_swf_url] = get_swf_for_interest(@interest)
      set_meta_tags :og => {
        :title => "Recommended by Famous!",
        :description => 'Famous.ph, the video-powered recruitment app for skilled technical and vocational workers. Sign up now!',
        :url => user_interest_url(params[:id]),
        :type => "video",
        :image => @interest[:payload_thumb_url],
        :video => @interest[:payload_swf_url]
      }
      
      respond_to do |format|
        format.html
      end
    end

    def new
      @interest = Interest.new
      respond_with(@interest)
    end

    def edit
    end

    def create
      interest = Interest.new
      interest.name = params[:interest][:name]
      interest.description = params[:interest][:description]
      interest.user_id = params[:user_id]
      if interest.save
        video = Video.new
        video.payload = params[:video][:payload]
        video.name = params[:interest][:name]
        video.resource_type = "skill"
        video.stars = 0
        video.testimonies = 0
        video.like_count = 0
        video.views_count = 0
        video.comments_count = 0
        video.description = params[:interest][:name]
        video.created_by = params[:user_id]
        video.resource_id = interest.id
        if video.save
          interest.update_attributes(:video_id => video.id)
        end
      end
      respond_to do |format|
        format.html { redirect_to user_path(interest.user_id), notice: "You have added a new Interest." }
        format.json { render :json=> interest.as_json(:user_id=>interest.user_id, :status=>201, :message=>"Interest Added"), :status=>201 }
      end
    end

    def update
      @interest.update_attributes(params.require(:interest).permit!)
      render :json => interest.as_json(:status => 201, :message => "Updated"), status => 201
    end

    def destroy
      @interest.destroy
      render :json => {:message => "Deleted"}
    end

    def destroy
      @video = Video.where(resource_id: @interest.id, resource_type: "skill")
      video_id = @interest.video_id
      if @interest.destroy && @interest.user_id == @user.id
        @video.destroy(video_id)
        render :json => {:message => "Deleted"}
      else
        render :json => {:message => "You are not authorized to delete the resource"}
      end
      
    end

    def permission
      interest = Interest.where(id: params[:interest_id])
      interest.update_attributes(params[:interest])
      render :json => interest.as_json(:status => 201, :message => "Permission is set"), status => 201
    end

    def get_video_for_interest(interest)
      video = Video.where(id: interest.video_id)
      interest[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
      if video.exists?
        pre_vid = JSON.parse(interest[:video])
        return pre_vid["payload_mobile_url"]
      else
        return
      end
    end

    def get_thumb_for_interest(interest)
      video = Video.where(id: interest.video_id)
      interest[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
      if video.exists?
        pre_vid = JSON.parse(interest[:video])
        return pre_vid["payload_thumb_url"]
      else
        return
      end
    end

    def get_swf_for_interest(interest)
      video = Video.where(id: interest.video_id)
      interest[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url, :payload_swf_url])
      if video.exists?
        pre_vid = JSON.parse(interest[:video])
        return pre_vid["payload_swf_url"]
      else
        return
      end
    end

    private
      def set_interest
        @interest = Interest.find(params[:id])
      end
      
      def authenticate_token!
        auth_user = User.where(authentication_token: params[:authentication_token])
        @user = auth_user[0]
        if auth_user.count >= 1
          true
        else
          render :json => {:message => "your authentication token is not valid"}
        end
      end

      def require_login
        unless current_user
          redirect_to new_user_registration_url, :alert => "Please login or signup"
        end
      end
  end
