class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :description
      t.string :type
      t.string :name

      t.timestamps
    end
    add_attachment :images, :image
  end
end
