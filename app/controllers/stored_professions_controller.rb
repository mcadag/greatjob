
class StoredProfessionsController < ApplicationController
  # GET /stored_professions
  # GET /stored_professions.json
  def index
    @stored_professions = StoredProfession.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stored_professions }
    end
  end

  # GET /stored_professions/1
  # GET /stored_professions/1.json
  def show
    @stored_profession = StoredProfession.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stored_profession }
    end
  end

  # GET /stored_professions/new
  # GET /stored_professions/new.json
  def new
    @stored_profession = StoredProfession.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stored_profession }
    end
  end

  # GET /stored_professions/1/edit
  def edit
    @stored_profession = StoredProfession.find(params[:id])
  end

  # POST /stored_professions
  # POST /stored_professions.json
  def create
    @stored_profession = StoredProfession.new(stored_profession_params)

    respond_to do |format|
      if @stored_profession.save
        format.html { redirect_to @stored_profession, notice: 'Stored profession was successfully created.' }
        format.json { render json: @stored_profession, status: :created, location: @stored_profession }
      else
        format.html { render action: "new" }
        format.json { render json: @stored_profession.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stored_professions/1
  # PATCH/PUT /stored_professions/1.json
  def update
    @stored_profession = StoredProfession.find(params[:id])

    respond_to do |format|
      if @stored_profession.update_attributes(stored_profession_params)
        format.html { redirect_to @stored_profession, notice: 'Stored profession was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stored_profession.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stored_professions/1
  # DELETE /stored_professions/1.json
  def destroy
    @stored_profession = StoredProfession.find(params[:id])
    @stored_profession.destroy

    respond_to do |format|
      format.html { redirect_to stored_professions_url }
      format.json { head :no_content }
    end
  end

    def search
      # if params[:limit] >= 1
        @stored_professions = StoredProfession.where("name LIKE ?", "%"+params[:q]+"%").limit(5)
      # else
        # @stored_professions = StoredProfession.where("name LIKE ?", "%"+params[:q]+"%").all
      # end
      if @stored_professions.count >= 1
        render json: @stored_professions.as_json(:only => [:name])
      else
        render :json => {:message => "No Qualifications Found with params:" + params[:q], :status => 200}
      end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def stored_profession_params
      params.require(:stored_profession).permit(:name)
    end
end
