
class QualificationsController < ApplicationController
  before_filter :set_profession, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_token!, :only => [:create, :update, :destroy]
  respond_to :json

  # GET /qualifications
  # GET /qualifications.json
  def index
    @qualifications = Qualification.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @qualifications }
    end
  end

  # GET /qualifications/1
  # GET /qualifications/1.json
  def show
    @qualification = Qualification.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @qualification }
    end
  end

  # GET /qualifications/new
  # GET /qualifications/new.json
  def new
    @qualification = Qualification.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @qualification }
    end
  end

  # GET /qualifications/1/edit
  def edit
    @qualification = Qualification.find(params[:id])
  end

  # POST /qualifications
  # POST /qualifications.json
  def create
    @qualification = Qualification.new(qualification_params)

    respond_to do |format|
      if @qualification.save
        format.html { redirect_to @qualification, notice: 'Qualification was successfully created.' }
        format.json { render json: @qualification, status: :created, location: @qualification }
      else
        format.html { render action: "new" }
        format.json { render json: @qualification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /qualifications/1
  # PATCH/PUT /qualifications/1.json
  def update
    @qualification = Qualification.find(params[:id])

    respond_to do |format|
      if @qualification.update_attributes(qualification_params)
        format.html { redirect_to @qualification, notice: 'Qualification was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @qualification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qualifications/1
  # DELETE /qualifications/1.json
  def destroy
    @qualification = Qualification.find(params[:id])
    @qualification.destroy

    respond_to do |format|
      format.html { redirect_to qualifications_url }
      format.json { head :no_content }
    end
  end

  def search
    results = []
    @qualifications = Qualification.where("name LIKE ?", "%"+params[:q]+"%").all
    @testimonies = StoredTestimony.where("name LIKE ?", "%"+params[:q]+"%").all
    @achievements = StoredAchievement.where("name LIKE ?", "%"+params[:q]+"%").all
    @adjectives = Adjective.where("name LIKE ?", "%"+params[:q]+"%").all

    @qualifications.each do | qualification |
      results << qualification
    end
    @achievements.each do | achievement |
      results << achievement
    end
    @testimonies.each do | testimony |
      results << testimony
    end
    @adjectives.each do | adjective |
      results << adjective
    end

    render json: results.as_json(:only => [:name])
  end

  private
    def authenticate_token!
      auth_user = User.where(authentication_token: params[:authentication_token])
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def qualification_params
      params.require(:qualification).permit()
    end
end
