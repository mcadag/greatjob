class JobsController < ApplicationController
  before_filter :require_login, :only => [:index, :edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token
  respond_to :json, :html

  # GET /jobs
  # GET /jobs.json
  def index
    if current_user.employer?
      company_id = current_user.company_ids
      @jobs = Job.includes(:company).where(company_id: company_id)
    else
      @jobs = Job.includes(:company).page(params[:page]).per(20)
    end
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @jobs, :include => :company }
      end
  end

 def answer
    answered = JobQuestionsAnswer.where(job_id: params[:job_id], user_id: current_user.id, job_question_id: params[:job_question_id]).present? 
    if !answered
      answer = JobQuestionsAnswer.new
      answer[:job_id] = params[:job_id]
      answer[:user_id] = @user.id
      answer[:job_question_id] = params[:job_question_id]
      if params[:video_answer].present?
        answer[:video_answer] = params[:video_answer]
      else
        answer[:answer] = params[:answer]
      end
      if answer.save
        render :json => {:message => "You have answered a question"}
      else
        render :json => {:message => "something went wrong"}
      end
    else
      render :json => {:message => "you have already answered this question"}
    end
  end

  def apply 

    @count = Candidate.where(user_id: current_user.id, job_id: params[:job_id]).limit(1)
    #TODO how do i make this static?
    faye_publisher = Faye::Client.new('http://localhost:9292/faye')
    
    if @count.size > 0
        flash[:error] = "You already Applied to this job"
        redirect_to jobs_path
    else
      @job = Job.includes(:company).find(params[:job_id])
      employee_ids = []
      
      if @job.company
          employee_ids = CompanyEmployee.where(company_id: @job.company.id).pluck(:id)
      end

      @candidate = Candidate.new()
      @candidate.user_id = current_user.id
      @candidate.job_id = params[:job_id]
      if @candidate.save
        @activity = @candidate.create_activity action: 'apply', parameters: {title: current_user.name + ' applied to ' +  @job.company.name +  @job.position,
                                                                    resource_type: 'apply',
                                                                    resource_id:  @job.id,
                                                                    company_id: @job.company.id,
                                                                    company_name: @job.company.name
                                                              }, owner: current_user
        jsonHash = {
           :hello => "goodbye",
           :trackable_type => "Candidate",
           :title => ' applied to ' +  @job.company.name + " as " +  @job.position,
           :owner_name => current_user.name,  
           :action => 'candidate.new',
           :resource_id => @job.id
         }

       path = '/notifications/jobs/' + params[:job_id] + '/candidates'

       faye_publisher.publish(path, JSON.generate(jsonHash))

       flash[:sucess] = "Successfully Applied to " + @job.position
       redirect_to jobs_url
      else
       flash[:error] = "Cant apply"
      #  render :json => {:message => "something went wrong with your application"}
        redirect_to jobs_url
      end
    end
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
    if current_user.employer?
      @applied = Candidate.includes(:job,:user).where("application_type IS NULL OR application_type = :app_type AND job_id = :jobs_id",{app_type: ' ', jobs_id: params[:id]})
      # @applied = Candidate.includes(:job,:user).where(job_id: params[:id], application_type: ' ')
      @short_listed = Candidate.includes(:job,:user).where(job_id: params[:id], application_type: 'short_listed')
      @for_interview = Candidate.includes(:job,:user).where(job_id: params[:id], application_type: 'interview')
      @hired =  Candidate.includes(:job,:user).where(job_id: params[:id], application_type: 'hired')
    else
      @job = Job.find(params[:id])
    end
  
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @job }
    end
  end

  # GET /jobs/new
  # GET /jobs/new.json
  def new
    @job = Job.new
    @company_ids = CompanyEmployee.where(user_id: current_user.id).pluck(:company_id)
    @company = Company.where(id: @company_ids)
    @all_work_experiences = WorkExperience.all
    # @job_work_experiences = @job.work_experiences.build
    @job_titles =  StoredProfession.pluck(:name)
    gon.job_titles = @job_titles
    respond_to do |format|
      format.html
    end
  end

  # GET /jobs/1/edit
  def edit
    @job = Job.find(params[:id])
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new
    @company = Company.where(user_id: current_user.id)
    @job_work_experiences = @job.work_experiences.build
    @job.position = params[:job][:position]
    @job.description = params[:job][:description]
    @job.details = params[:job][:details]
    @job.company_id = params[:job][:company_id]
    @job.location = params[:job][:location]
    @job.city = params[:job][:city]
    if @job.save
      if params[:questions]
        params[:questions].each do | question |
          question = JobQuestions.new
          question[:name] = question
          question[:job_id] = @job.id
          question.save
        end
      end
    end

    @all_work_experiences = WorkExperience.all
    @job_titles =  StoredProfession.pluck(:name)
    gon.job_titles = @job_titles
    
    respond_to do |format|
      if @job.save
        flash[:success] = 'Job was successfully created.'
        format.html { render action: 'new'}
        format.json { render json: @job }
      else
        format.html { render action: 'new' }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    @job = Job.find(params[:id])

    respond_to do |format|
      if @job.update_attributes(job_params)
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job = Job.find(params[:id])
    @job.destroy

    respond_to do |format|
      format.html { redirect_to jobs_url }
      format.json { head :no_content }
    end
  end

  def candidates
    @result = []
    @candidates = Candidate.where(job_id: params[:job_id])
    @candidates.each do | candidate |
      user = User.where(id: candidate.user_id).first
      candidate[:user] = user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
      candidate[:short_list_url] = api_job_short_list_candidate_url(params[:job_id], candidate.id)
      candidate[:profile_url] = user_url(candidate.user_id)
      @result << candidate.as_json(:only => [:id, :user, :job_id, :short_list_url, :profile_url])
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @result }
    end
  end


  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def job_params
      params.require(:job).permit(:avatar, :banner, :company_id, :description, :details, :position, :poster, :user_id)
    end

    def authenticate_token_for_user!
      auth_user = User.where(authentication_token: params[:authentication_token])
      @user = auth_user[0]
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

    def require_login
      unless current_user
        redirect_to new_user_registration_url, :alert => "Please login or signup"
      end
    end

end
