require 'spec_helper'

describe "stored_professions/index" do
  before(:each) do
    assign(:stored_professions, [
      stub_model(StoredProfession,
        :name => "Name"
      ),
      stub_model(StoredProfession,
        :name => "Name"
      )
    ])
  end

  it "renders a list of stored_professions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
