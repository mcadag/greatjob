class AddDirectUploadUrlToVideos < ActiveRecord::Migration
  def change
  	add_column :videos, :direct_url, :string, null: false
  end
end
