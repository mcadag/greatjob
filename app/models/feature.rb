class Feature < ActiveRecord::Base
  attr_accessible :content, :feature_type, :name
end
