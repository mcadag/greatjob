require 'spec_helper'

describe "stored_achievements/show" do
  before(:each) do
    @stored_achievement = assign(:stored_achievement, stub_model(StoredAchievement,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
