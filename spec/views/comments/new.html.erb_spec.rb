require 'spec_helper'

describe "comments/new" do
  before(:each) do
    assign(:comment, stub_model(Comment,
      :user_id => 1,
      :nominee_id => 1,
      :video_id => 1,
      :message => "MyString",
      :details => "MyString",
      :description => "MyString",
      :approved => false,
      :private => false
    ).as_new_record)
  end

  it "renders new comment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", comments_path, "post" do
      assert_select "input#comment_user_id[name=?]", "comment[user_id]"
      assert_select "input#comment_nominee_id[name=?]", "comment[nominee_id]"
      assert_select "input#comment_video_id[name=?]", "comment[video_id]"
      assert_select "input#comment_message[name=?]", "comment[message]"
      assert_select "input#comment_details[name=?]", "comment[details]"
      assert_select "input#comment_description[name=?]", "comment[description]"
      assert_select "input#comment_approved[name=?]", "comment[approved]"
      assert_select "input#comment_private[name=?]", "comment[private]"
    end
  end
end
