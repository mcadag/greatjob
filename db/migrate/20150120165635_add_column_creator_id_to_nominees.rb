class AddColumnCreatorIdToNominees < ActiveRecord::Migration
  def change
    add_column :nominees, :creator_id, :integer
  end
end
