require "spec_helper"

describe SmsgatewaysController do
  describe "routing" do

    it "routes to #index" do
      get("/smsgateways").should route_to("smsgateways#index")
    end

    it "routes to #new" do
      get("/smsgateways/new").should route_to("smsgateways#new")
    end

    it "routes to #show" do
      get("/smsgateways/1").should route_to("smsgateways#show", :id => "1")
    end

    it "routes to #edit" do
      get("/smsgateways/1/edit").should route_to("smsgateways#edit", :id => "1")
    end

    it "routes to #create" do
      post("/smsgateways").should route_to("smsgateways#create")
    end

    it "routes to #update" do
      put("/smsgateways/1").should route_to("smsgateways#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/smsgateways/1").should route_to("smsgateways#destroy", :id => "1")
    end

  end
end
