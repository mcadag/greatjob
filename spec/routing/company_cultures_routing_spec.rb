require "spec_helper"

describe CompanyCulturesController do
  describe "routing" do

    it "routes to #index" do
      get("/company_cultures").should route_to("company_cultures#index")
    end

    it "routes to #new" do
      get("/company_cultures/new").should route_to("company_cultures#new")
    end

    it "routes to #show" do
      get("/company_cultures/1").should route_to("company_cultures#show", :id => "1")
    end

    it "routes to #edit" do
      get("/company_cultures/1/edit").should route_to("company_cultures#edit", :id => "1")
    end

    it "routes to #create" do
      post("/company_cultures").should route_to("company_cultures#create")
    end

    it "routes to #update" do
      put("/company_cultures/1").should route_to("company_cultures#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/company_cultures/1").should route_to("company_cultures#destroy", :id => "1")
    end

  end
end
