class AddPlaceGroupToTestimonies < ActiveRecord::Migration
  def change
    add_column :testimonies, :place_group_id, :integer , foreign_key: true
    add_index :testimonies, :place_group_id
  end
end
