require 'spec_helper'

describe "company_cultures/new" do
  before(:each) do
    assign(:company_culture, stub_model(CompanyCulture,
      :name => "MyString",
      :company_id => 1,
      :user_id => 1
    ).as_new_record)
  end

  it "renders new company_culture form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", company_cultures_path, "post" do
      assert_select "input#company_culture_name[name=?]", "company_culture[name]"
      assert_select "input#company_culture_company_id[name=?]", "company_culture[company_id]"
      assert_select "input#company_culture_user_id[name=?]", "company_culture[user_id]"
    end
  end
end
