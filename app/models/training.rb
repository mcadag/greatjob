class Training < ActiveRecord::Base
	include PublicActivity::Model
  	tracked
	belongs_to :user
  	attr_accessible :company_name, :description, :details, :end_date, :name, :other_details, :start_date
end
