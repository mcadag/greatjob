class CompanyCulture < ActiveRecord::Base
	include PublicActivity::Model
  tracked
  attr_accessible :company_id, :name, :user_id
  belongs_to :companies
  belongs_to :stored_company_culture
end
