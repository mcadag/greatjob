  class TestimoniesController < ApplicationController
    before_filter :set_testimony, only: [:edit, :update, :destroy]
    before_filter :set_user, only: [:create]
    before_filter :require_login, :only => [:index, :edit, :update, :destroy]
    respond_to :json, :html

    def index
      @results = []
      @testimonies = Testimony.where(user_id: params[:user_id]).latest
      @testimonies.each do |testimony|
        @video = Video.where(resource_id: testimony.id, resource_type: 'testimony')
        if @video.exists?
          testimony[:video] = @video.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])[0]
          @results << testimony
        end
      end

      gon.current_user_testimonies = @results

      respond_to do |format|
        format.html
        format.json { render :json => @results.as_json() }
      end
    end

    def show
      @comment = Comment.new 
      @testimony = Testimony.find(params[:id])
      @video = Video.testimony(@testimony.video_id)
      # @comments = Comment.where(video_id: @testimony.video_id).order('created_at ASC').page(params[:page])
      @comments = Comment.order('created_at DESC').paginate(:per_page => 5, :page => params[:page])
      @user = User.where(id: @testimony.user_id)
      @testimony[:payload_mobile_url] = get_video_for_testimony(@testimony)
      @testimony[:payload_thumb_url] = get_thumb_for_testimony(@testimony)
      @testimony[:payload_swf_url] = get_swf_for_testimony(@testimony)
      set_meta_tags :og => {
        :title => "Recommended by Famous!",
        :description => 'Famous.ph, the video-powered recruitment app for skilled technical and vocational workers. Sign up now!', 
        :url => user_testimony_url(params[:id]),
        :type => "video",
        :image => @testimony[:payload_thumb_url],
        :video => @testimony[:payload_swf_url],
      }

      respond_to do |format|
        format.html
      end
    end
  
    def new
      @testimony = Testimony.new
      respond_to do |format|
        format.html
        format.json { render :json => @testimony.as_json() }
      end
    end
  
    def edit
    end
  
    def create
      @testimony = Testimony.new
      @testimony.name = params[:testimony][:name]
      @testimony.description = params[:testimony][:description]
      @testimony.user_id = params[:user_id]
      @testimony.created_by_id = @user.id
      if @testimony.save
        video = Video.new
        video.payload = params[:video][:payload]
        video.name = params[:testimony][:name]
        video.resource_type = "testimony"
        video.stars = 0
        video.testimonies = 0
        video.like_count = 0
        video.views_count = 0
        video.comments_count = 0
        video.description = params[:testimony][:description]
        video.created_by = @user.id
        video.resource_id = @testimony.id
        if video.save
          @testimony.update_attributes(:video_id => video.id )
        end
      end
      account_given = User.select("name, id, email").where(id: params[:user_id]).first

      if account_given.id != @user.id
        NotificationsMailer.testified(account_given, @user, @testimony).deliver
      end

      respond_to do |format|
        format.html { redirect_to user_path(@testimony.user_id), notice: "You have added a new Testimonial." }
        format.json { render :json=> @testimony.as_json(:user_id=>@testimony.user_id, :status=>201), :status=>201 }
      end
    end
  
    def confirm
      
    end

    def update
      @testimony.update_attributes(params.require(:testimony).permit!)
      render :json => testimony.as_json(:status => 201, :message => "Updated"), status => 201
    end
  
    def destroy
      @video = Video.where(resource_id: @testimony.id, resource_type: "testimony")
      video_id = @testimony.video_id
      if @testimony.destroy && @testimony.user_id == @user.id
        @video.destroy(video_id)
        render :json => {:message => "Deleted"}
      else
        render :json => {:message => "You are not authorized to delete the resource"}
      end
    end

    def permission
      testimony = Testimony.where(id: params[:testimony_id])
      testimony.update_attributes(params[:testimony])
      render :json => testimony.as_json(:status => 201, :message => "Permission is set"), status => 201
    end

    def get_video_for_testimony(testimony)
      video = Video.where(id: testimony.video_id)
      testimony[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
      if video.exists?
        pre_vid = JSON.parse(testimony[:video])
        return pre_vid["payload_mobile_url"]
      else
        return
      end
    end

    def get_swf_for_testimony(testimony)
      video = Video.where(id: testimony.video_id)
      testimony[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_swf_url])
      if video.exists?
        pre_vid = JSON.parse(testimony[:video])
        return pre_vid["payload_swf_url"]
      else
        return
      end
    end

    
    def get_thumb_for_testimony(testimony)
      video = Video.where(id: testimony.video_id)
      testimony[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
      if video.exists?
        pre_vid = JSON.parse(testimony[:video])
        return pre_vid["payload_thumb_url"]
      else
        return
      end
    end
  
    private
      def set_testimony
        @testimony = Testimony.find(params[:id])
      end

      def set_user
        auth_user = User.where(id: params[:user_id])
        @user = auth_user[0]
      end
      
      def authenticate_token!
        auth_user = User.where(authentication_token: params[:authentication_token])
        if auth_user.count >= 1
          @user = auth_user[0]
          true
        else
          render :json => {:message => "your authentication token is not valid"}
        end
      end
  
      def require_login
        unless current_user
          redirect_to new_user_registration_url, :alert => "Please login or signup"
        end
      end

end