module Api
  class InterestsController < ApplicationController
    before_filter :set_interest, only: [:show, :edit, :update, :destroy]
    before_filter :authenticate_token!, :only => [:create, :update, :destroy, :approve, :reject]
    respond_to :json
    
    def index
      interests = []
      results = Interest.my(params[:user_id]).latest
      results.each do |interest|
        video = Video.interests(interest.id)
        if video.count >= 1
          interest[:video] = video.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
          interest[:facebook_share_url] = user_interest_url(interest.user_id, interest.id)
          interests << interest          
        end
      end

      respond_with(interests)
    end

    def show
      interest = Interest.where(id: params[:id])
      render json: interest.as_json()
    end

    def new
      @interest = Interest.new
      respond_with(@interest)
    end

    def edit
    end

    def create
      interest = Interest.new
      interest.name = params[:interest][:name]
      interest.description = params[:interest][:description]
      interest.user_id = params[:user_id]
      interest.created_by_id = @user.id
      if @user.id == params[:user_id]
        interest.permission = "allow"
      end
      if interest.save
        User.update_counters @user.id, star_points: 5
        video = Video.new
        video.payload = params[:video][:payload]
        video.name = params[:video][:title]
        video.resource_type = "skill"
        video.stars = 0
        video.testimonies = 0
        video.like_count = 0
        video.views_count = 0
        video.comments_count = 0
        video.description = params[:video][:description]
        video.created_by = @user.id
        video.resource_id = interest.id
        if video.save
          interest.update_attributes(:video_id => video.id)
          creator = User.where(id: @user.id).limit(1)
          uploaded_to_user = User.where(id: interest.user_id).limit(1)
          video_a = Video.where(id: video.id).limit(1)
          if creator[0].id != params[:user_id]
            NotificationsMailer.new_skill_added(uploaded_to_user[0], creator[0], interest, video_a[0].payload.url, video_a[0].payload(:thumb)).deliver
          end
          recipient = User.where(id: params[:user_id]).first
          render :json=> interest.as_json(:user_id=>interest.user_id, :status=>201, :message=>"Interest Added"), :status=>201
        end
      end
    end

    def update
      @interest.update_attributes(params.require(:interest).permit!)
      render :json => interest.as_json(:status => 201, :message => "Updated"), status => 201
    end

    def destroy
      @video = Video.interests(@interest.id)
      video_id = @interest.video_id
      if @interest.user_id == @user.id
        @interest.destroy
        @video.destroy(video_id)
        render :json => {:message => "Deleted"}
      else
        render :json => {:message => "You are not authorized to delete the resource"}, :status => :bad_request
      end
      
    end

    def approve
      @interest = Interest.find(params[:interest_id])
      @interest.update_attributes(permission: "allow")
      render :json => @interest.as_json(:status => 201, :message => "Updated"), status => 201
    end

    def reject
      @interest = Interest.find(params[:interest_id])
      @interest.update_attributes(permission: "denied")
      render :json => @interest.as_json(:status => 201, :message => "Updated"), status => 201
    end

    private
      def set_interest
        @interest = Interest.find(params[:id])
      end
      
      def authenticate_token!
      auth_user = User.where(authentication_token: params[:authentication_token])
      @user = auth_user[0]
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

  end
end