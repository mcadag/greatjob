class AddHasVideoParamsToComments < ActiveRecord::Migration
  def change
    add_column :comments, :has_video, :boolean
  end
end
