class CompanyDreamJobsController < ApplicationController
  # GET /company_dream_jobs
  # GET /company_dream_jobs.json
  def index
    @company_dream_jobs = CompanyDreamJob.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @company_dream_jobs }
    end
  end

  # GET /company_dream_jobs/1
  # GET /company_dream_jobs/1.json
  def show
    @company_dream_job = CompanyDreamJob.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @company_dream_job }
    end
  end

  # GET /company_dream_jobs/new
  # GET /company_dream_jobs/new.json
  def new
    @company_dream_job = CompanyDreamJob.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @company_dream_job }
    end
  end

  # GET /company_dream_jobs/1/edit
  def edit
    @company_dream_job = CompanyDreamJob.find(params[:id])
  end

  # POST /company_dream_jobs
  # POST /company_dream_jobs.json
  def create
    @company_dream_job = CompanyDreamJob.new(company_dream_job_params)

    respond_to do |format|
      if @company_dream_job.save
        format.html { redirect_to @company_dream_job, notice: 'Company dream job was successfully created.' }
        format.json { render json: @company_dream_job, status: :created, location: @company_dream_job }
      else
        format.html { render action: "new" }
        format.json { render json: @company_dream_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company_dream_jobs/1
  # PATCH/PUT /company_dream_jobs/1.json
  def update
    @company_dream_job = CompanyDreamJob.find(params[:id])

    respond_to do |format|
      if @company_dream_job.update_attributes(company_dream_job_params)
        format.html { redirect_to @company_dream_job, notice: 'Company dream job was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @company_dream_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company_dream_jobs/1
  # DELETE /company_dream_jobs/1.json
  def destroy
    @company_dream_job = CompanyDreamJob.find(params[:id])
    @company_dream_job.destroy

    respond_to do |format|
      format.html { redirect_to company_dream_jobs_url }
      format.json { head :no_content }
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def company_dream_job_params
      params.require(:company_dream_job).permit(:comapny_id, :name, :user_id)
    end
end