require 'spec_helper'

describe "stored_trainings/new" do
  before(:each) do
    assign(:stored_training, stub_model(StoredTraining,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new stored_training form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stored_trainings_path, "post" do
      assert_select "input#stored_training_name[name=?]", "stored_training[name]"
    end
  end
end
