require 'spec_helper'

describe "stored_professions/new" do
  before(:each) do
    assign(:stored_profession, stub_model(StoredProfession,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new stored_profession form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stored_professions_path, "post" do
      assert_select "input#stored_profession_name[name=?]", "stored_profession[name]"
    end
  end
end
