class Key < ActiveRecord::Base
  attr_accessible :name, :value, :flag_value, :flag_type, :other
  validates :name, :uniqueness => { :case_sensitive => false }
end
