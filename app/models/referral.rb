class Referral < ActiveRecord::Base
  belongs_to :company
  belongs_to :job
  attr_accessible :created_by, :created_to
end
