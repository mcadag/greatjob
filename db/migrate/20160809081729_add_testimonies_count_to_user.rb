class AddTestimoniesCountToUser < ActiveRecord::Migration
  def change
    add_column :users, :testimonies_count, :integer
  end
end
