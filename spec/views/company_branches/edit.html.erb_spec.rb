require 'spec_helper'

describe "company_branches/edit" do
  before(:each) do
    @company_branch = assign(:company_branch, stub_model(CompanyBranch,
      :name => "MyString",
      :comapny_id => 1,
      :branch_id => 1,
      :user_id => 1
    ))
  end

  it "renders the edit company_branch form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", company_branch_path(@company_branch), "post" do
      assert_select "input#company_branch_name[name=?]", "company_branch[name]"
      assert_select "input#company_branch_comapny_id[name=?]", "company_branch[comapny_id]"
      assert_select "input#company_branch_branch_id[name=?]", "company_branch[branch_id]"
      assert_select "input#company_branch_user_id[name=?]", "company_branch[user_id]"
    end
  end
end
