require "spec_helper"

describe PlaceGroupsController do
  describe "routing" do

    it "routes to #index" do
      get("/place_groups").should route_to("place_groups#index")
    end

    it "routes to #new" do
      get("/place_groups/new").should route_to("place_groups#new")
    end

    it "routes to #show" do
      get("/place_groups/1").should route_to("place_groups#show", :id => "1")
    end

    it "routes to #edit" do
      get("/place_groups/1/edit").should route_to("place_groups#edit", :id => "1")
    end

    it "routes to #create" do
      post("/place_groups").should route_to("place_groups#create")
    end

    it "routes to #update" do
      put("/place_groups/1").should route_to("place_groups#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/place_groups/1").should route_to("place_groups#destroy", :id => "1")
    end

  end
end
