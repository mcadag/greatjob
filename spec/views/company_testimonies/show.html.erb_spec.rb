require 'spec_helper'

describe "company_testimonies/show" do
  before(:each) do
    @company_testimony = assign(:company_testimony, stub_model(CompanyTestimony,
      :name => "Name",
      :description => "Description",
      :user_id => 1,
      :company_id => 2,
      :video_id => 3,
      :created_by => 4,
      :allowed => false,
      :private => false,
      :permission => "Permission"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Description/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/4/)
    rendered.should match(/false/)
    rendered.should match(/false/)
    rendered.should match(/Permission/)
  end
end
