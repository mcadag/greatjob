class AddMoreParamsToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :location, :string
    add_column :companies, :website, :string
    add_column :companies, :email, :string
    add_column :companies, :mobile_number, :string
  end
end
