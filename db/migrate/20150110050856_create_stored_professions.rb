class CreateStoredProfessions < ActiveRecord::Migration
  def change
    create_table :stored_professions do |t|
      t.string :name

      t.timestamps
    end
  end
end
