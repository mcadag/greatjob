class CreateTestimonies < ActiveRecord::Migration
  def change
    create_table :testimonies do |t|
      t.string :name
      t.integer :user_id
      t.integer :video_id
      t.string :description

      t.timestamps
    end
  end
end
