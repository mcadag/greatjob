class Notification < ActiveRecord::Base
  
  scope :unread, -> { where(checked: false) }

  self.inheritance_column = "not_sti"
  belongs_to :user
  attr_accessible :checked, :message, :topic_id, :type
  
end
