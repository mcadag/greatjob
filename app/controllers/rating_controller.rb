class RatingController < ApplicationController
  # GET /adjectives
  # GET /adjectives.json
  def index
    @rating = Rating.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @rating }
    end
  end

  # GET /adjectives/1
  # GET /adjectives/1.json
  def show
    @rating = Rating.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @rating }
    end
  end

  def update
  end

  # GET /adjectives/new
  # GET /adjectives/new.json
  def new
    @rating = Rating.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @rating }
    end
  end

  # GET /adjectives/1/edit
  def edit
    @rating = Rating.find(params[:id])
  end

  # POST /adjectives
  # POST /rating :rate :candidate_id :job_id
  def create
    if current_user.employer?
      @rating = Rating.where(:user_id => current_user.id, :job_id => params[:job_id])
      respond_to do |format|
        if @rating.first_or_create(:rate => params[:rate],:user_id => current_user.id,:candidate_id => params[:candidate_id],:job_id => params[:job_id])
          format.html { redirect_to @rating, notice: 'Rating was successfully updated.' }
          format.json { head :no_content }
        else
          format.json { render json: @rating.errors, status: :unprocessable_entity }
        end
      end
      end
    end
end