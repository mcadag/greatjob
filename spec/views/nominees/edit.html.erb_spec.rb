require 'spec_helper'

describe "nominees/edit" do
  before(:each) do
    @nominee = assign(:nominee, stub_model(Nominee,
      :user_id => 1,
      :competition_id => 1,
      :video_id => 1
    ))
  end

  it "renders the edit nominee form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", nominee_path(@nominee), "post" do
      assert_select "input#nominee_user_id[name=?]", "nominee[user_id]"
      assert_select "input#nominee_competition_id[name=?]", "nominee[competition_id]"
      assert_select "input#nominee_video_id[name=?]", "nominee[video_id]"
    end
  end
end
