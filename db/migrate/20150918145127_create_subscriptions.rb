class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :stored_profession
      t.references :user

      t.timestamps
    end
    add_index :subscriptions, :stored_profession_id
    add_index :subscriptions, :user_id
  end
end
