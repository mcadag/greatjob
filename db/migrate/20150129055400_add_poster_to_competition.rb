class AddPosterToCompetition < ActiveRecord::Migration
  def change
  	add_attachment :competitions, :poster
  end
end
