class Competition < ActiveRecord::Base
  include PublicActivity::Model
  tracked
  attr_accessible :company_id, :description, :end_date, :name, :prize_id, :start_date, :user_id, :year, :superlative, :location, :poster, :poster_file_name, :is_private
  has_many :nominees
  has_many :competitionQualifications
  acts_as_followable

    has_attached_file :poster,
           :styles => { :medium => "x300", :thumb => "x100" },
           :storage => :s3,
           :s3_permissions => :private,
           :s3_credentials => "#{Rails.root}/config/aws.yml"
  	do_not_validate_attachment_file_type :poster
  
  def poster_url
        poster.url(:medium)
  end

end

