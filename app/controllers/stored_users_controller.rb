class StoredUsersController < ApplicationController
  # GET /stored_users
  # GET /stored_users.json
  def index
    @stored_users = StoredUser.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stored_users }
    end
  end

  # GET /stored_users/1
  # GET /stored_users/1.json
  def show
    @stored_user = StoredUser.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stored_user }
    end
  end

  # GET /stored_users/new
  # GET /stored_users/new.json
  def new
    @stored_user = StoredUser.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stored_user }
    end
  end

  # GET /stored_users/1/edit
  def edit
    @stored_user = StoredUser.find(params[:id])
  end

  # POST /stored_users
  # POST /stored_users.json
  def create
    @stored_user = StoredUser.new(stored_user_params)

    respond_to do |format|
      if @stored_user.save
        format.html { redirect_to @stored_user, notice: 'Stored user was successfully created.' }
        format.json { render json: @stored_user, status: :created, location: @stored_user }
      else
        format.html { render action: "new" }
        format.json { render json: @stored_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stored_users/1
  # PATCH/PUT /stored_users/1.json
  def update
    @stored_user = StoredUser.find(params[:id])

    respond_to do |format|
      if @stored_user.update_attributes(stored_user_params)
        format.html { redirect_to @stored_user, notice: 'Stored user was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stored_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stored_users/1
  # DELETE /stored_users/1.json
  def destroy
    @stored_user = StoredUser.find(params[:id])
    @stored_user.destroy

    respond_to do |format|
      format.html { redirect_to stored_users_url }
      format.json { head :no_content }
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def stored_user_params
      params.require(:stored_user).permit(:age, :brgy, :country, :education, :education, :email, :first_name, :gender, :last_name, :middle_initial, :mobile_number, :province, :town, :training, :training_end, :training_start)
    end
end
