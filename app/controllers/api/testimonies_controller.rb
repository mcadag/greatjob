module Api
  class TestimoniesController < ApplicationController
    before_filter :set_testimony, only: [:show, :edit, :update, :destroy]
    before_filter :authenticate_token!, :only => [:index, :create, :update, :destroy, :index, :approve, :reject]
    respond_to :json

    def index
      results = []
      @testimonies = Testimony.my(params[:user_id]).latest
      render :json => @testimonies.as_json(:include => { :video => {
                             :include => { :user => {
                                             :only => [:name,:id],:methods => [:avatar_url] } },
                             :methods =>  [:payload_url, :payload_mobile_url, :payload_thumb_url]} })
    end
  
    def gallery
      results = []
      testimonies = Testimony.my(params[:user_id]).where(created_by_id: params[:user_id]).latest
      testimonies.each do |testimony|
        video = Video.testimony(testimony.id)
          if video.count >= 1
            testimony[:video] = video.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
            testimony[:facebook_share_url] = user_testimony_url(testimony.user_id, testimony.id)
            results << testimony
          end
      end
      render :json => results.as_json()
    end

    def testimonials
      results = []
      testimonies = Testimony.my(params[:user_id]).where("testimonies.created_by_id != ?", params[:user_id]).latest
      testimonies.each do |testimony|
        video = Video.testimony(testimony.id)
          if video.count >= 1
            testimony[:video] = video.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
            testimony[:facebook_share_url] = user_testimony_url(testimony.user_id, testimony.id)
            results << testimony
          end
        end
      render :json => results.as_json()
    end

    def show
      respond_with(@testimony)
    end
  
    def new
      @testimony = Testimony.new
      respond_with(@testimony)
    end
  
    def edit
    end
  
    def create
      @testimony = Testimony.new
      @testimony.name = params[:testimony][:name]
      @testimony.description = params[:testimony][:description]
      @testimony.user_id = params[:user_id]
      @testimony.created_by_id = @user.id
      if @user.id == params[:user_id]
        @testimony.permission = "allow"
      end
      if @testimony.save
        video = Video.new
        video.payload = params[:video][:payload]
        video.name = params[:video][:title]
        video.resource_type = "testimony"
        video.stars = 0
        video.testimonies = 0
        video.like_count = 0
        video.views_count = 0
        video.comments_count = 0
        video.description = params[:video][:description]
        video.created_by = @user.id
        video.user_id = @user.id
        video.resource_id = @testimony.id
        if video.save
          
          User.update_counters @user.id, star_points: 5
          @testimony.update_attributes(:video_id => video.id )
          @creator = User.find(@user.id)
          @uploaded_to_user = User.find(@testimony.user_id)
          @video_a = Video.find(video.id)

          @user.follow!(@uploaded_to_user)
          
          if @creator.id != params[:user_id]
            Pusher.trigger('users-' + params[:user_id], 'testimony.create', { message: @user.name + ' posted a video recommendation on your profile ', testimony: @testimony.as_json(), video: @video_a.as_json() } )
            @message = @user.name + ' posted a video recommendation on ' + @uploaded_to_user.name
            #TODO EMAIL
            # NotificationsMailer.testified(uploaded_to_user[0], creator[0], @testimony, video_a[0].payload.url, video_a[0].payload(:thumb)).deliver
          else
            @message = @user.name + ' posted added a new video'  
          end
          
          @testimony.create_activity :create, owner: @user, recipient: @uploaded_to_user, parameters: { message: @message, from: @user.as_json(:methods => :avatar_url, :except => [:authentication_token, :email] ), testimony: @testimony.as_json(), video: @video_a.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url]) }

          respond_to do |format|
            format.json { render :json=> @testimony.as_json(), :status=>201 }
          end
        end
      end
    end
  
    def update
      @testimony.update_attributes(params.require(:testimony).permit!)
      render :json => testimony.as_json(:status => 201, :message => "Updated"), status => 201
    end
  
    def destroy
      @video = Video.testimony(@testimony.id)
      video_id = @testimony.video_id
      if @testimony.user_id == @user.id
        @testimony.destroy 
        @video.destroy(video_id)
        render :json => {:message => "Deleted"}
      else
        render :json => {:message => "You are not authorized to delete the resource"}, :status => :bad_request
      end
    end

    def approve
      @testimony = Testimony.find(params[:testimony_id])
      @testimony.update_attributes(permission: "allow")
      render :json => @testimony.as_json(:status => 201, :message => "Updated"), status => 201
    end

    def reject
      @testimony = Testimony.find(params[:testimony_id])
      @testimony.update_attributes(permission: "denied")
      render :json => @testimony.as_json(:status => 201, :message => "Updated"), status => 201
    end

    private
      def set_testimony
        @testimony = Testimony.find(params[:id])
      end
      
      def authenticate_token!
      auth_user = User.where(authentication_token: params[:authentication_token])
      if auth_user.count >= 1
        @user = auth_user[0]
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

  end
end


      # device = Device.where(user_id: 2).first
      # Urbanairship.register_device(device.unique_identifier, :provider => :android)
      # notification = {
      #   :segments => [device.unique_identifier],
      #   :apids => [device.unique_identifier],
      #   :android => {
      #     :alert => 'You have a new message!'
      #   }
      # }
      # Urbanairship.push(notification)
