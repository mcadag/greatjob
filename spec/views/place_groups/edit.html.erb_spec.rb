require 'spec_helper'

describe "place_groups/edit" do
  before(:each) do
    @place_group = assign(:place_group, stub_model(PlaceGroup))
  end

  it "renders the edit place_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", place_group_path(@place_group), "post" do
    end
  end
end
