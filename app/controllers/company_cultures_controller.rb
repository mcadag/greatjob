
class CompanyCulturesController < ApplicationController
  # GET /company_cultures
  # GET /company_cultures.json
  def index
    @company_cultures = CompanyCulture.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @company_cultures }
    end
  end

  # GET /company_cultures/1
  # GET /company_cultures/1.json
  def show
    @company_culture = CompanyCulture.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @company_culture }
    end
  end

  # GET /company_cultures/new
  # GET /company_cultures/new.json
  def new
    @company_culture = CompanyCulture.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @company_culture }
    end
  end

  # GET /company_cultures/1/edit
  def edit
    @company_culture = CompanyCulture.find(params[:id])
  end

  # POST /company_cultures
  # POST /company_cultures.json
  def create
    @company_culture = CompanyCulture.new(company_culture_params)

    respond_to do |format|
      if @company_culture.save
        format.html { redirect_to @company_culture, notice: 'Company culture was successfully created.' }
        format.json { render json: @company_culture, status: :created, location: @company_culture }
      else
        format.html { render action: "new" }
        format.json { render json: @company_culture.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company_cultures/1
  # PATCH/PUT /company_cultures/1.json
  def update
    @company_culture = CompanyCulture.find(params[:id])

    respond_to do |format|
      if @company_culture.update_attributes(company_culture_params)
        format.html { redirect_to @company_culture, notice: 'Company culture was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @company_culture.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company_cultures/1
  # DELETE /company_cultures/1.json
  def destroy
    @company_culture = CompanyCulture.find(params[:id])
    @company_culture.destroy

    respond_to do |format|
      format.html { redirect_to company_cultures_url }
      format.json { head :no_content }
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def company_culture_params
      params.require(:company_culture).permit(:company_id, :name, :user_id)
    end
end
