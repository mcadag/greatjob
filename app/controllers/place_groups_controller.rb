class PlaceGroupsController < ApplicationController
  # GET /place_groups
  # GET /place_groups.json
  before_filter :set_place_group, only: [:join_group, :leave_group]

  def index
  
    @place_groups  = current_user.place_groups
  
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @place_groups }
    end
    
  end

  # GET /place_groups/1
  # GET /place_groups/1.json
  def show
    @place_group = PlaceGroup.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @place_group }
    end
  end

  def discovery
    @location = Geokit::Geocoders::IpGeocoder.geocode(current_user.last_sign_in_ip)
    @place_groups = PlaceGroup.all
     respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @place_group }
    end
  end

  def leave_group
    @group_member = @place_group.group_members.where(user: current_user)
    respond_to do |format|
      if @group_member.destroy!
        format.html # index.html.erb
        format.json { render json: @place_groups }
      else
        format.html { render action: "index" }
        format.json { render json: @place_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def join_group
    @group_member = GroupMember.new(place_group: @place_group, user: current_user);
    respond_to do |format|
      if @place_group.group_members << group_member
        format.html # index.html.erb
        format.json { render json: @place_groups }
      else
        format.html { render action: "index" }
        format.json { render json: @place_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /place_groups/new
  # GET /place_groups/new.json
  def new
    @place_group = PlaceGroup.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @place_group }
    end
  end

  # GET /place_groups/1/edit
  def edit
    @place_group = PlaceGroup.find(params[:id])
  end

  # POST /place_groups
  # POST /place_groups.json
  def create
    @place_group = PlaceGroup.new(place_group_params)
    @place_group.user = current_user
    current_user.add_role :admin, @place_group
    respond_to do |format|
      if @place_group.save
        #bug on rails version
        gm = GroupMember.new(place_group_id: @place_group.id, user_id: current_user.id)
        gm.confirmed = true
        gm.save!
        format.html { redirect_to @place_group, notice: 'Place group was successfully created.' }
        format.json { render json: @place_group, status: :created, location: @place_group }
      else
        format.html { render action: "new" }
        format.json { render json: @place_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /place_groups/1
  # PATCH/PUT /place_groups/1.json
  def update
    @place_group = PlaceGroup.find(params[:id])

    respond_to do |format|
      if @place_group.update_attributes(place_group_params)
        format.html { redirect_to @place_group, notice: 'Place group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @place_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /place_groups/1
  # DELETE /place_groups/1.json
  def destroy
    @place_group = PlaceGroup.find(params[:id])
    @place_group.destroy

    respond_to do |format|
      format.html { redirect_to place_groups_url }
      format.json { head :no_content }
    end
  end

  private
    def set_place_group
      @place_group = PlaceGroup.find(params[:id])
    end
    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def place_group_params
      params.require(:place_group).permit(:name, :place_id, :lat, :lng, :address)
    end
end
