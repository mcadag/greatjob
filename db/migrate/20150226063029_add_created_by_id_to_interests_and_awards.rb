class AddCreatedByIdToInterestsAndAwards < ActiveRecord::Migration
  def change
  	add_column :interests, :created_by_id, :integer
  	add_column :awards, :created_by_id, :integer
  end
end
