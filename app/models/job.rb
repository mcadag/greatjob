class Job < ActiveRecord::Base
  include PublicActivity::Model
  tracked
  attr_accessible :stored_profession, :avatar, :banner, :company_id, :description, :details, :position, :poster, :user_id, :city, :country, :salary, :license, :location, :application_type, :work_experiences
  belongs_to :company
  has_many :users, through: :candidates
  has_many :work_experiences, through: :job_work_experience
  has_many :job_work_experience

  validates :position, presence: true
  validates :location, presence: true
  validates :details, presence: true

end
