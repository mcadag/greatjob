require 'spec_helper'

describe "goals/edit" do
  before(:each) do
    @goal = assign(:goal, stub_model(Goal,
      :name => "MyString",
      :user_id => 1,
      :position => "MyString"
    ))
  end

  it "renders the edit goal form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", goal_path(@goal), "post" do
      assert_select "input#goal_name[name=?]", "goal[name]"
      assert_select "input#goal_user_id[name=?]", "goal[user_id]"
      assert_select "input#goal_position[name=?]", "goal[position]"
    end
  end
end
