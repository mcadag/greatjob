require 'spec_helper'

describe "stored_testimonies/edit" do
  before(:each) do
    @stored_testimony = assign(:stored_testimony, stub_model(StoredTestimony,
      :name => "MyString"
    ))
  end

  it "renders the edit stored_testimony form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stored_testimony_path(@stored_testimony), "post" do
      assert_select "input#stored_testimony_name[name=?]", "stored_testimony[name]"
    end
  end
end
