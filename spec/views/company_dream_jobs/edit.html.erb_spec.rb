require 'spec_helper'

describe "company_dream_jobs/edit" do
  before(:each) do
    @company_dream_job = assign(:company_dream_job, stub_model(CompanyDreamJob,
      :name => "MyString",
      :comapny_id => 1,
      :user_id => 1
    ))
  end

  it "renders the edit company_dream_job form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", company_dream_job_path(@company_dream_job), "post" do
      assert_select "input#company_dream_job_name[name=?]", "company_dream_job[name]"
      assert_select "input#company_dream_job_comapny_id[name=?]", "company_dream_job[comapny_id]"
      assert_select "input#company_dream_job_user_id[name=?]", "company_dream_job[user_id]"
    end
  end
end
