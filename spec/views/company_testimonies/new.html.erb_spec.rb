require 'spec_helper'

describe "company_testimonies/new" do
  before(:each) do
    assign(:company_testimony, stub_model(CompanyTestimony,
      :name => "MyString",
      :description => "MyString",
      :user_id => 1,
      :company_id => 1,
      :video_id => 1,
      :created_by => 1,
      :allowed => false,
      :private => false,
      :permission => "MyString"
    ).as_new_record)
  end

  it "renders new company_testimony form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", company_testimonies_path, "post" do
      assert_select "input#company_testimony_name[name=?]", "company_testimony[name]"
      assert_select "input#company_testimony_description[name=?]", "company_testimony[description]"
      assert_select "input#company_testimony_user_id[name=?]", "company_testimony[user_id]"
      assert_select "input#company_testimony_company_id[name=?]", "company_testimony[company_id]"
      assert_select "input#company_testimony_video_id[name=?]", "company_testimony[video_id]"
      assert_select "input#company_testimony_created_by[name=?]", "company_testimony[created_by]"
      assert_select "input#company_testimony_allowed[name=?]", "company_testimony[allowed]"
      assert_select "input#company_testimony_private[name=?]", "company_testimony[private]"
      assert_select "input#company_testimony_permission[name=?]", "company_testimony[permission]"
    end
  end
end
