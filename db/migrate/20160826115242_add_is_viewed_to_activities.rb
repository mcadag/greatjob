class AddIsViewedToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :is_viewed, :boolean , :default => false
  end
end
