class CreateInterviewSchedules < ActiveRecord::Migration
  def change
    create_table :interview_schedules do |t|
      t.integer :user_id
      t.integer :job_id
      t.datetime :schedule
      t.integer :candidate_id
      t.boolean :accept
      t.boolean :reschedule

      t.timestamps
    end
  end
end
