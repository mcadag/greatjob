var app = angular.module('profileHeader',[
	'ui.bootstrap',
	"ngSanitize",
	"com.2fdevs.videogular",
	"com.2fdevs.videogular.plugins.controls",
	"com.2fdevs.videogular.plugins.overlayplay",
	"com.2fdevs.videogular.plugins.poster"
	]);

app.controller('PostProfessionController', ['$scope', function($scope) {

	$scope.init = function() {

	};

	$scope.update = function(user){
		var user_authentication_token = gon.current_user.authentication_token;
		console.log("User professions for Post Profession Controller");
	};
}]);

angular.module('profileHeader').filter('pagination', function()
{
 return function(input, start)
 {
  start = +start;
  return input.slice(start);
 };
});


app.factory('Training', ['$q', '$http', function($q, $http) {
  
	return {
		getTrainings: function(options){
			return $http.get('/api/users/' + gon.current_user.id + '/trainings?authentication_token=' + gon.current_user.authentication_token)
			.then(function (response) {
				console.log(response);
				return response;
			});
              }
	}	
}]);

app.controller('TrainingController',['$scope','Training',  function($scope, Training){
	$scope.trainingsList = [];

	$scope.init = function(){
	}
	$scope.getTrainings = function(options) {
	 	var promise = Training.getTrainings();
	 		console.log(promise); 
       		promise.then(function(trainings) {
				console.log(trainings.data);
        		$scope.trainingsList = trainings.data;
        	},
        	function(error) {
          	});
	}
     
}]);

app.factory('Professions', ['$q', '$http', function($q, $http) {
  
	return {
		getProfessions: function(options){
			return $http.get('/api/users/' + gon.current_user.id + '/professions?authentication_token=' + gon.current_user.authentication_token)
			.then(function (response) {
				console.log(response);
				return response;
			});
              }
	}	
}]);

app.controller('ProfessionController',['$scope','Professions',  function($scope, Professions){
	$scope.professionsList = [];

	$scope.init = function(){
	}
	$scope.getProfessions = function(options) {
	 	var promise = Professions.getProfessions();
	 		console.log(promise); 
       		promise.then(function(professions) {
				console.log(professions.data);
        		$scope.professionsList = professions.data;
        	},
        	function(error) {
          	});
	}
     
}]);

app.factory('Goals', ['$q', '$http', function($q, $http) {
  
	return {
		getGoals: function(options){
			return $http.get('/api/users/' + gon.current_user.id + '/goals?authentication_token=' + gon.current_user.authentication_token)
			.then(function (response) {
				console.log(response);
				console.log("Hello");
				return response;
			});
              }
	}	
}]);

app.controller('GoalsController',['$scope','Goals',  function($scope, Goals){
	$scope.goalsList = [];

	$scope.init = function(){
	}
	$scope.getGoals = function(options) {
	 	var promise = Goals.getGoals();
       		promise.then(function(goals) {
        		$scope.goalsList = goals.data;
        	},
        	function(error) {
          	});
	}
     
}]);

app.factory('Testimonies', ['$q', '$http', function($q, $http) {
  
	return {
		getTestimonies: function(options){
			return $http.get('/api/users/' + gon.current_user.id + '/testimonies?authentication_token=' + gon.current_user.authentication_token)
			.then(function (response) {
				return response;
			});
              }
	}	
	
}]);

app.controller('TestimonyController',['$scope','Testimonies',  function($scope, Testimonies){
	$scope.testimoniesList = [];
	$scope.myInterval = -1;
  	var slides = $scope.slides = [];
  	var log = [];
  	$scope.curPage = 0;
 	$scope.pageSize = 3;
	$scope.init = function(){
	}
	$scope.getTestimonies = function(options) {
	 	var promise = Testimonies.getTestimonies();
       		promise.then(function(testimonies) {
        		$scope.testimoniesList = testimonies.data;
        		angular.forEach(testimonies.data, function(value, key) {
  					slides.push(value);
				}, log);
        	},
        function(error) {
          	});
	}

	 $scope.numberOfPages = function() {
				return Math.ceil($scope.slides.length / $scope.pageSize);
			};
}]);

app.factory('Interests', ['$q', '$http', function($q, $http) {
  
	return {
		getInterests: function(options){
			return $http.get('/api/users/' + gon.current_user.id + '/interests?authentication_token=' +gon.current_user.authentication_token)
			.then(function (response) {
				console.log(response);
				return response;
			});
              }
	}	
	
}]);

app.controller('InterestsController',['$scope','Interests',  function($scope, Interests) {
	$scope.interestsList = [];
	$scope.myInterval = -1;
  	var slides = $scope.slides = [];
  	var log = [];
  	$scope.curPage = 0;
 	$scope.pageSize = 3;
	$scope.init = function(){
	}
	$scope.getInterests = function(options) {
	 	var promise = Interests.getInterests();
       		promise.then(function(interests) {
        		$scope.interestsList = interests.data;
				angular.forEach(interests.data, function(value, key) {
  					slides.push(value);
				}, log);
        	},
        	function(error) {
          	});
	}
	$scope.numberOfPages = function() {
				return Math.ceil($scope.slides.length / $scope.pageSize);
			};
}]);

app.factory('Awards', ['$q', '$http', function($q, $http) {
  
	return {
		getAwards: function(options){
			return $http.get('/api/users/' + gon.current_user.id + '/awards?authentication_token=' +gon.current_user.authentication_token)
			.then(function (response) {
				return response;
			});
              }
	}	
	
}]);

app.controller('AwardsController',['$scope','Awards',  function($scope, Awards){
	$scope.awardsList = [];
    $scope.myInterval = -1;
  	var slides = $scope.slides = [];
  	var log = [];
	$scope.curPage = 0;
 	$scope.pageSize = 3;

	$scope.init = function(){
	}
	$scope.getAwards = function(options) {
	 	var promise = Awards.getAwards();
       		promise.then(function(awards) {
        		$scope.awardsList = awards.data;
				angular.forEach(awards.data, function(value, key) {
  					slides.push(value);
				}, log);
        	},
        	function(error) {
          	});
	}


	 $scope.numberOfPages = function() {
				return Math.ceil($scope.slides.length / $scope.pageSize);
			};
     
}]);

app.controller('VideosController',['$scope','$modal','$sce', '$log',  function($scope, $modal, $sce, $log){
	
	$scope.API = null;
	
	$scope.onPlayerReady = function(API) {
		$scope.API = API;
	};

	$scope.args = {};
	$scope.open = function (video) {
		var payload_url = video.video[0].payload_mobile_url;

		$scope.args.sources = [
			{
				src: $sce.trustAsResourceUrl(payload_url),
				type: "video/mp4"
			}
		];
		$scope.args.theme = '/styles/videogular.css';
		$scope.args.tracks =  [
			{
				src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
				kind: "subtitles",
				srclang: "en",
				label: "English",
				default: ""
			}
		];

		$scope.args.size = 'lg';
		var modalInstance = $modal.open({
			templateUrl: '/templates/fvideomodal.html',
			controller: 'ModalInstanceCtrl',
			size: 'lg',
			resolve: {
				args: function () {
					return $scope.args;
				},

			}
		});


		modalInstance.result.then(function (selectedItem) {
			
		}, function () {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
     
}]);

app.controller('ModalInstanceCtrl', ['$scope','$modalInstance','args',  function ($scope, $modalInstance, args) {

	$scope.args = args;
	$scope.size = args.size;
	$scope.ok = function () {
		$modalInstance.close();
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
}]);

app.directive('fcoverimg', function(){
	return {
	    restrict: "A",
	    scope: {
	      fcoverimg: "@"
	    },
	    link: function (scope, element, attrs, controller) {
	    	console.log(element.css("background-image", "url("+attrs.fcoverimg+")"))
	    	
	    }
	}

});

app.directive('fvideo', function(){
	return {

		templateUrl: '/templates/profileVideo.html'
	}

});

app.controller('CollapseDemoCtrl', function ($scope) {
  $scope.isCollapsed = false;
});

app.controller('MainCtrl', function ($scope) {
    $scope.showModal = false;
    $scope.toggleModal = function(){
        $scope.showModal = !$scope.showModal;
    };
  });

app.directive("modalShow", function ($parse) {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {

            //Hide or show the modal
            scope.showModal = function (visible, elem) {
                if (!elem)
                    elem = element;

                if (visible)
                    $(elem).modal("show");                     
                else
                    $(elem).modal("hide");
            }

            //Watch for changes to the modal-visible attribute
            scope.$watch(attrs.modalShow, function (newValue, oldValue) {
                scope.showModal(newValue, attrs.$$element);
            });

            //Update the visible value when the dialog is closed through UI actions (Ok, cancel, etc.)
            $(element).bind("hide.bs.modal", function () {
                $parse(attrs.modalShow).assign(scope, false);
                if (!scope.$$phase && !scope.$root.$$phase)
                    scope.$apply();
            });
        }

    };
});
