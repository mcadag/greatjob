class CreateStoredCompanyCultures < ActiveRecord::Migration
  def change
    create_table :stored_company_cultures do |t|
      t.string :name

      t.timestamps
    end
  end
end
