class CreateWorkers < ActiveRecord::Migration
  def change
    create_table :workers do |t|
      t.string :region
      t.string :province
      t.string :district
      t.string :municipality
      t.string :provider
      t.string :TBP_id
      t.string :address
      t.string :type_of_institution
      t.string :provider_classification
      t.string :qualification
      t.string :COPR_number
      t.string :delivery_mode
      t.string :student_id_number
      t.string :last_name
      t.string :first_name
      t.string :middle_initial
      t.string :mobile_number
      t.string :email
      t.string :street_address
      t.string :barangay
      t.string :home_municipality
      t.string :district
      t.string :province
      t.string :gender
      t.string :birth_date
      t.string :age
      t.string :civil_status
      t.string :education
      t.string :scholarship
      t.string :client_type
      t.string :date_started
      t.string :date_ended
      t.string :reason
      t.string :assesment_result
      t.string :employment_date
      t.string :name_of_employer
      t.string :address_of_employer

      t.timestamps
    end
  end
end
