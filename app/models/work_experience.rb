class WorkExperience < ActiveRecord::Base
  attr_accessible :name
  belongs_to :user
  belongs_to :job
  has_many :jobs, :through => :job_work_experiences
end
