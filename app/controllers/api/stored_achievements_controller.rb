module Api
class StoredAchievementsController < ApplicationController
  # GET /stored_achievements
  # GET /stored_achievements.json
  def index
    @stored_achievements = StoredAchievement.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stored_achievements }
    end
  end

  # GET /stored_achievements/1
  # GET /stored_achievements/1.json
  def show
    @stored_achievement = StoredAchievement.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stored_achievement }
    end
  end

  # GET /stored_achievements/new
  # GET /stored_achievements/new.json
  def new
    @stored_achievement = StoredAchievement.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stored_achievement }
    end
  end

  # GET /stored_achievements/1/edit
  def edit
    @stored_achievement = StoredAchievement.find(params[:id])
  end

  # POST /stored_achievements
  # POST /stored_achievements.json
  def create
    @stored_achievement = StoredAchievement.new(stored_achievement_params)

    respond_to do |format|
      if @stored_achievement.save
        format.html { redirect_to @stored_achievement, notice: 'Stored achievement was successfully created.' }
        format.json { render json: @stored_achievement, status: :created, location: @stored_achievement }
      else
        format.html { render action: "new" }
        format.json { render json: @stored_achievement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stored_achievements/1
  # PATCH/PUT /stored_achievements/1.json
  def update
    @stored_achievement = StoredAchievement.find(params[:id])

    respond_to do |format|
      if @stored_achievement.update_attributes(stored_achievement_params)
        format.html { redirect_to @stored_achievement, notice: 'Stored achievement was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stored_achievement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stored_achievements/1
  # DELETE /stored_achievements/1.json
  def destroy
    @stored_achievement = StoredAchievement.find(params[:id])
    @stored_achievement.destroy

    respond_to do |format|
      format.html { redirect_to stored_achievements_url }
      format.json { head :no_content }
    end
  end

    def search
    @stored_achievement = StoredAchievement.where("name LIKE ?", "%"+params[:q]+"%").all
    if @stored_achievement.count >= 1
      render json: @stored_achievement.as_json(:only => [:name])
    else
      render :json => {:message => "No achievement Found with params:" + params[:q], :status => 404}
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def stored_achievement_params
      params.require(:stored_achievement).permit(:name)
    end
end
end