class AddApplicationTypeToCandidates < ActiveRecord::Migration
  def change
    add_column :candidates, :application_type, :string
  end
end
