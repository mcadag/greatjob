class CreateStoredAchievements < ActiveRecord::Migration
  def change
    create_table :stored_achievements do |t|
      t.string :name

      t.timestamps
    end
  end
end
