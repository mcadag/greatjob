class CreateUserJobRecommendations < ActiveRecord::Migration
  def change
    create_table :user_job_recommendations do |t|
      t.integer :recommended_by_id
      t.integer :user_id
      t.integer :job_id

      t.timestamps
    end
  end
end
