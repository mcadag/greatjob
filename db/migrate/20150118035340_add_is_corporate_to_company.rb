class AddIsCorporateToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :is_corporate, :boolean
  end
end
