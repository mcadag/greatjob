module Api
  class RegistrationsController < ApplicationController
    respond_to :json
    skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }

    def create
      @user = User.new(params.require(:registration).permit!)
      if !@user.name.nil?
        @user.name = params[:registration][:name].split.map(&:capitalize)*' '
      end
      if @user.save
        render :json=> @user.as_json(:methods => :avatar_url), :status=>201
      else
        warden.custom_failure!
        render :json=> @user.errors, :status=>422
      end
    end

    def update
      resource = User.find_for_database_authentication( authentication_token: params[:authentication_token] )
      user = resource
      if params[:avatar].blank? 
        render :json=> resource.errors(:status=>422, :error => :unprocessable_entity, :message => 'No image or featured_video passed'), :status=>422
      else
        if resource.update_attributes(avatar: params[:avatar])
          render :json => user.as_json(:methods => [:avatar_url], status: 201), :status =>201
        else
          render :json=> user.errors(:status=>422, :error => :unprocessable_entity), :status=>422
        end
      end
    end

      def featured_video
        resource = User.find_for_database_authentication( authentication_token: params[:authentication_token] )
        user = resource
        if !params[:user][:featured_video].blank?
          if resource.update_attributes(featured_video: params[:user][:featured_video])
            render :json => user.as_json(:methods => [:featured_video_url], status: 201), :status =>201
          else
            render :json=> user.errors(:status=>422, :error => :unprocessable_entity), :status=>422
          end
        end
      end


  end
end
