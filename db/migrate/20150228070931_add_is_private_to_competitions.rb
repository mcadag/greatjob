class AddIsPrivateToCompetitions < ActiveRecord::Migration
  def change
    add_column :competitions, :is_private, :boolean
  end
end
