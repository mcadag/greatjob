class AddadditionalParamsToLocations < ActiveRecord::Migration
  def up
  	add_column :locations, :location, :string
    add_column :locations, :website, :string
    add_column :locations, :email, :string
    add_column :locations, :mobile_number, :string
  end

  def down
  end
end
