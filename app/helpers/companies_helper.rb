module CompaniesHelper
  def CompanyJSONResponse(company)
    return company.as_json(:methods => [:payload_url], :only => [:id, :mobile_number, :details, :location, :website, :email, :name])
  end
end
