class RenameCreatedToTestimonies < ActiveRecord::Migration
  def up
  	rename_column :testimonies, :create_to_id, :created_by_id
  	rename_column :testimonies, :create_to_name, :user_name
  	rename_column :testimonies, :create_to_email, :user_email
  end

  def down
  end
end
