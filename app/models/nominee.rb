class Nominee < ActiveRecord::Base
	include PublicActivity::Model
  	tracked
    belongs_to :video
	has_many :nominee_testimonies
	has_one :counter
	belongs_to :competition
	belongs_to :user
	attr_accessible :competition_id, :user_id, :video_id, :creator_id

	def video_comment_count
		Comment.where("video_id = ?", self.video_id).count(:id)
	end

	def star_count
		
	end

	def video_view
		0
	end

	def nominated_by
		User.select(:name).find(self.user_id)
	end
end
