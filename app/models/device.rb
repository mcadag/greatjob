class Device < ActiveRecord::Base
  include PublicActivity::Model
  tracked
  attr_accessible :active, :platform, :unique_identifier, :user_id, :unique_id
end
