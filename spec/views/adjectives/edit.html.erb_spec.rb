require 'spec_helper'

describe "adjectives/edit" do
  before(:each) do
    @adjective = assign(:adjective, stub_model(Adjective))
  end

  it "renders the edit adjective form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", adjective_path(@adjective), "post" do
    end
  end
end
