class Rating < ActiveRecord::Base
  belongs_to :candidate
  belongs_to :user
  attr_accessible :rate
end
