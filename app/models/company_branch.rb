class CompanyBranch < ActiveRecord::Base
	include PublicActivity::Model
  tracked
  attr_accessible :branch_id, :comapny_id, :name, :user_id
end
