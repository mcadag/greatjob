class AddPayloadToVideos < ActiveRecord::Migration
	def self.up
    	add_attachment :videos, :payload
	end

	def self.down
    	remove_attachment :videos, :payload
	end
end
