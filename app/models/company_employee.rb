class CompanyEmployee < ActiveRecord::Base
  attr_accessible :company_id, :current, :user_id, :verified
  belongs_to :company
  belongs_to :user
end
