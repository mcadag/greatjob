class AddUserRefToPlaceGroups < ActiveRecord::Migration
  def change
    change_table :place_groups do |t|
      t.references :user, index: true 
    end
  end
  def down
    change_table :place_groups do |t|
      t.remove :user_id
    end
  end
end
