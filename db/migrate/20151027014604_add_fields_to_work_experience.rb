class AddFieldsToWorkExperience < ActiveRecord::Migration
  def change
    add_column :job_work_experiences, :job_id, :integer
    add_column :job_work_experiences, :work_experience_id, :integer
    add_column :job_work_experiences, :source_id, :integer
  end
end
