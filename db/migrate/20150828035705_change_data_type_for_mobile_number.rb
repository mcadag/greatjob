class ChangeDataTypeForMobileNumber < ActiveRecord::Migration
  def up 
  	change_table :stored_users do |t|
  		t.change :mobile_number, :string
  	end
  end

  def down
    change_table :stored_users do |t|
  		t.change :mobile_number, :int
  	end
  	
  end
end
