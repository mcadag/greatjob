class Testimony < ActiveRecord::Base
	include PublicActivity::Model
	belongs_to :user , counter_cache: true
	belongs_to :video
	attr_accessible :description, :name, :user_id, :video_id, :created_by_id, :user_name, :user_email, :permission

	scope :allowed, where(:permission => 'allowed')
	scope :latest, order('created_at DESC')
	scope :my, ->(*args) { where(:user_id => args.first) }

end
