class AddApplicationTypeToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :application_type, :string
  end
end
