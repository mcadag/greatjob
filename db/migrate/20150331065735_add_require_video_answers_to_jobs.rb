class AddRequireVideoAnswersToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :video_answer, :boolean
  end
end
