require 'spec_helper'

describe "nominees/new" do
  before(:each) do
    assign(:nominee, stub_model(Nominee,
      :user_id => 1,
      :competition_id => 1,
      :video_id => 1
    ).as_new_record)
  end

  it "renders new nominee form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", nominees_path, "post" do
      assert_select "input#nominee_user_id[name=?]", "nominee[user_id]"
      assert_select "input#nominee_competition_id[name=?]", "nominee[competition_id]"
      assert_select "input#nominee_video_id[name=?]", "nominee[video_id]"
    end
  end
end
