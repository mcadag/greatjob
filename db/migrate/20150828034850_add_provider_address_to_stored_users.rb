class AddProviderAddressToStoredUsers < ActiveRecord::Migration
  def change
    add_column :stored_users, :provider_address, :string
  end
end
