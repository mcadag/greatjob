class Company < ActiveRecord::Base
  include PublicActivity::Model
  tracked
  attr_accessible :details, :logo, :name, :user_id, :payload, :location, :website, :email, :user_id, :mobile_number
  belongs_to :users
  has_many :company_cultures
  has_many :stored_company_cultures, through: :company_cultures
  has_many :company_employees
  has_many :jobs

  acts_as_follower
  acts_as_followable

  validates :website, presence: true, uniqueness: {message: "already exists"}
  validates :mobile_number, presence: true
  validates :name, presence: true, uniqueness: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create } 
  validates :location, presence: true

  has_attached_file :payload,
           :styles => { :medium => "x300", :thumb => "x100" },
           :storage => :s3,
           :s3_permissions => :private,
           :s3_credentials => "#{Rails.root}/config/aws.yml"
  do_not_validate_attachment_file_type :payload
  
  def payload_url
        payload.url(:medium)
  end

end
