require "spec_helper"

describe StoredUsersController do
  describe "routing" do

    it "routes to #index" do
      get("/stored_users").should route_to("stored_users#index")
    end

    it "routes to #new" do
      get("/stored_users/new").should route_to("stored_users#new")
    end

    it "routes to #show" do
      get("/stored_users/1").should route_to("stored_users#show", :id => "1")
    end

    it "routes to #edit" do
      get("/stored_users/1/edit").should route_to("stored_users#edit", :id => "1")
    end

    it "routes to #create" do
      post("/stored_users").should route_to("stored_users#create")
    end

    it "routes to #update" do
      put("/stored_users/1").should route_to("stored_users#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/stored_users/1").should route_to("stored_users#destroy", :id => "1")
    end

  end
end
