class AddJobIdToJobDescriptionDetails < ActiveRecord::Migration
  def change
    add_column :job_description_details, :job_id, :integer
  end
end
