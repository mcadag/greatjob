class AddSuperlativeToGoals < ActiveRecord::Migration
  def change
    add_column :goals, :superlative, :string
    add_column :goals, :adjective, :string
    add_column :goals, :location, :string
  end
end
