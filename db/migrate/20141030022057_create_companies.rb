class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.integer :user_id
      t.string :details
      t.timestamps
    end
  end
end
