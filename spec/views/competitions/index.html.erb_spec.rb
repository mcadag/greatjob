require 'spec_helper'

describe "competitions/index" do
  before(:each) do
    assign(:competitions, [
      stub_model(Competition,
        :name => "Name",
        :user_id => 1,
        :company_id => 2,
        :description => "Description",
        :prize_id => 3
      ),
      stub_model(Competition,
        :name => "Name",
        :user_id => 1,
        :company_id => 2,
        :description => "Description",
        :prize_id => 3
      )
    ])
  end

  it "renders a list of competitions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
