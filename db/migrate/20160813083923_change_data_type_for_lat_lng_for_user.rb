class ChangeDataTypeForLatLngForUser < ActiveRecord::Migration
   def up 
  	change_table :users do |t|
  		t.change :lat, :float
  		t.change :lng, :float
  	end
  end

  def down
    change_table :users do |t|
    	t.change :lat, :int
  		t.change :lng, :int
  	end
  end

end
