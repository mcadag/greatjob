require 'spec_helper'

describe "companies/new" do
  before(:each) do
    assign(:company, stub_model(Company,
      :name => "MyString",
      :user_id => 1,
      :details => "MyString",
      :logo => ""
    ).as_new_record)
  end

  it "renders new company form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", companies_path, "post" do
      assert_select "input#company_name[name=?]", "company[name]"
      assert_select "input#company_user_id[name=?]", "company[user_id]"
      assert_select "input#company_details[name=?]", "company[details]"
      assert_select "input#company_logo[name=?]", "company[logo]"
    end
  end
end
