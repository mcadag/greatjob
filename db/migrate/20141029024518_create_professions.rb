class CreateProfessions < ActiveRecord::Migration
  def change
    create_table :professions do |t|
    	t.date :start_date
    	t.date :end_date
    	t.string :job_title
    	t.string :job_description
    	t.string :other_details
    	t.integer :company_id
    	t.integer :user_id
    	t.string :other
    	t.boolean :current_job
      t.timestamps
    end
  end
end
