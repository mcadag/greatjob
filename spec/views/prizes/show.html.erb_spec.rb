require 'spec_helper'

describe "prizes/show" do
  before(:each) do
    @prize = assign(:prize, stub_model(Prize,
      :name => "Name",
      :amount => "",
      :details => "Details",
      :competition_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(//)
    rendered.should match(/Details/)
    rendered.should match(/1/)
  end
end
