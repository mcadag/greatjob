class RenameTypeTocategoryForDocuments < ActiveRecord::Migration
  def up
  	rename_column :documents, :type, :category
  end

  def down
  end
end
