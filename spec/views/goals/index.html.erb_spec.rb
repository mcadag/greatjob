require 'spec_helper'

describe "goals/index" do
  before(:each) do
    assign(:goals, [
      stub_model(Goal,
        :name => "Name",
        :user_id => 1,
        :position => "Position"
      ),
      stub_model(Goal,
        :name => "Name",
        :user_id => 1,
        :position => "Position"
      )
    ])
  end

  it "renders a list of goals" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Position".to_s, :count => 2
  end
end
