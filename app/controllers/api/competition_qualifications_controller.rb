module Api
  class CompetitionQualificationsController < ApplicationController
    before_filter :set_testimony, only: [:show, :edit, :update, :destroy]
    before_filter :authenticate_token!, :only => [:create, :update, :destroy, :index]
    respond_to :json

    # GET /competition_qualifications
    # GET /competition_qualifications.json
    def index
      @competition_qualifications = CompetitionQualification.where(competition_id: params[:competition_id])

      respond_to do |format|
        format.json { render json: @competition_qualifications }
      end
    end

    # GET /competition_qualifications/1
    # GET /competition_qualifications/1.json
    def show
      @competition_qualification = CompetitionQualification.find(params[:id])

      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @competition_qualification }
      end
    end

    # GET /competition_qualifications/new
    # GET /competition_qualifications/new.json
    def new
      @competition_qualification = CompetitionQualification.new

      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @competition_qualification }
      end
    end

  # GET /competition_qualifications/1/edit
    def edit
      @competition_qualification = CompetitionQualification.find(params[:id])
    end

    # POST /competition_qualifications
    # POST /competition_qualifications.json
    def create
      results = []
      params[:qualification][:name].split(/, ?/).each do | qualification |
        @competition_qualification = CompetitionQualification.new
        @competition_qualification.name = qualification
        @competition_qualification.competition_id = params[:competition_id]
        @competition_qualification.save
      end
      render :json => { status: 201, alert: 'qualifications created'}
  end

  # PATCH/PUT /competition_qualifications/1
  # PATCH/PUT /competition_qualifications/1.json
  def update
    @competition_qualification = CompetitionQualification.find(params[:id])

    respond_to do |format|
      if @competition_qualification.update_attributes(competition_qualification_params)
        format.html { redirect_to @competition_qualification, notice: 'Competition qualification was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @competition_qualification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /competition_qualifications/1
  # DELETE /competition_qualifications/1.json
  def destroy
    @competition_qualification = CompetitionQualification.find(params[:id])
    @competition_qualification.destroy

    respond_to do |format|
      format.html { redirect_to competition_qualifications_url }
      format.json { head :no_content }
    end
  end

  private
    def authenticate_token!
      auth_user = User.where(authentication_token: params[:authentication_token])
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def competition_qualification_params
      params.require(:qualification).permit(:name, :competition_id)
    end
  end
end