require 'spec_helper'

describe "awards/new" do
  before(:each) do
    assign(:award, stub_model(Award,
      :name => "MyString",
      :user_id => 1,
      :video_id => 1,
      :description => "MyString"
    ).as_new_record)
  end

  it "renders new award form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", awards_path, "post" do
      assert_select "input#award_name[name=?]", "award[name]"
      assert_select "input#award_user_id[name=?]", "award[user_id]"
      assert_select "input#award_video_id[name=?]", "award[video_id]"
      assert_select "input#award_description[name=?]", "award[description]"
    end
  end
end
