class UserJobRecommendations < ActiveRecord::Base
  attr_accessible :job_id, :recommended_by_id, :user_id
end
