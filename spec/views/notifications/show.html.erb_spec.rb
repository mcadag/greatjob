require 'spec_helper'

describe "notifications/show" do
  before(:each) do
    @notification = assign(:notification, stub_model(Notification,
      :user => nil,
      :message => "MyText",
      :type => "Type",
      :topic_id => 1,
      :checked => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/MyText/)
    rendered.should match(/Type/)
    rendered.should match(/1/)
    rendered.should match(/false/)
  end
end
