module Api
  class TrainingsController < ApplicationController
  before_filter :set_training, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_token!, :only => [:create, :update, :destroy]
  respond_to :json

  def index
    trainings = []
    results = Training.where(user_id: params[:user_id]).order('created_at DESC')
    results.each do |training|
      trainings << training
    end
    respond_with(trainings)
  end

  def show
    respond_with(@training)
  end

  def new
    @training = Training.new
    respond_with(@training)
  end

  def edit
  end

  def create
    training = Training.new
    training.name = params[:training][:name]
    training.start_date = params[:training][:start_date]
    training.end_date = params[:training][:end_date]
    training.company_name = params[:training][:company_name]
    training.description = params[:training][:description]
    training.details = params[:training][:details]
    training.other_details = params[:training][:other_details]
    training.permission = "pending"    
    training.user_id = params[:user_id]
    training.save
    render :json=> training.as_json(:user_id=>training.user_id, :status=>201), :status=>201
  end


  def update
    @training.update_attributes(params.require(:training).permit!)
    render :json => @training.as_json(:status => 201, :message => "Updated"), status => 201
  end

  def destroy
      if @training.destroy && @training.user_id == @user.id
        render :json => {:message => "Deleted"}
      else
        render :json => {:message => "You are not authorized to delete the resource"}
      end
      
    end

  def permission
    training = Training.where(id: params[:training_id])
    training.update_attributes(params[:training])
    render :json => training.as_json(:status => 201, :message => "Permission is set"), status => 201
  end

  private
    def set_training
      @training = Training.find(params[:id])
    end

    def authenticate_token!
      auth_user = User.where(authentication_token: params[:authentication_token])
      @user = auth_user[0]
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end
end
end