require 'spec_helper'

describe "competition_video_comments/show" do
  before(:each) do
    @competition_video_comment = assign(:competition_video_comment, stub_model(CompetitionVideoComment,
      :payload => "",
      :name => "Name",
      :competition_id => 1,
      :user_id => 2,
      :message => "Message",
      :nominee_id => 3,
      :details => "Details",
      :description => "Description",
      :approved => false,
      :is_private => "",
      :has_video => false,
      :video_id => 4
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Name/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/Message/)
    rendered.should match(/3/)
    rendered.should match(/Details/)
    rendered.should match(/Description/)
    rendered.should match(/false/)
    rendered.should match(//)
    rendered.should match(/false/)
    rendered.should match(/4/)
  end
end
