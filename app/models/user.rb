class User < ActiveRecord::Base
  rolify
  acts_as_mappable
  include PublicActivity::Model
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  validates :email, presence: true, uniqueness: true
  validates :name, presence: true, length: { minimum: 2 }
  validates :about, length: { maximum: 500 }

  devise :invitable, :database_authenticatable, :registerable, :token_authenticatable, :invitable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:facebook]
  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, 
                  :remember_me, :name, :authentication_token, 
                  :avatar, :address, :contact_number, 
                  :facebook_id, :birthday, :featured_video,
                  :first_name, :last_name, :gender, :star_points,
                  :login, :employer, :about, :lat, :lng, :is_following
  attr_accessor :login

  has_many :videos
  has_many :professions
  has_many :competitions
  has_many :companies
  has_many :awards
  has_many :testimonies
  has_many :goals
  has_many :nominees
  has_many :users
  has_many :interests
  has_many :notifications
  has_many :jobs
  has_many :candidates
  has_many :ratings
  has_many :company_employees
  has_many :work_experiences
  
  has_many :group_members
  has_many :place_groups, through: :group_members

  before_save :ensure_authentication_token

  acts_as_follower
  acts_as_liker
  acts_as_followable

  Roles = [ :admin , :default ]

  def is?( requested_role )
    self.role == requested_role.to_s
  end

  has_attached_file :avatar,
           :styles => { :medium => "x300", :thumb => "x100" },
           :default_url => "https://ccrnrcrc.files.wordpress.com/2012/03/profile-placeholder.gif",
           :storage => :s3,
           :s3_permissions => :private,
           :s3_credentials => "#{Rails.root}/config/aws.yml"
  do_not_validate_attachment_file_type :avatar

  has_attached_file :featured_video,
    :storage => :s3,
    :s3_permissions => :private,
    :s3_credentials => "#{Rails.root}/config/aws.yml",
    :styles => {
        :mobile => {:geometry => "400x300", 
              :format => 'mp4'},
        :thumb => { :geometry => "400x400#", 
              :format => 'jpg', 
              :time => 3 }
      }, 
      :processors => [:ffmpeg],
      :use_timestamp => false
  do_not_validate_attachment_file_type :featured_video

  def avatar_url
    avatar.url(:medium)
  end

  def featured_video_url
    featured_video.url(:mobile)
  end

  def featured_video_thumb_url
    featured_video.url(:thumb)
  end

  def self.from_fb_graph2(user)
    @user = User.new
    @user.name = user.name
    @user.email = user.email
    @user.provider = 'facebook'
    @user.last_name = user.last_name
    @user.first_name = user.first_name
    @user.uid = user.id
    @user.gender = user.gender
    @user.facebook_image = user.picture(:large).url
    return @user
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def self.from_omniauth(auth)
      where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
        user.provider = auth.provider
        user.uid = auth.uid
        user.email = auth.info.email
        user.name = auth.info.name   # assuming the user model has a name
        user.password = Devise.friendly_token[0,20]
      end
  end

  def self.find_for_database_authentication(warden_conditions)
      conditions = warden_conditions.dup
      if login = conditions.delete(:login)
        where(conditions.to_h).where(["contact_number = :value OR email = :value", { :value => login.downcase }]).first
      else
        where(conditions.to_h).first
      end
  end

end
