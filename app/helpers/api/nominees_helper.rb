module Api::NomineesHelper
	def nomineeJSONResponse(nominee)
	   nom = (Nominee.joins(:counter).where(competition_id: params[:id]).order("votes DESC").limit(1))

      video = Video.where(["id = ?" , nom.pluck(:video_id)]).first
      nominated_by_name = User.select('name').where(id: video.created_by).limit(1)
      nominated_by_id = User.select('id').where(id: video.created_by).limit(1)
      leading_nominee_by_name = User.select('name').where(id: video.user_id).limit(1)
      votes = Counter.where(nominee_id: nominee.id)
      comments = CompetitionVideoComment.where(video_id: nom.pluck(:video_id), competition_id: params[:id])

      nominee[:nominee_id] = nominee.id
      nominee[:nominee_name] = leading_nominee_by_name.pluck(:name)[0]
      nominee[:nominated_by_name] = nominated_by_name.pluck(:name)[0]
      nominee[:nominated_by_id] = nominated_by_id.pluck(:id)[0]
      nominee[:stars_count] = comments.count
      return nominee
	end

	def userNominationsJSONResponse(nominee, com)
		  competition = Competition.where(id: nominee.competition_id)
          video = Video.where(id: nominee.video_id)
          nominated_by_name = User.select('name').where(id: video.pluck(:created_by))
          nominated_by_id = User.select('id').where(id: video.pluck(:created_by)).limit(1)
          company = Company.where(id: competition.pluck(:company_id))
          prize = Prize.where(id: com.prize_id)

          nominee[:competition_name] = com.name
          nominee[:competition_title] = competition.pluck(:superlative)[0].to_s() + " " + competition.pluck(:name)[0].to_s() + " in  " + competition.pluck(:location)[0].to_s() + " for " + competition.pluck(:year)[0].to_s()
          nominee[:competition_id] = com.id
          nominee[:competition_description] = com.description
          nominee[:start_date] = com.start_date
          nominee[:end_date] = com.end_date
          nominee[:prize] = prize.as_json()
          nominee[:company] = company.pluck(:name)[0]
          nominee[:video] = video.as_json(:methods => [:payload_url, :payload_thumb_url, :payload_mobile_url])
          nominee[:nominated_by_name] = nominated_by_name.pluck(:name)[0]
          nominee[:nominated_by_id] = nominated_by_id.pluck(:id)[0]
          if nominee[:video].empty?
          	return
          else
          	return nominee
          end
	end
end
