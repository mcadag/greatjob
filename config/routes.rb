FamousAPI::Application.routes.draw do

  resources :group_members


  resources :place_groups


  resources :subscriptions


  resources :referrals


  resources :stored_users


  resources :smsgateways

  resources :ratings

  resources :comments

  # resources :candidates do
  #   get 'candidates/:candidate_id/comments', to: 'candidates#comments'
  # end
  
  resources :candidates do
    resources :comments
  end

  resources :settings


  resources :welcomes
  root to: 'welcomes#index'

   # static pages
  match '/faqs' => 'welcomes#faqs'
  match '/pricing' => 'welcomes#pricing'
  
  put 'group_members/:id/confirm' => 'group_members#confirm', :as => 'confirm_member'

  devise_scope :user do
    #root to: "devise/registrations#new"
  end

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", :invitations => 'users/invitations' }

  resources :users do
    resources :notifications
    resources :place_groups do
      resources :group_members
    end
  end

  get 'discovery', to: :discovery, controller: 'place_groups'
  put 'group_action', to: :group_action, controller: 'group_members'

  resources :place_groups do
    resources :group_members
  end
  
  namespace :api, defaults: {format: 'json'} do
    devise_for :users

    get 'feeds' => 'feeds#index'
    get 'search' => 'search#index'
    get 'search/people' => 'search#people'
    get 'search/company' => 'search#company'
    get 'search/competition' => 'search#competition'
    get 'search/latest' => 'search#latest'

    match "users/social_login" => "users#social_login", :via => :post

    resources :devices

    resources :competitions do
      get 'top', on: :collection
      get 'suggested', on: :collection
      get 'search', on: :collection
      get 'nominees', on: :member
      post ':id/nominees', to: "nominees#create"
      resources :competition_qualifications do
        get 'search', on: :collection
      end
      resources :prizes
      resources :nominees do
        get 'ranking', on: :collection
        resources :nominee_testimonies, :path => :testimonies
        resources :videos do
          resources :competition_video_comments do
            resources :ratings
          end
        end
      end
    end

    resources :jobs do
      post 'apply', to: 'jobs#apply'
      post 'refer', to: 'jobs#refer'
      post 'invite', to: 'jobs#invite'
      get 'applied', on: :collection
      get 'candidates', to: 'jobs#candidates'
      get 'candidates/short_listed', to: 'jobs#short_listed'
      get 'candidates/candidate_short_listed', to: 'jobs#candidate_short_listed'
      get 'candidates/short_listed/accepted_schedule', to: 'jobs#accepted_schedule'
      get 'candidates/short_listed/re_schedule', to: 'jobs#re_schedule'
      post 'candidates/:candidate_id/short_list', to: 'jobs#short_list', as: :short_list_candidate
      post 'candidates/:candidate_id/accept_interview_schedule', to: 'jobs#accept_interview_schedule', as: :accept_interview_schedule
      post 'candidates/:candidate_id/reschedule_interview', to: 'jobs#reschedule_interview', as: :reschedule_interview
      post 'questions/:job_question_id/answer', to: 'jobs#answer', as: :answer
      post 'candidates/cancel_application', to: 'jobs#cancel_application', as: :cancel_application
      get 'search', on: :collection, as: :search
      post 'candidates/:candidate_id/promote_video_resource', to: 'jobs#promote_video_resource', as: :promote_video_resource
    end

    get 'users/:user_id/companies/following' => 'users#followingCompanies'
    get 'users/:user_id/followers' => 'users#followers'
    get 'users/:user_id/subscribed', to: 'jobs#subscribed'
    post 'users/:user_id/subscribe' => 'jobs#subscribe'
    get 'users/:user_id/following' => 'users#following'
    get 'users/:user_id/activities' => 'users#activities'
    get 'users/:user_id/companies/managed' => 'companies#managed'
    get 'users/:user_id/pending_permissions' => 'users#pending_permissions'
    get 'companies/:company_id/employees' => 'companies#employees'
    get 'users/:user_id/active_nominations' => 'users#active_nominations'
    get 'users/:user_id/inactive_nominations' => 'users#inactive_nominations'
    
    get 'place_groups/:place_id/members' => "place_groups#members"
    
    get 'users/:user_id/testimonies/gallery' => "testimonies#gallery"
    get 'users/:user_id/testimonies/testimonials' => "testimonies#testimonials"

    resources :activities do
      resources :comments
    end

    resources :users do
      get 'search', on: :collection
      get 'me', on: :collection
      get 'nearest', on: :collection
      get 'latest', on: :collection

      post 'follow', to: "users#follow"
      post 'unfollow', to: "users#unfollow"

      put 'featured_video', to: "registrations#featured_video"

      resources :companies do
        resources :company_testimonies
      end
      resources :interests do
        post 'permission', to: 'interests#permission'
        post 'allow', to: "interests#approve"
        post 'reject', to: "interests#reject"
      end
      resources :trainings do
        post 'permission', to: 'trainings#permission'
      end
      resources :videos do
        get 'mine', on: :collection
        post 'view', to: 'videos#updated_view_count'
        resources :comments
      end
      resources :professions do
        post 'permission', to: 'professions#permission'
      end
      resources :goals do
        post 'permission', to: 'goals#permission'
      end
      resources :videos do
        resources :comments
      end
      resources :testimonies do
        post 'allow', to: "testimonies#approve"
        post 'reject', to: "testimonies#reject"
        resources :videos
      end
      resources :awards do
          post 'allow', to: "testimonies#approve"
          post 'reject', to: "testimonies#reject"
          resources :videos
          resources :competitions
      end
      resources :documents
      resources :images
      resources :place_groups
      resources :notifications
    end

    resources :videos do
      post 'like', to: "videos#like"
      put 'unlike', to: "videos#unlike"
      get 'trending', on: :collection
      resources :comments
    end
    
    resources :qualifications do
      get :autocomplete_name, :on => :collection
      get 'search', on: :collection
    end

    resources :adjectives do
      get :autocomplete_name, :on => :collection
      get 'search', on: :collection
    end

    resources :stored_professions do
      get :autocomplete_name, :on => :collection
      get 'search', on: :collection
    end

    resources :locations do
      get :autocomplete_name, :on => :collection
      get 'search', on: :collection
    end

    resources :companies do
      get :autocomplete_name, :on => :collection
      get 'search', on: :collection
    end

    resources :stored_achievements do
      get :autocomplete_name, :on => :collection
      get 'search', on: :collection
    end

    resources :stored_testimonies do
      get :autocomplete_name, :on => :collection
      get 'search', on: :collection
    end

    resources :stored_trainings do
      get :autocomplete_name, :on => :collection
      get 'search', on: :collection
    end

    resources :stored_company_cultures do
      get :autocomplete_name, :on => :collection
      get 'search', on: :collection
    end

    get 'companies/:id/followers' => 'companies#followers'

    resources :companies do
      get :autocomplete_name, :on => :collection
      post 'follow', to: "companies#follow"
      post 'unfollow', to: "companies#unfollow"

      resources :company_testimonies

      resources :company_branches

      resources :company_dream_jobs

      resources :company_cultures
    end


    post 'nominate', to: 'nominees#create'
    get 'users/:id', to: 'users#show'
    resources :keys, except: :show
    get 'keys/:name', to: 'keys#show'
  end

  resources :users do
    get :autocomplete_name, :on => :collection
    get 'latest', to: 'api#users#latest'
    get 'pending_permissions', to: 'api#users#pending_permissions'
  end

  resources :users, defaults: {format: 'html'} do
    resources :goals
    resources :trainings
    resources :qualifications
    resources :awards
    resources :testimonies
    resources :professions
    resources :interests
    resources :documents
    resources :companies
    resources :company_employees
  end

  get 'feeds', to: 'feeds#index'

  get 'search', to: 'search#index'

  get 'search/candidates', to: 'search#candidates'

  get 'dashboard', to: 'dashboard#index'

  resources :companies do
    get 'invite', to: 'companies#invite'
    get 'accept', to: 'companies#accept'
  end
  resources :professions
  resources :trainings
  resources :interests
  resources :testimonies
  resources :goals

  resources :videos do
    post 'view', to: 'videos#updated_view_count'
  end

  resources :competitions, defaults: {format: 'html'} do
    get :autocomplete_name, :on => :collection
    resources :nominees
  end

  get 'trending', to: 'site#trending'

  resources :jobs do
    post 'apply', to: 'jobs#apply'
    get 'answer', to: 'jobs#answer'
    post 'post_answer', to: 'jobs#post_answer', as: :answer
    get :autocomplete_position, :on => :collection
  end
end
