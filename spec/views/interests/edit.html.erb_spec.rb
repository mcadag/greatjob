require 'spec_helper'

describe "interests/edit" do
  before(:each) do
    @interest = assign(:interest, stub_model(Interest,
      :name => "MyString",
      :description => "MyString",
      :user_id => 1
    ))
  end

  it "renders the edit interest form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", interest_path(@interest), "post" do
      assert_select "input#interest_name[name=?]", "interest[name]"
      assert_select "input#interest_description[name=?]", "interest[description]"
      assert_select "input#interest_user_id[name=?]", "interest[user_id]"
    end
  end
end
