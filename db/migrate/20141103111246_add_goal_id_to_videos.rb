class AddGoalIdToVideos < ActiveRecord::Migration
  def change
  	add_column :videos, :title, :string
  	add_column :videos, :goal_id, :integer
  	add_column :videos, :stars, :integer
  	add_column :videos, :testimonies, :integer
  	add_column :videos, :nominated_by, :integer
  	add_column :videos, :training_id, :integer
  	add_column :videos, :nominee_id, :integer
  	add_column :videos, :interest_id, :integer
  	add_column :videos, :comment_id, :integer
  end
end
