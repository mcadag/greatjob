class SmsgatewaysController < ApplicationController
  # GET /smsgateways
  # GET /smsgateways.json
  def index
    @smsgateways = Smsgateway.all
         @response = HTTParty.get('http://smsgateway.me/api/v3/devices?email=francis@famous.ph&password=LORIANE3387')
     User.all.each do |user|
        @message = HTTParty.post('http://smsgateway.me/api/v3/messages/send', 
                :body => {:email => 'francis@famous.ph',
                          :password => 'LORIANE3387',
                          :device => 7595,
                          :number => user.number,
                          :message => params[:message]
                          }.to_json,
                :headers => { 'Content-Type' => 'application/json' } )
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @smsgateways }
    end
  end

  # GET /smsgateways/1
  # GET /smsgateways/1.json
  def show
    @smsgateway = Smsgateway.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @smsgateway }
    end
  end

  # GET /smsgateways/new
  # GET /smsgateways/new.json
  def new
    @smsgateway = Smsgateway.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @smsgateway }
    end
  end

  # GET /smsgateways/1/edit
  def edit
    @smsgateway = Smsgateway.find(params[:id])
  end

  # POST /smsgateways
  # POST /smsgateways.json
  def create
    @smsgateway = Smsgateway.new(smsgateway_params)

    respond_to do |format|
      if @smsgateway.save
        format.html { redirect_to @smsgateway, notice: 'Smsgateway was successfully created.' }
        format.json { render json: @smsgateway, status: :created, location: @smsgateway }
      else
        format.html { render action: "new" }
        format.json { render json: @smsgateway.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /smsgateways/1
  # PATCH/PUT /smsgateways/1.json
  def update
    @smsgateway = Smsgateway.find(params[:id])

    respond_to do |format|
      if @smsgateway.update_attributes(smsgateway_params)
        format.html { redirect_to @smsgateway, notice: 'Smsgateway was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @smsgateway.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /smsgateways/1
  # DELETE /smsgateways/1.json
  def destroy
    @smsgateway = Smsgateway.find(params[:id])
    @smsgateway.destroy

    respond_to do |format|
      format.html { redirect_to smsgateways_url }
      format.json { head :no_content }
    end
  end

  private
    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def smsgateway_params
      params.require(:smsgateway).permit()
    end
  end
end
