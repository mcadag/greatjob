class CreatePlaceGroups < ActiveRecord::Migration
  def change
    create_table :place_groups do |t|
      t.string :name
      t.string :place_id

      t.timestamps
    end
  end
end
