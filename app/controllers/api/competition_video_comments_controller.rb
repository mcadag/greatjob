module Api
class CompetitionVideoCommentsController < ApplicationController
  before_filter :authenticate_token!, :only => [:create, :update, :destroy]
  respond_to :json

  # GET /competition_video_comments
  # GET /competition_video_comments.json
  def index
    comments = []
    com = CompetitionVideoComment.where(video_id: params[:video_id], competition_id: params[:competition_id]).order('created_at DESC')
    com.each do |comment|
        user = User.where(id: comment.user_id)
        if user.count >= 1
          comment[:has_video] = comment.video.exists?
          comment[:user] = user.as_json(:methods => [:avatar_url], :only => [:name, :id])
          comment[:video_url] = :video_url
          comment[:video_thumb_url] = :video_thumb_url
          comment[:video_mobile_url] = :video_mobile_url
          comments << comment          
        end
    end
    results = comments
    respond_with(results)
  end

  # GET /competition_video_comments/1
  # GET /competition_video_comments/1.json
  def show
    @competition_video_comment = CompetitionVideoComment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @competition_video_comment }
    end
  end

  # GET /competition_video_comments/new
  # GET /competition_video_comments/new.json
  def new
    @competition_video_comment = CompetitionVideoComment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @competition_video_comment }
    end
  end

  # GET /competition_video_comments/1/edit
  def edit
    @competition_video_comment = CompetitionVideoComment.find(params[:id])
  end

  # POST /competition_video_comments
  # POST /competition_video_comments.json
  def create    
    comment = CompetitionVideoComment.new
    comment.approved = 0
    comment.is_private = 0
    comment.competition_id = params[:competition_id]
    comment.description = params[:comment][:description]
    comment.details = params[:comment][:details]
    comment.has_video = params[:comment][:has_video]
    comment.message = params[:comment][:message]
    comment.name = params[:comment][:name]
    comment.nominee_id = params[:nominee_id]
    comment.video = params[:comment][:video]
    comment.user_id = @user.id
    comment.video_id = params[:video_id]
      if comment.save
        @vote = Counter.where(nominee_id: comment.nominee_id)
        vidComment = CompetitionVideoComment.where(user_id: @user.id, competition_id: params[:competition_id])
        if comment.video.exists?
          Counter.increment_counter(:votes, @vote.pluck(:id)[0])
        end
        comment[:has_video] = comment.video.exists?
        render json: comment.to_json(:methods => [:video_url,:video_mobile_url,:video_thumb_url], 
                                   :success => true, 
                                   :status => 201)
        else
          render json: comment.errors, status: :unprocessable_entity
      end
  end

  # PATCH/PUT /competition_video_comments/1
  # PATCH/PUT /competition_video_comments/1.json
  def update
    @competition_video_comment = CompetitionVideoComment.find(params[:id])

    respond_to do |format|
      if @competition_video_comment.update_attributes(competition_video_comment_params)
        format.html { redirect_to @competition_video_comment, notice: 'Competition video comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @competition_video_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /competition_video_comments/1
  # DELETE /competition_video_comments/1.json
  def destroy
    @competition_video_comment = CompetitionVideoComment.find(params[:id])
    @competition_video_comment.destroy

    respond_to do |format|
      format.html { redirect_to competition_video_comments_url }
      format.json { head :no_content }
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def competition_video_comment_params
      params.require(:competition_video_comment).permit(:approved, :competition_id, :description, :details, :has_video, :is_private, :message, :name, :nominee_id, :video, :user_id, :video_id)
    end
    
    def authenticate_token!
      auth_user = User.select("id").where(authentication_token: params[:authentication_token])
      @user = auth_user[0]
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

end
end