class AddUserFeaturedVideo < ActiveRecord::Migration
  def up
  	add_attachment :users, :featured_video
  end

  def down
  end
end
