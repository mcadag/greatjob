module Api
class DocumentsController < ApplicationController
  before_filter :set_document, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_token!, :only => [:create, :update, :destroy]

  respond_to :json

  def index
    results = []
    @documents = Document.where(user_id: params[:user_id])
    @documents.each do |document|
      document[:payload_url] = :payload_url
      results << document
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: results.as_json }
    end
  end

  # GET /documents/1
  # GET /documents/1.json
  def show
    @document = Document.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @document }
    end
  end

  # GET /documents/new
  # GET /documents/new.json
  def new
    @document = Document.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @document }
    end
  end

  # GET /documents/1/edit
  def edit
    @document = Document.find(params[:id])
  end

  # POST /documents
  # POST /documents.json
  def create
    @document = Document.new
    @document.payload = params[:document][:payload]
    @document.name = params[:document][:name]
    @document.category = params[:document][:category]
    @document.user_id = params[:user_id]
    @document.description = params[:document][:description]
    @document.save
    @document[:payload_url] = :payload_url
    render json: @document.as_json()
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update
    @document = Document.find(params[:id])
    @document.update_attributes(:category => params[:document][:category], :payload => params[:document][:payload])
    @document[:payload_url] = :payload_url
    render json: @document.as_json()
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @document = Document.find(params[:id])
    @document.destroy

    respond_to do |format|
      format.html { redirect_to documents_url }
      format.json { head :no_content }
    end
  end

  private
    def set_document
      @document = Document.find(params[:id])
    end

    def authenticate_token!
      auth_user = User.select("id").where(authentication_token: params[:authentication_token])
      if auth_user.count >= 1
        @user = auth_user[0]
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end
    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def document_params
      params.require(:document).permit(:description, :name, :payload, :category)
    end
end
end