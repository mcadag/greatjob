require "spec_helper"

describe StoredProfessionsController do
  describe "routing" do

    it "routes to #index" do
      get("/stored_professions").should route_to("stored_professions#index")
    end

    it "routes to #new" do
      get("/stored_professions/new").should route_to("stored_professions#new")
    end

    it "routes to #show" do
      get("/stored_professions/1").should route_to("stored_professions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/stored_professions/1/edit").should route_to("stored_professions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/stored_professions").should route_to("stored_professions#create")
    end

    it "routes to #update" do
      put("/stored_professions/1").should route_to("stored_professions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/stored_professions/1").should route_to("stored_professions#destroy", :id => "1")
    end

  end
end
