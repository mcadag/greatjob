require 'spec_helper'

describe "stored_trainings/index" do
  before(:each) do
    assign(:stored_trainings, [
      stub_model(StoredTraining,
        :name => "Name"
      ),
      stub_model(StoredTraining,
        :name => "Name"
      )
    ])
  end

  it "renders a list of stored_trainings" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
