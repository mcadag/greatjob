module Api
	class FeedsController < ApplicationController
	before_filter :authenticate_token_for_user!
	
	def index
		ids = @user.followees(User).map(&:id)
		@feeds = Activity.order('created_at desc').where('activities.owner_id in (?) or  activities.recipient_id in (?)', ids ,  ids).includes(:comments).page(params[:page])
		render :json => {
	      :current_page => @feeds.current_page,
	      :per_page => @feeds.per_page,
	      :total_entries => @feeds.total_entries,
	      :first_page => @feeds.current_page == 1,
	      :last_page => @feeds.next_page.blank?,
          :previous_page => @feeds.previous_page,
          :next_page => @feeds.next_page,
          :activities => @feeds.as_json(:include => [:comments])
    	}
	end

	private
        def authenticate_token_for_user!
          auth_user = User.where(authentication_token: params[:authentication_token])
          @user = auth_user[0]
          if auth_user.count >= 1
            true
          else
            render :json => {:message => "your authentication token is not valid" }
          end
        end
	end
end