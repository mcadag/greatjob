class CreateEducationalAttainments < ActiveRecord::Migration
  def change
    create_table :educational_attainments do |t|
      t.string :name

      t.timestamps
    end
  end
end
