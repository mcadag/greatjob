require "spec_helper"

describe CompetitionQualificationsController do
  describe "routing" do

    it "routes to #index" do
      get("/competition_qualifications").should route_to("competition_qualifications#index")
    end

    it "routes to #new" do
      get("/competition_qualifications/new").should route_to("competition_qualifications#new")
    end

    it "routes to #show" do
      get("/competition_qualifications/1").should route_to("competition_qualifications#show", :id => "1")
    end

    it "routes to #edit" do
      get("/competition_qualifications/1/edit").should route_to("competition_qualifications#edit", :id => "1")
    end

    it "routes to #create" do
      post("/competition_qualifications").should route_to("competition_qualifications#create")
    end

    it "routes to #update" do
      put("/competition_qualifications/1").should route_to("competition_qualifications#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/competition_qualifications/1").should route_to("competition_qualifications#destroy", :id => "1")
    end

  end
end
