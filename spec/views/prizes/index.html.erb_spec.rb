require 'spec_helper'

describe "prizes/index" do
  before(:each) do
    assign(:prizes, [
      stub_model(Prize,
        :name => "Name",
        :amount => "",
        :details => "Details",
        :competition_id => 1
      ),
      stub_model(Prize,
        :name => "Name",
        :amount => "",
        :details => "Details",
        :competition_id => 1
      )
    ])
  end

  it "renders a list of prizes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Details".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
