module Api
class PlaceGroupsController < ApplicationController
  before_filter :set_place_group, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_token!, :only => [:create, :update, :destroy]
  before_filter :joinable_group!, :only => [:create]
  respond_to :json

  def index
    @place_groups = PlaceGroup.where(:user_id => params[:user_id])
    render :json => @place_groups.as_json()
  end

  def members
    # TODO change to members 
    @place_groups = PlaceGroup.where(:place_id => params[:place_group][:place_id]).includes(:user)
    if @place_groups.nil?
      render :json => {:message => "you place groups is nil"}
    else 
      render :json => @place_groups.as_json(include: { user: { except: :authentication_token, methods: :avatar_url } })
    end
  end

  def show
    respond_with(@place_group)
  end

  def new
    @place_group = PlaceGroup.new
    respond_with(@place_group)
  end

  def edit
  end

  def create
    @place_group = PlaceGroup.new(params.require(:place_group).permit!)
    @place_group.user_id = params[:user_id]
    if @place_group.save
      render :json => @place_group.as_json(include: { user: { except: :authentication_token, methods: :avatar_url} })
    else
      render :json => @place_group.errors
    end
  end

  def update
    @place_group.update_attributes(params.require(:place_group).permit!)
    respond_with(@place_group)
  end

  def destroy
    @place_group.destroy
    respond_with(@place_group)
  end

  private
    def set_place_group
      @place_group = PlaceGroup.find(params[:id])
    end

    def authenticate_token!
      @auth_user = User.where(authentication_token: params[:authentication_token]).first
      if @auth_user.nil?
        render :json => {:message => "your authentication token is not valid"}
      else
        true
      end
    end

    def joinable_group!
      @place_group = PlaceGroup.where(user_id: params[:user_id], place_id: params[:place_group][:place_id]).first
      if @place_group.nil?
        true
      else
        render :json => {:message => "your already part of the group"}
      end
    end

end
end