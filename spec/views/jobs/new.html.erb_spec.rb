require 'spec_helper'

describe "jobs/new" do
  before(:each) do
    assign(:job, stub_model(Job,
      :position => "MyString",
      :company_id => 1,
      :details => "MyString",
      :user_id => 1,
      :description => "MyString",
      :avatar => "",
      :poster => "",
      :banner => ""
    ).as_new_record)
  end

  it "renders new job form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", jobs_path, "post" do
      assert_select "input#job_position[name=?]", "job[position]"
      assert_select "input#job_company_id[name=?]", "job[company_id]"
      assert_select "input#job_details[name=?]", "job[details]"
      assert_select "input#job_user_id[name=?]", "job[user_id]"
      assert_select "input#job_description[name=?]", "job[description]"
      assert_select "input#job_avatar[name=?]", "job[avatar]"
      assert_select "input#job_poster[name=?]", "job[poster]"
      assert_select "input#job_banner[name=?]", "job[banner]"
    end
  end
end
