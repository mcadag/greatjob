class ChangeDataTypeForCreatedTo < ActiveRecord::Migration
  def up 
  	change_table :referrals do |t|
  		t.change :created_to, :string
  	end
  end

  def down
    change_table :referrals do |t|
  		t.change :created_to, :int
  	end
  	
  end
end

