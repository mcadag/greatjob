class AddUserIdToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :user_id, :integer
    add_column :documents, :uploaded_by, :integer
    add_column :documents, :is_public, :boolean
    add_column :documents, :permissions, :string
  end
end
