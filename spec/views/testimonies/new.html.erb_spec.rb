require 'spec_helper'

describe "testimonies/new" do
  before(:each) do
    assign(:testimony, stub_model(Testimony,
      :name => "MyString",
      :user_id => 1,
      :video_id => 1,
      :description => "MyString"
    ).as_new_record)
  end

  it "renders new testimony form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", testimonies_path, "post" do
      assert_select "input#testimony_name[name=?]", "testimony[name]"
      assert_select "input#testimony_user_id[name=?]", "testimony[user_id]"
      assert_select "input#testimony_video_id[name=?]", "testimony[video_id]"
      assert_select "input#testimony_description[name=?]", "testimony[description]"
    end
  end
end
