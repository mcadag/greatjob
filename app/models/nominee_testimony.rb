class NomineeTestimony < ActiveRecord::Base
	include PublicActivity::Model
  	tracked
  	attr_accessible :description, :name, :nominee_id, :video_id
  	belongs_to :nominees
end
