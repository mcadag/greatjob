require 'spec_helper'

describe "stored_trainings/show" do
  before(:each) do
    @stored_training = assign(:stored_training, stub_model(StoredTraining,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
