class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :user
      t.text :message
      t.string :type
      t.integer :topic_id
      t.boolean :checked

      t.timestamps
    end
    add_index :notifications, :user_id
  end
end
