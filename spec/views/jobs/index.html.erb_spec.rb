require 'spec_helper'

describe "jobs/index" do
  before(:each) do
    assign(:jobs, [
      stub_model(Job,
        :position => "Position",
        :company_id => 1,
        :details => "Details",
        :user_id => 2,
        :description => "Description",
        :avatar => "",
        :poster => "",
        :banner => ""
      ),
      stub_model(Job,
        :position => "Position",
        :company_id => 1,
        :details => "Details",
        :user_id => 2,
        :description => "Description",
        :avatar => "",
        :poster => "",
        :banner => ""
      )
    ])
  end

  it "renders a list of jobs" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Position".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Details".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
