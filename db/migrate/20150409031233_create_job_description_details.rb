class CreateJobDescriptionDetails < ActiveRecord::Migration
  def change
    create_table :job_description_details do |t|
      t.string :description

      t.timestamps
    end
  end
end
