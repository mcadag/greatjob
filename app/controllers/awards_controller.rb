
  class AwardsController < ApplicationController
    before_filter :set_award, only: [:show, :edit, :update, :destroy]
    before_filter :require_login, :only => [:index, :edit, :update, :destroy]
    respond_to :json, :html

    def index
      @results = []
      @awards = Award.where(user_id: params[:user_id]).latest
      @awards.each do |award|
        @video = Video.where(resource_id: award.id, resource_type: 'achievement')
        if @video.exists?
          award[:video] = @video.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])[0]
          @results << award
        end
      end

      gon.current_user_awards = @results

      respond_to do |format|
        format.html
        format.json { render :json => @results.as_json() }
      end
    end
    
    def show
      @award = Award.find(params[:id])
      @video = Video.testimony(@award.video_id)
      @user = User.where(id: @award.user_id)
      @award[:payload_mobile_url] = get_video_for_award(@award)
      @award[:payload_thumb_url] = get_thumb_for_award(@award)
      @award[:payload_swf_url] = get_swf_for_award(@award)
      set_meta_tags :og => {
        :title => "Recommended by Famous!",
        :description => 'Famous.ph, the video-powered recruitment app for skilled technical and vocational workers. Sign up now!',
        :url => user_award_url(params[:id]),
        :type => "video",
        :image => @award[:payload_thumb_url],
        :video => @award[:payload_mobile_url],
        :video => @interest[:payload_swf_url]
      }
      respond_to do |format|
        format.html
      end
    end
  
    def new
      @award = Award.new
      respond_with(@award)
    end
  
    def edit
    end
  
    def create
      award = Award.new
      award.name = params[:award][:name]
      award.description = params[:award][:description]
      award.user_id = params[:user_id]
      if award.save
        video = Video.new
        video.payload = params[:video][:payload]
        video.name = params[:award][:name]
        video.resource_type = "achievement"
        video.stars = 0
        video.testimonies = 0
        video.like_count = 0
        video.views_count = 0
        video.comments_count = 0
        video.description = params[:award][:description]
        video.created_by = params[:user_id]
        video.resource_id = award.id
        if video.save
          award.update_attributes(:video_id => video.id )
        end
      end
      respond_to do |format|
        format.html { redirect_to user_path(award.user_id), notice: "You have added a new Award." }
        format.json { render :json=> award.as_json(:user_id=>award.user_id, :status=>201, :message=>"Award Added"), :status=>201 }
      end
    end
  
    def update
      award.update_attributes(params.require(:award).permit!)
      render :json => award.as_json(:status => 201, :message => "Updated"), status => 201
    end
  
    def destroy
      @video = Video.where(resource_id: @award.id, resource_type: "achievement")
      video_id = @award.video_id
      if @award.destroy && @award.user_id == @user.id
        @video.destroy(video_id)
        render :json => {:message => "Deleted"}
      else
        render :json => {:message => "You are not authorized to delete the resource"}
      end
      
    end

    def get_video_for_award(award)
      video = Video.where(id: award.video_id)
      award[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
      if video.exists?
        pre_vid = JSON.parse(award[:video])
        return pre_vid["payload_mobile_url"]
      else
        return
      end
    end

    def get_thumb_for_award(award)
      video = Video.where(id: award.video_id)
      award[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
      if video.exists?
        pre_vid = JSON.parse(award[:video])
        return pre_vid["payload_thumb_url"]
      else
        return
      end
    end

    def get_swf_for_award(award)
      video = Video.where(id: award.video_id)
      award[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_swf_url])
      if video.exists?
        pre_vid = JSON.parse(award[:video])
        return pre_vid["payload_swf_url"]
      else
        return
      end
    end

  
    def permission
      award = Awards.where(id: params[:award_id])
      award.update_attributes(params[:award])
      render :json => award.as_json(:status => 201, :message => "Permission is set"), status => 201
    end

    private
      def set_award
        @award = Award.find(params[:id])
      end

      def authenticate_token!
          auth_user = User.where(authentication_token: params[:authentication_token])
          @user = auth_user[0]
          if auth_user.count >= 1
            true
          else
            render :json => {:message => "your authentication token is not valid"}
        end
      end

      def require_login
        unless current_user
          redirect_to new_user_registration_url, :alert => "Please login or signup"
        end
      end
  end
