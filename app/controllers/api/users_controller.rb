module Api
	class UsersController < ApplicationController
    include NomineesHelper
    include UsersHelper
    include CompaniesHelper
		before_filter :authenticate_token_for_user!, :only => [:follow ,:show, :me, :search, :update, :active_nominations, :inactive_nominations, :follow, :unfollow, :following, :followers, :latest, :pending_permissions]
    before_filter :set_followable!, :only =>[:follow, :unfollow, :following, :followers, :followingCompanies]
		respond_to :json

      def featured_video
        resource = User.find_for_database_authentication( authentication_token: params[:authentication_token] )
        user = resource
        if params[:featured_video].blank?
          if resource.update_attributes(featured_video: params[:featured_video])
            render :json => user.as_json(:methods => [:featured_video_url], status: 201), :status =>201
          else
            render :json=> user.errors(:status=>422, :error => :unprocessable_entity), :status=>422
          end        
        end
      end

      def social_login
        if params[:provider] == 'facebook'
         @fb_user = FbGraph2::User.me(params[:token]).fetch(fields: 'id,email,name,last_name,first_name,gender,picture')
          @user= User.find_by_email(@fb_user.email.downcase)
          if @user.nil?
            @user = User.from_fb_graph2(@fb_user)
            @user.ensure_authentication_token!
            if @user.save!
              render :json => @user.as_json(:methods => [:avatar_url, :featured_video_url, :featured_video_thumb_url])
            end
          else
            render :json => @user.as_json(:methods => [:avatar_url, :featured_video_url, :featured_video_thumb_url])
          end
        end
      end

      def nearest
        user = User.where(authentication_token: params[:authentication_token]).first
        if user.nil?
          render :json => {:message => "your authentication token is not valid"}
        elsif (user.lat == nil || user.lat < 0 ) && ( user.lng == nil || user.lng == nil)
          render :json => {:message => "invalid lat and lng"}
        else
          @users = User.within(10, :origin =>[user.lat,user.lng])
          render :json => @users.as_json(:except => [:authentication_token], :methods => [:avatar_url, :featured_video_url, :featured_video_thumb_url])
        end
      end
  		
  		def show
    		user = User.find_by_id(params[:id])
        auth_user = User.where(authentication_token: params[:authentication_token])
        user[:star_points] = user.star_points
        if auth_user.count >= 1
            render :json => user.as_json(:methods => [:avatar_url, :featured_video_url, :featured_video_thumb_url], :except => [:authentication_token])
        else
          render :json => {:message => "your authentication token is not valid"}
        end
  		end

      def me
        user = User.where(authentication_token: params[:authentication_token])
        user[:star_points] = user.star_points
        render :json=> user.as_json(:methods => [:avatar_url, :featured_video_url, :featured_video_thumb_url])
      end

      def update
        user = User.where(authentication_token: params[:authentication_token],id: params[:id]).first
        if user.nil?
          render :json => {:message => "your authentication token is not valid"}
        else
          if user.update_attributes(params.require(:user).permit!)
            user.create_activity :update, owner: user, parameters: { has_no_photo: params[:user][:avatar].blank?, message: "updated his profile", from: user.as_json(:methods => :avatar_url, :except => [:authentication_token, :email] ) }
            render :json=> user.as_json(:auth_token=>user.authentication_token, :email=>user.email, 
                                    :methods => [:avatar_url, :featured_video_url, :featured_video_thumb_url], :address=>user.address, 
                                    :contact_number=>user.contact_number), :status=>201
          else
            render json: user.errors, status: :unprocessable_entity
          end
        end
      end

      def search
        results = []
        users_result = []
        users_result << User.where("email LIKE ? OR name LIKE ? OR contact_number LIKE ?", "%" + params[:q] + "%", "%" + params[:q] + "%", "%" + params[:q] + "%")
        users_result << StoredUser.where("last_name LIKE ? OR first_name LIKE ? OR training LIKE ? OR education LIKE ? ", "%" + params[:q] + "%", "%" + params[:q] + "%", "%" + params[:q] + "%", "%" + params[:q] + "%")
        users_result.each do |user|
          @user = user
          goal = Goal.where(user_id: user.id).first
          if goal
            if user.name.present?
              @user[:title] = user.name + ", " + goal.position
            else
              @user[:title] = user.first_name + " " + + user.last_name + ", " + goal.position
            end
            @user[:goal] = goal.position
          else
            if user.name.present?
              @user[:title] = user.name + ", " + goal.position
            else
              @user[:title] = user.first_name + " " + + user.last_name + ", " + goal.position
            end
            @user[:goal] = nil
          end
          @user[:avatar_url] = @user.as_json(:methods => [:avatar_url])
          results << @user
        end
        render :json => results.as_json(:except => [:authentication_token])
      end

      def active_nominations
        nominees = []
        results = Nominee.where(user_id: params[:user_id]).order('created_at DESC')
        results.each do |nominee|
          competition = Competition.where(id: nominee.competition_id)
          com = competition[0]
          now = DateTime.now
          if (com.start_date < now && now < com.end_date) && !userNominationsJSONResponse(nominee, com).nil?
            nominee[:status] = "Active"
            nominees << userNominationsJSONResponse(nominee, com)
          end
        end
        render :json => nominees.as_json(:include => { :video => {:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url]}})
      end

      def inactive_nominations
        nominees = []
        results = Nominee.where(user_id: params[:user_id]).order('created_at DESC')
        results.each do |nominee|
          competition = Competition.where(id: nominee.competition_id)
          com = competition[0]
          now = DateTime.now
          if (com.start_date < now && now > com.end_date) && !video.empty?
            nominee[:status] = "Inactive"
            nominees << userNominationsJSONResponse(nominee, com)
          end
        end
        render :json => nominees.as_json(:include => { :video => {:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url]}})
      end

      def follow
        followed = @user.follow!(@follow_user)
        if followed
          render :json => @follow_user.as_json(:except => [:authentication_token, :email], :methods => [:avatar_url] )
          @user.create_activity :follow, owner: @user, recipient: @follow_user, parameters: { message: @user.name + " started following " + @follow_user.name, from: @user.as_json(:methods => :avatar_url, :except => [:authentication_token, :email] ) }
          Pusher.trigger('users-' + @follow_user.id.to_s, 'user.follow', { message: @user.name + " started following you", from: @user.as_json() } )
        else
          render :json => {:message => "Cannot follow user"}
        end
      end

      def unfollow
        unfollowed = @user.unfollow!(@follow_user)
        if unfollowed
           render :json => @follow_user.as_json(:except => [:authentication_token, :email], :methods => [:avatar_url] )
        else
          render :json => {:message => "Cannot unfollow user"}
        end

      end

      def following
        results = []
        followees = @follow_user.followees(User)
        followees.each do | user |
          results << UsersJSONResponseForWithNoAuth(user)
        end
        render :json => results
      end

      def followers
        results = []
        followers = @user.followers(User)
        followers.each do | user |
          is_following = @user.follows?(user)
          user.is_following = is_following
          results << UsersJSONResponseForWithNoAuth(user)
        end
        render :json => results
      end

      def followingCompanies
        results = []
        q = @follow_user.followees(Company)
        q.each do |comapny|
          results << CompanyJSONResponse(comapny)
        end
        render :json => results
      end

      def activities
        @activities = PublicActivity::Activity.where(recipient_id: params[:user_id]).order('updated_at DESC')
        render :json => @activities
      end

      def latest
        results = []
        users = User.select("name, id, avatar_file_name, avatar_updated_at").where("avatar_content_type IS NOT NULL").order('created_at DESC').page(1).per(10)
        users.each do |user|
          @user = user
          goal = Goal.where(user_id: user.id).first
          if goal
            @user[:title] = user.name + ", " + goal.position
            @user[:goal] = goal.position
          else
            @user[:title] = user.name
            @user[:goal] = nil
          end

          @user[:avatar_url] = @user.as_json(:methods => [:avatar_url])
          results << @user if @user.avatar.exists?
        end
        render :json => results
      end

      def pending_permissions
        results = []
        testimonies = Testimony.where(user_id: @user.id, permission: nil)
        testimonies.each do |testimony|
          video = Video.where(id: testimony.video_id).first
          testimony[:video] = video.as_json(:methods => [:payload_mobile_url, :payload_thumb_url, :payload_url])
          testimony[:resource_type] = "testimony"
          testimony[:approve_link] = api_user_testimony_allow_url(testimony.user_id, testimony.id)
          testimony[:reject_link] = api_user_testimony_reject_url(testimony.user_id, testimony.id)
          results << testimony if (!testimony[:video].nil? && testimony.created_by_id != @user.id && !testimony.created_by_id.nil?)
        end

        awards = Award.where(user_id: @user.id, permission: nil)
        awards.each do |award|
          video = Video.where(id: award.video_id).first
          award[:video] = video.as_json(:methods => [:payload_mobile_url, :payload_thumb_url, :payload_url])
          award[:resource_type] = "award"
          award[:approve_link] = api_user_award_allow_url(award.user_id, award.id)
          award[:reject_link] = api_user_award_reject_url(award.user_id, award.id)
          results << award if (!award[:video].nil? && award.created_by_id != @user.id  && !award.created_by_id.nil?)
        end

        interests = Interest.where(user_id: @user.id, permission: nil)
        interests.each do |interest|
          video = Video.where(id: interest.video_id).first
          interest[:video] = video.as_json(:methods => [:payload_mobile_url, :payload_thumb_url, :payload_url])
          interest[:resource_type] = "interest"
          interest[:approve_link] = api_user_interest_allow_url(interest.user_id, interest.id)
          interest[:reject_link] = api_user_interest_reject_url(interest.user_id, interest.id)
          results << interest if (!interest[:video].nil? && interest.created_by_id != @user.id && !interest.created_by_id.nil?) 
        end
        render :json => results
      end

		private
        def set_followable!
          follow_user = User.where(id: params[:user_id])
          @follow_user = follow_user[0]
        end
        def authenticate_token_for_user!
          auth_user = User.where(authentication_token: params[:authentication_token])
          @user = auth_user[0]
          if auth_user.count >= 1
            true
          else
            render :json => {:message => "your authentication token is not valid"}
          end
        end
		end
end
