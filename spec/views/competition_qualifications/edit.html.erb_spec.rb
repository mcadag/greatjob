require 'spec_helper'

describe "competition_qualifications/edit" do
  before(:each) do
    @competition_qualification = assign(:competition_qualification, stub_model(CompetitionQualification))
  end

  it "renders the edit competition_qualification form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", competition_qualification_path(@competition_qualification), "post" do
    end
  end
end
