class CompetitionQualification < ActiveRecord::Base
  include PublicActivity::Model
  tracked
  attr_accessible :name, :competition_id
  belongs_to :competitions
end
