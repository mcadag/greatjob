class CompanyTestimony < ActiveRecord::Base
  include PublicActivity::Model
  tracked
  attr_accessible :allowed, :company_id, :created_by, :description, :name, :permission, :private, :user_id, :video_id
end
