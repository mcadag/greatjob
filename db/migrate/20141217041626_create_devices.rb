class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.integer :user_id
      t.string :unique_identifier
      t.string :platform
      t.boolean :active

      t.timestamps
    end
  end
end
