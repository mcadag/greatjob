require 'spec_helper'

describe "company_dream_jobs/show" do
  before(:each) do
    @company_dream_job = assign(:company_dream_job, stub_model(CompanyDreamJob,
      :name => "Name",
      :comapny_id => 1,
      :user_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/1/)
    rendered.should match(/2/)
  end
end
