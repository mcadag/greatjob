  class CompanyTestimoniesController < ApplicationController
    before_filter :set_competition, only: [:show, :edit, :update, :destroy]
    before_filter :authenticate_token!, :only => [:create, :update, :destroy, :index]
    respond_to :json
  # GET /company_testimonies
  # GET /company_testimonies.json
  def index
    results = []
    @testimonies = CompanyTestimony.where(company_id: params[:company_id]).order('created_at DESC')
    @testimonies.each do |testimony|
      video = Video.where(resource_id: testimony.id, resource_type: 'company_testimony')
        if video.count >= 1
          testimony[:video] = video.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
          results << testimony          
        end
    end
    respond_with(results)
  end

  # GET /company_testimonies/1
  # GET /company_testimonies/1.json
  def show
    @company_testimony = CompanyTestimony.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @company_testimony }
    end
  end

  # GET /company_testimonies/new
  # GET /company_testimonies/new.json
  def new
    @company_testimony = CompanyTestimony.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @company_testimony }
    end
  end

  # GET /company_testimonies/1/edit
  def edit
    @company_testimony = CompanyTestimony.find(params[:id])
  end

  # POST /company_testimonies
  # POST /company_testimonies.json
  def create
      @testimony = CompanyTestimony.new
      @testimony.name = params[:testimony][:name]
      @testimony.description = params[:testimony][:description]
      @testimony.user_id = @user.id
      @testimony.company_id = params[:company_id]
      @testimony.created_by = params[:user_id]
      @testimony.user_id = @user.id
      if @testimony.save
        video = Video.new
        video.payload = params[:video][:payload]
        video.name = @testimony.name
        video.resource_type = "company_testimony"
        video.stars = 0
        video.testimonies = 0
        video.like_count = 0
        video.views_count = 0
        video.comments_count = 0
        video.description = @testimony.description
        video.created_by = @testimony.created_by
        video.resource_id = @testimony.id
        if video.save
          @testimony.update_attributes(:video_id => video.id )
        end
      end
      render :json=> @testimony.as_json(:user_id=>@testimony.user_id, :status=>201), :status=>201
  end

  # PATCH/PUT /company_testimonies/1
  # PATCH/PUT /company_testimonies/1.json
  def update
    @company_testimony = CompanyTestimony.find(params[:id])
    @company_testimony.update_attributes(params.require(:testimony).permit!)
    render :json => company_testimony.as_json(:status => 201, :message => "Updated"), status => 201
  end

  # DELETE /company_testimonies/1
  # DELETE /company_testimonies/1.json
  def destroy
      @testimony = CompanyTestimony.find(params[:id])
      @video = Video.where(resource_id: @testimony.id, resource_type: "company_testimony")
      video_id = @testimony.video_id
      if @testimony.destroy && @testimony.user_id == @user.id
        @video.destroy(video_id)
        render :json => {:message => "Deleted"}
      else
        render :json => {:message => "You are not authorized to delete the resource"}
      end      
  end

  private
    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def company_testimony_params
      params.require(:company_testimony).permit(:allowed, :company_id, :created_by, :description, :name, :permission, :private, :user_id, :video_id)
    end

    def authenticate_token!
      auth_user = User.where(authentication_token: params[:authentication_token])
      @user = auth_user[0]
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end
  end
