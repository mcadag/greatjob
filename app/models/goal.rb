class Goal < ActiveRecord::Base
	belongs_to :user
	has_many :videos
  	attr_accessible :name, :position, :user_id, :location, :superlative, :adjective

end
