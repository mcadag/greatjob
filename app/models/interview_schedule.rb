class InterviewSchedule < ActiveRecord::Base
  attr_accessible :accept, :candidate_id, :job_id, :reschedule, :schedule, :user_id
end
