module Api
  class JobsController < ApplicationController
  before_filter :authenticate_token_for_user!
  before_filter :candidate_video_resource_params, only: [:promote_video_resource]

  # GET /jobs
  # GET /jobs.json
  def index
    @jobs = Job.all
    result = []
    @jobs.each do |job|
      company = Company.where(id: job.company_id).first
      user = User.where(id: job.user_id).first
      job[:company] = company.as_json(:methods => [:payload_url])
      job[:user] = user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
      job[:apply_link] = api_job_apply_url(job.id)
      job[:share_link] = job_url(job.id)
      job[:lat] = 14.123456
      job[:lon] = 121.21356
      candidate =  Candidate.where(user_id: @user.id, job_id: job.id)
      if candidate.count == 0
        result << job.as_json(:only => [:id, :company, :user, :name, :apply_link, :share_link, :position, :lat, :lon, :created_at, :salary])
      end
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: result }
    end
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
    @job = Job.find(params[:id])
    company = Company.where(id: @job.company_id).first
    user = User.where(id: @job.user_id).first
    qualifications = JobsQualification.where(job_id: @job.id)
    requirements = Requirements.where(job_id: @job.id)
    questions = JobQuestions.where(job_id: @job.id)
    questions_result = []
    total_questions = JobQuestions.where(job_id: @job.id).count
    total_answered = JobQuestionsAnswer.where(job_id: @job.id, user_id: @user.id).count
    details_result = []
    job_details = JobDescriptionDetails.where(job_id: @job.id)
    job_details.each do |detail|
      details_result << detail
    end
    questions.each do |question|
      answer = JobQuestionsAnswer.where(job_id: @job.id, user_id: @user.id, job_question_id: question.id).first
      if answer.present?
        question[:answer] = answer.answer
      end
      question[:answered] = answer.present?
      questions_result << question
    end
    if total_answered.to_s == total_questions.to_s 
      @job[:completed_answering] = true
    else
      @job[:completed_answering] = false
    end
    qualifications_result = []

    qualifications.each do | qualification |
      qualification[:is_qualified] = true
      qualifications_result << qualification.as_json(:only => [:name, :id, :is_qualified])
    end

    @job[:details_result] = details_result.as_json()
    @job[:company] = company.as_json(:methods => [:payload_url])
    @job[:user] = user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
    @job[:qualifications] = qualifications_result
    @job[:requirements] = requirements.as_json(:only => [:name, :id])
    @job[:apply_link] = api_job_apply_url(@job.id)
    @job[:share_link] = job_url(@job.id)
    @job[:cancel_link] = api_job_cancel_application_url(@job.id)
    @job[:lat] = 14.123456
    @job[:lon] = 121.21356
    @job[:address] = company.location
    @job[:questions] = questions_result.as_json(:only => [:id, :question, :answered, :answer], :methods => [:video_answer_url, :video_answer_thumb_url, :video_answer_mobile_url])
    @job[:is_video_invterview] = @job.video_answer
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @job.as_json(:only => [:id, :company, :user, :name, 
                                                        :description, :details, :apply_link, 
                                                        :share_link, :requirements, :position, 
                                                        :lat, :lon, :address, :created_at, 
                                                        :qualifications, :salary, :questions, 
                                                        :completed_answering, :cancel_link,
                                                        :is_video_invterview, :details_result]) }
    end
  end

  # GET /jobs/new
  # GET /jobs/new.json
  def new
    @job = Job.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @job }
    end
  end

  # GET /jobs/1/edit
  def edit
    @job = Job.find(params[:id])
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(job_params)

    respond_to do |format|
      if @job.save
        format.html { redirect_to @job, notice: 'Job was successfully created.' }
        format.json { render json: @job, status: :created, location: @job }
      else
        format.html { render action: "new" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    @job = Job.find(params[:id])

    respond_to do |format|
      if @job.update_attributes(job_params)
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job = Job.find(params[:id])
    @job.destroy

    respond_to do |format|
      format.html { redirect_to jobs_url }
      format.json { head :no_content }
    end
  end


  def apply
    candidates = Candidate.where(job_id: params[:job_id], user_id: @user.id)
    if candidates.count == 0
      candidate = Candidate.new
      candidate.user_id = @user.id
      candidate.job_id = params[:job_id]
      if candidate.save
        render :json => {:message => "You have applied to a job"}
      else
        render :json => {:message => "something went wrong with your application"}
      end
    end
  end

  def refer
    referral = Referral.where(job_id: params[:job_id], created_to: params[:created_to])
    job = Job.includes(:company).find(params[:job_id])
    if referral.count == 0
      referral = Referral.new
      referral.job_id = params[:job_id]
      referral.company_id = job.company.id
      referral.created_by = @user.id
      referral.created_to = params[:created_to]
      if referral.save
        by_user = User.where(email: params[:created_to])
        @user_by = by_user[0]
        render :json => {:message => "referal submitted"}  
        if @job.company.email.blank?
          NotificationsMailer.refered(@user_by, @user, job, job.company).deliver
        end
      else
        render :json => {:message => "something went wrong with your referal"}  
      end
    else 
      render :json => {:message => "referal already exists"}  
    end
  end

  def subscribed
    result_id = Subscription.where(:user_id => @user.id).pluck(:stored_profession_id)
    @jobs = Job.where(:stored_profession_id => result_id)
    result = []
    @jobs.each do |job|
      company = Company.where(id: job.company_id).first
      user = User.where(id: job.user_id).first
      profession = StoredProfession.where(id: job.stored_profession_id).first
      job[:company] = company.as_json(:methods => [:payload_url])
      job[:stored_profession] = profession.as_json()
      job[:user] = user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
      job[:apply_link] = api_job_apply_url(job.id)
      job[:share_link] = job_url(job.id)
      job[:lat] = 14.123456
      job[:lon] = 121.21356
      result << job.as_json(:only => [:id, :company, :user, :name, :apply_link, :share_link, :position, :lat, :lon, :created_at, :salary, :stored_profession])
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: result }
    end
  end

  def subscribe
    Subscription.where(:user_id => @user.id).destroy_all
      params["subscription"].each do |subscription|
         sub = Subscription.new
         sub.user_id = @user.id
         sub.stored_profession_id = subscription["stored_profession_id"]
         sub.save
      end
    result = Subscription.where(:user_id => @user.id)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: result }
    end
  end

  def invite
    if @user.empolyer
    referral = Referral.where(job_id: params[:job_id], created_to: params[:created_to])
    job = Job.includes(:company).find(params[:job_id])
    if referral.count == 0
      referral = Referral.new
      referral.job_id = params[:job_id]
      referral.company_id = job.company.id
      referral.created_by = @user.id
      referral.created_to = params[:created_to]
      if referral.save
        by_user = User.where(email: params[:created_to])
        @user_by = by_user[0]
        # TODO mailer here for invitation
        # NotificationsMailer.refered(@user_by, @user, job, job.company).deliver
        render :json => {:message => "invitation submitted"} 
      else
        render :json => {:message => "something went wrong with your invitation"}  
      end
    else 
      render :json => {:message => "referal already exists"}  
    end
    else
        render :json => {:message => "you cannot invite since you're not an employer"}  
    end
  end

  def applied
    result = []
    appilications = Candidate.where(user_id: @user.id)
    appilications.each do | application |
      job = Job.where(id: application.job_id).first
      company = Company.where(id: job.company_id).first
      user = User.where(id: job.user_id).first
      questions = JobQuestions.where(job_id: job.id)
      questions_result = []
      total_questions = JobQuestions.where(job_id: job.id).count
      total_answered = JobQuestionsAnswer.where(job_id: job.id, user_id: @user.id).count
      questions.each do |question|
        answer = JobQuestionsAnswer.where(job_id: job.id, user_id: @user.id, job_question_id: question.id).first
        if answer.present?
          question[:answer] = answer.answer
        end
        question[:answered] = answer.present?
        questions_result << question
      end
      if total_answered.to_s == total_questions.to_s 
        job[:completed_answering] = true
      else
        job[:completed_answering] = false
      end
      job[:questions] = questions_result.as_json(:only => [:id, :question, :answered, :answer], :methods => [:video_answer_url, :video_answer_thumb_url, :video_answer_mobile_url])
      job[:progress] = total_answered.to_s + "/" + total_questions.to_s
      job[:company] = company.as_json(:methods => [:payload_url])
      job[:user] = user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
      job[:apply_link] = api_job_apply_url(job.id)
      job[:share_link] = job_url(job.id)
      job[:cancel_link] = api_job_cancel_application_url(job.id)
      job[:application_type] = application.application_type
      job[:lat] = 14.123456
      job[:lon] = 121.21356
      result << job.as_json(:only => [:id, :application_type, :company, :user, :name, :apply_link, :share_link, :cancel_link, :position, :lat, :lon, :created_at, :salary, :progress, :completed_answering, :progress, :questions])
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: result }
    end
  end

  def candidates
    result = []
    candidates = Candidate.where(job_id: params[:job_id])
    candidates.each do | candidate |
      user = User.where(id: candidate.user_id).first
      candidate[:user] = user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
      candidate[:short_list_url] = api_job_short_list_candidate_url(params[:job_id], candidate.id)
      candidate[:profile_url] = user_url(candidate.user_id)
      result << candidate.as_json(:only => [:id, :user, :job_id, :short_list_url, :profile_url])
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: result }
    end
  end

  def candidate_short_listed
    result = []
    schedules = InterviewSchedule.where(job_id: params[:job_id], user_id: @user.id)
    schedules.each do |schedule|
      job = Job.where(id: schedule.job_id).first
      user = User.where(id: schedule.user_id).first
      company = Company.where(id: job.company_id).first
      qualifications = JobsQualification.where(job_id: schedule.job_id)
      requirements = Requirements.where(job_id: schedule.job_id)

      schedule[:user] = user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
      schedule[:short_list] = api_job_short_list_candidate_url(params[:job_id], schedule.id)
      schedule[:profile_url] = user_url(schedule.user_id)
      schedule[:accept_url] = api_job_accept_interview_schedule_url(schedule.job_id, schedule.id)
      schedule[:re_shedule_url] = api_job_reschedule_interview_url(schedule.job_id, schedule.id)
      schedule[:company] = company.as_json(:methods => [:payload_url])
      schedule[:qualifications] = qualifications.as_json(:only => [:name, :id])
      schedule[:requirements] = requirements.as_json(:only => [:name, :id])
      schedule[:share_link] = job_url(job.id)
      schedule[:lat] = 14.123456
      schedule[:lon] = 121.21356
      schedule[:address] = company.location

      schedule[:job] = job.as_json(:only => [:id, :name, :description, :details, :share_link, :position, :created_at])
      result << schedule.as_json(:only => [:id, :user, :job_id, :accept_url, :re_shedule_url, :profile_url, :job, :company, :requirements, :lat, :lon, :qualifications, :salary, :address])
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: result }
    end
  end

  def short_listed
    result = []
    candidates = InterviewSchedule.where(job_id: params[:job_id])
    candidates.each do |candidate|
      user = User.where(id: candidate.user_id).first
      candidate[:user] = user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
      candidate[:short_list] = api_job_short_list_candidate_url(params[:job_id], candidate.id)
      candidate[:profile_url] = user_url(candidate.user_id)
      candidate[:accept_url] = api_job_accept_interview_schedule_url(candidate.job_id, candidate.id)
      candidate[:re_shedule_url] = api_job_reschedule_interview_url(candidate.job_id, candidate.id)
      result << candidate.as_json(:only => [:id, :user, :job_id, :accept_url, :re_shedule_url, :profile_url])
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: result }
    end
  end

  def accepted_schedule
    result = []
    candidates = InterviewSchedule.where(job_id: params[:job_id], accept: true)
    candidates.each do |candidate|
      user = User.where(id: candidate.user_id).first
      candidate[:user] = user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
      candidate[:profile_url] = user_url(candidate.user_id)
      candidate[:accept_url] = api_job_accept_interview_schedule_url(candidate.job_id, candidate.id)
      candidate[:re_shedule_url] = api_job_reschedule_interview_url(candidate.job_id, candidate.id)
      result << candidate.as_json(:only => [:id, :user, :job_id, :accept_url, :re_shedule_url, :profile_url])
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: result }
    end
  end

  def re_schedule
    result = []
    candidates = InterviewSchedule.where(job_id: params[:job_id], reschedule: true)
    candidates.each do |candidate|
      user = User.where(id: candidate.user_id).first
      candidate[:user] = user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
      candidate[:profile_url] = user_url(candidate.user_id)
      candidate[:accept_url] = api_job_accept_interview_schedule_url(candidate.job_id, candidate.id)
      candidate[:re_shedule_url] = api_job_reschedule_interview_url(candidate.job_id, candidate.id)
      result << candidate.as_json(:only => [:id, :user, :job_id, :accept_url, :re_shedule_url, :profile_url])
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: result }
    end
  end

  def short_list
    temp_candidate = Candidate.where(id: params[:candidate_id], job_id: params[:job_id]).first
    existing_candidate = InterviewSchedule.where(job_id: params[:job_id], user_id: temp_candidate.user_id)
    if existing_candidate.count <= 0
      candidate = InterviewSchedule.new
      candidate[:user_id] = temp_candidate.user_id
      candidate[:job_id] = params[:job_id]
      candidate[:schedule] = params[:schedule]
      candidate[:accept] = false
      candidate[:reschedule] = false
      if candidate.save
        render :json => {:message => "candidate has been short listed"}
      else 
        render :json => {:message => "there is an error shortlisting the candidate"}
      end
    else
      render :json => {:message => "Candidate has already been short listed"}
    end
  end

  def accept_interview_schedule
    interviewSchedule = InterviewSchedule.where(job_id: params[:job_id], user_id: @user.id, accept: false).first
    if !interviewSchedule.accept
      interviewSchedule.update_attributes(:accept => true)
      render :json => {:message => "You have accepted the job interview schedule"}
    else
      render :json => {:message => "You have already accepted the job interview schedule."}
    end
  end

  def reschedule_interview
    interviewSchedule = InterviewSchedule.where(job_id: params[:job_id], user_id: @user.id, accept: false).first
    if !interviewSchedule.accept
      interviewSchedule.update_attributes(:accept => true, :schedule => params[:schedule], :reschedule => true)
      render :json => {:message => "You have re scheduled the interview"}
    else
      render :json => {:message => "something went wrong"}
    end    
  end

  def post_answer
    answered = JobQuestionsAnswer.where(job_id: params[:job_id], user_id: @user.id, job_question_id: params[:job_question_id]).present? 
    if !answered
      answer = JobQuestionsAnswer.new
      answer[:job_id] = params[:job_id]
      answer[:user_id] = @user.id
      answer[:job_question_id] = params[:job_question_id]
      if params[:video_answer].present?
        answer[:video_answer] = params[:video_answer]
      else
        answer[:answer] = params[:answer]
      end
      if answer.save
        render :json => {:message => "You have answered a question"}
      else
        render :json => {:message => "something went wrong"}
      end
    else
      render :json => {:message => "you have already answered this question"}
    end
  end

  def cancel_application
    candidate = Candidate.where(job_id: params[:job_id], user_id: @user.id).first
    candidate.destroy
    render :json => {:message => "You have removed your job application"}
  end

  def search
    result = []
    jobs = Job.where("position LIKE ?", "%" + params[:q] + "%")
    jobs.each do | job |
      company = Company.where(id: job.company_id).first
      user = User.where(id: job.user_id).first
      job[:company] = company.as_json(:methods => [:payload_url])
      job[:user] = user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
      job[:apply_link] = api_job_apply_url(job.id)
      job[:share_link] = job_url(job.id)
      job[:lat] = 14.123456
      job[:lon] = 121.21356
      candidate =  Candidate.where(user_id: @user.id, job_id: job.id)
      if candidate.count == 0
        result << job.as_json(:only => [:id, :company, :user, :name, :apply_link, :share_link, :position, :lat, :lon, :created_at, :salary])
      end
      result << job
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: result }
    end
  end

  def promote_video_resource
    promoted_resource = CandidatePreQualificationResource.new
    promoted_resource[:resource_id] = params[:resource_id]
    promoted_resource[:resource_type] = params[:resource_type]
    promoted_resource[:promoted_resource_id] = params[:promoted_resource_id]
    promoted_resource[:promoted_resource_type] = params[:promoted_resource_type]
    promoted_resource[:candidate_id] = params[:candidate_id]

    promoted_resource.save

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: promoted_resource }
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def job_params
      params.require(:job).permit(:avatar, :banner, :company_id, :description, :details, :position, :poster, :user_id)
    end

    def candidate_video_resource_params
      params.require(:job).permit(:resource_id, :resource_type, :promoted_resource_id, :promoted_resource_type)
    end

    def authenticate_token_for_user!
      auth_user = User.where(authentication_token: params[:authentication_token])
      @user = auth_user[0]
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

  end
end