class PlaceGroup < ActiveRecord::Base

  validates :name, presence: true
  validates :place_id, presence: true
  validates :lat, presence: true
  validates :lng, presence: true

  has_many :group_members
  has_many :users, through: :group_members

  attr_accessible :name, :place_id, :lat, :lng, :address
  belongs_to :user
  resourcify
end
