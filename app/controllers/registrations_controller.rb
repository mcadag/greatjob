class RegistrationsController < Devise::RegistrationsController
   def create
    build_resource sign_up_params
    if resource.save
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_up(resource_name, resource)
        respond_with resource, :location => after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
        expire_session_data_after_sign_in!
        respond_with resource, :location => after_inactive_sign_up_path_for(resource)
      end
    else
      set_flash_message :notice, :"invalid_registration" if is_navigational_format?
      clean_up_passwords resource
      respond_with resource
    end
    Rails.logger.info(@user.errors.inspect)
  end

  private

  def registration_params
    params.require(:user).permit(:email, :first_name, :last_name, :password, :password_confirmation ,:employer)
    #params.require(:user).permit(:email, :title_id, :first_name, :last_name, 
     # :province_id, :password, :password_confirmation, :name)
  end

end