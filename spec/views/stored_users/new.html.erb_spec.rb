require 'spec_helper'

describe "stored_users/new" do
  before(:each) do
    assign(:stored_user, stub_model(StoredUser,
      :last_name => "MyString",
      :first_name => "MyString",
      :middle_initial => "MyString",
      :training => "MyString",
      :mobile_number => 1,
      :brgy => "MyString",
      :town => "MyString",
      :province => "MyString",
      :country => "MyString",
      :gender => "MyString",
      :education => "MyString",
      :education => "MyString",
      :email => "MyString",
      :age => 1
    ).as_new_record)
  end

  it "renders new stored_user form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stored_users_path, "post" do
      assert_select "input#stored_user_last_name[name=?]", "stored_user[last_name]"
      assert_select "input#stored_user_first_name[name=?]", "stored_user[first_name]"
      assert_select "input#stored_user_middle_initial[name=?]", "stored_user[middle_initial]"
      assert_select "input#stored_user_training[name=?]", "stored_user[training]"
      assert_select "input#stored_user_mobile_number[name=?]", "stored_user[mobile_number]"
      assert_select "input#stored_user_brgy[name=?]", "stored_user[brgy]"
      assert_select "input#stored_user_town[name=?]", "stored_user[town]"
      assert_select "input#stored_user_province[name=?]", "stored_user[province]"
      assert_select "input#stored_user_country[name=?]", "stored_user[country]"
      assert_select "input#stored_user_gender[name=?]", "stored_user[gender]"
      assert_select "input#stored_user_education[name=?]", "stored_user[education]"
      assert_select "input#stored_user_education[name=?]", "stored_user[education]"
      assert_select "input#stored_user_email[name=?]", "stored_user[email]"
      assert_select "input#stored_user_age[name=?]", "stored_user[age]"
    end
  end
end
