class StoredCompanyCulture < ActiveRecord::Base
  attr_accessible :name
  has_many :company_cultures
end
