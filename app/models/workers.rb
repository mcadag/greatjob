class Workers < ActiveRecord::Base
  attr_accessible :COPR_number, :TBP_id, :address, :address_of_employer, :age, :assesment_result, :barangay, :birth_date, :civil_status, :client_type, :date_ended, :date_started, :delivery_mode, :district, :district, :education, :email, :employment_date, :first_name, :gender, :home_municipality, :last_name, :middle_initial, :mobile_number, :municipality, :name_of_employer, :provider, :provider_classification, :province, :province, :qualification, :reason, :region, :scholarship, :street_address, :student_id_number, :type_of_institution
end
