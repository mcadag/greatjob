module Api
class NotificationsController < ApplicationController
  before_filter :authenticate_token_for_user!
  respond_to :json
  # GET /notifications
  # GET /notifications.json
  def index
    @notifications = PublicActivity::Activity.order("created_at desc").where(recipient_id: params[:user_id])
    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @notifications.as_json() }
    end
  end
  # GET /notifications/1
  # GET /notifications/1.json
  def show
    @notification = Notification.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @notification }
    end
  end

  # GET /notifications/new
  # GET /notifications/new.json
  def new
    @notification = Notification.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @notification }
    end
  end

  # GET /notifications/1/edit
  def edit
    @notification = Notification.find(params[:id])
  end

  # POST /notifications
  # POST /notifications.json
  def create
    @notification = Notification.new(notification_params)

    respond_to do |format|
      if @notification.save
        format.html { redirect_to @notification, notice: 'Notification was successfully created.' }
        format.json { render json: @notification, status: :created, location: @notification }
      else
        format.html { render action: "new" }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notifications/1
  # PATCH/PUT /notifications/1.json
  def update
    @notification = PublicActivity::Activity.find(params[:id])
    @notification.is_viewed = params[:is_viewed]
    if @notification.save
      render :json => @notification.as_json
    else
      render :json => @notification.errors
    end
  end

  # DELETE /notifications/1
  # DELETE /notifications/1.json
  def destroy
    @notification = Notification.find(params[:id])
    @notification.destroy

    respond_to do |format|
      format.html { redirect_to notifications_url }
      format.json { head :no_content }
    end
  end

  private
    
    def authenticate_token_for_user!
      auth_user = User.where(authentication_token: params[:authentication_token])
      @user = auth_user[0]
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

    def notification_params
      params.require(:notification).permit(:is_viewed)
    end
end
end