class Prize < ActiveRecord::Base
  attr_accessible :amount, :competition_id, :details, :name
end
