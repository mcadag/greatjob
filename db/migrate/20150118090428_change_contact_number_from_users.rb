class ChangeContactNumberFromUsers < ActiveRecord::Migration
  def up
  	change_column(:users, :contact_number, :string)
  end

  def down
  end
end
