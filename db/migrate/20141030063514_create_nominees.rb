class CreateNominees < ActiveRecord::Migration
  def change
    create_table :nominees do |t|
      t.integer :user_id
      t.integer :competition_id
      t.integer :video_id

      t.timestamps
    end
  end
end
