require 'spec_helper'

describe "testimonies/index" do
  before(:each) do
    assign(:testimonies, [
      stub_model(Testimony,
        :name => "Name",
        :user_id => 1,
        :video_id => 2,
        :description => "Description"
      ),
      stub_model(Testimony,
        :name => "Name",
        :user_id => 1,
        :video_id => 2,
        :description => "Description"
      )
    ])
  end

  it "renders a list of testimonies" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
