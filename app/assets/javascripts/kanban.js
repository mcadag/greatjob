/*
 * @author Shaumik "Dada" Daityari
 * @copyright December 2013
 */

/* Some info
Using newer versions of jQuery and jQuery UI in place of the links given in problem statement
All data is stored in local storage
User data is extracted from local storage and saved in variable todo.data
Otherwise, comments are provided at appropriate places
 */
var data, todo;

todo = todo || {};

data = JSON.parse(localStorage.getItem('todoData'));

data = data || {};

(function(todo, data, $) {
  var codes, defaults, generateDialog, generateElement, removeElement;
  defaults = {
    todoTask: 'todo-task',
    todoHeader: 'task-header',
    todoDate: 'task-date',
    todoDescription: 'task-description',
    taskId: 'task-',
    formId: 'todo-form',
    dataAttribute: 'data',
    deleteDiv: 'delete-div'
  };
  codes = {
    '1': '#pending',
    '2': '#inProgress',
    '3': '#completed'
  };
  todo.init = function(options) {
    options = options || {};
    options = $.extend({}, defaults, options);
    $.each(data, function(index, params) {
      generateElement(params);
    });

    for (var i = gon.requirements.length - 1; i >= 0; i--) {
      generateElement({
        id: gon.requirements.indexOf(gon.requirements[i]),
        code: "1",
        title: gon.requirements[i],
        date: "22/12/2013",
        description: "Blah Blah"
      });
    };

    $.each(codes, function(index, value) {
      $(value).droppable({
        drop: function(event, ui) {
          var css_id, element, id, object;
          element = ui.helper;
          css_id = element.attr('id');
          id = css_id.replace(options.taskId, '');
          object = data[id];
          removeElement(object);
          object.code = index;
          generateElement(object);
          data[id] = object;
          localStorage.setItem('todoData', JSON.stringify(data));
          $('#' + defaults.deleteDiv).hide();
        }
      });
    });

    $('#' + options.deleteDiv).droppable({
      drop: function(event, ui) {
        var css_id, element, id, object;
        element = ui.helper;
        css_id = element.attr('id');
        id = css_id.replace(options.taskId, '');
        object = data[id];
        removeElement(object);
        delete data[id];
        localStorage.setItem('todoData', JSON.stringify(data));
        $('#' + defaults.deleteDiv).hide();
      }
    });
  };

  generateElement = function(params) {
    var parent, wrapper;
    parent = $(codes[params.code]);
    wrapper = void 0;
    if (!parent) {
      return;
    }
    wrapper = $('<div />', {
      'class': defaults.todoTask,
      'id': defaults.taskId + params.id,
      'data': params.id
    }).appendTo(parent);
    $('<div />', {
      'class': defaults.todoHeader,
      'text': params.title
    }).appendTo(wrapper);
    $('<div />', {
      'class': defaults.todoDate,
      'text': params.date
    }).appendTo(wrapper);
    $('<div />', {
      'class': defaults.todoDescription,
      'text': params.description
    }).appendTo(wrapper);
    wrapper.draggable({
      start: function() {
        $('#' + defaults.deleteDiv).show();
      },
      stop: function() {
        $('#' + defaults.deleteDiv).hide();
      },
      revert: 'invalid',
      revertDuration: 200
    });
  };

  removeElement = function(params) {
    $('#' + defaults.taskId + params.id).remove();
  };

  todo.add = function() {
    var date, description, errorMessage, id, inputs, tempData, title;
    inputs = $('#' + defaults.formId + ' :input');
    errorMessage = 'Title can not be empty';
    id = void 0;
    title = void 0;
    description = void 0;
    date = void 0;
    tempData = void 0;
    if (inputs.length != 4) {
      return;
    }
    title = inputs[0].value;
    description = inputs[1].value;
    date = inputs[2].value;
    if (!title) {
      generateDialog(errorMessage);
      return;
    }
    id = (new Date).getTime();
    tempData = {
      id: id,
      code: '1',
      title: title,
      date: date,
      description: description
    };
    data[id] = tempData;
    localStorage.setItem('todoData', JSON.stringify(data));
    generateElement(tempData);
    inputs[0].value = '';
    inputs[1].value = '';
    inputs[2].value = '';
  };

  generateDialog = function(message) {
    var buttonOptions, responseDialog, responseId, title;
    responseId = 'response-dialog';
    title = 'Messaage';
    responseDialog = $('#' + responseId);
    buttonOptions = void 0;
    if (!responseDialog.length) {
      responseDialog = $('<div />', {
        title: title,
        id: responseId
      }).appendTo($('body'));
    }
    responseDialog.html(message);
    buttonOptions = {
      'Ok': function() {
        responseDialog.dialog('close');
      }
    };
    responseDialog.dialog({
      autoOpen: true,
      width: 400,
      modal: true,
      closeOnEscape: true,
      buttons: buttonOptions
    });
  };

  todo.clear = function() {
    data = {};
    localStorage.setItem('todoData', JSON.stringify(data));
    $('.' + defaults.todoTask).remove();
  };
})(todo, data, jQuery);

// ---
// generated by coffee-script 1.9.0