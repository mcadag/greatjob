class CreateStoredTrainings < ActiveRecord::Migration
  def change
    create_table :stored_trainings do |t|
      t.string :name

      t.timestamps
    end
  end
end
