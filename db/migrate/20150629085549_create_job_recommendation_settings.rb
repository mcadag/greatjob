class CreateJobRecommendationSettings < ActiveRecord::Migration
  def change
    create_table :job_recommendation_settings do |t|
      t.string :referal_bonus
      t.integer :candidates
      t.string :step
      t.integer :job_id

      t.timestamps
    end
  end
end
