require "spec_helper"

describe StoredTrainingsController do
  describe "routing" do

    it "routes to #index" do
      get("/stored_trainings").should route_to("stored_trainings#index")
    end

    it "routes to #new" do
      get("/stored_trainings/new").should route_to("stored_trainings#new")
    end

    it "routes to #show" do
      get("/stored_trainings/1").should route_to("stored_trainings#show", :id => "1")
    end

    it "routes to #edit" do
      get("/stored_trainings/1/edit").should route_to("stored_trainings#edit", :id => "1")
    end

    it "routes to #create" do
      post("/stored_trainings").should route_to("stored_trainings#create")
    end

    it "routes to #update" do
      put("/stored_trainings/1").should route_to("stored_trainings#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/stored_trainings/1").should route_to("stored_trainings#destroy", :id => "1")
    end

  end
end
