require 'spec_helper'

describe "competition_video_comments/edit" do
  before(:each) do
    @competition_video_comment = assign(:competition_video_comment, stub_model(CompetitionVideoComment,
      :payload => "",
      :name => "MyString",
      :competition_id => 1,
      :user_id => 1,
      :message => "MyString",
      :nominee_id => 1,
      :details => "MyString",
      :description => "MyString",
      :approved => false,
      :is_private => "",
      :has_video => false,
      :video_id => 1
    ))
  end

  it "renders the edit competition_video_comment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", competition_video_comment_path(@competition_video_comment), "post" do
      assert_select "input#competition_video_comment_payload[name=?]", "competition_video_comment[payload]"
      assert_select "input#competition_video_comment_name[name=?]", "competition_video_comment[name]"
      assert_select "input#competition_video_comment_competition_id[name=?]", "competition_video_comment[competition_id]"
      assert_select "input#competition_video_comment_user_id[name=?]", "competition_video_comment[user_id]"
      assert_select "input#competition_video_comment_message[name=?]", "competition_video_comment[message]"
      assert_select "input#competition_video_comment_nominee_id[name=?]", "competition_video_comment[nominee_id]"
      assert_select "input#competition_video_comment_details[name=?]", "competition_video_comment[details]"
      assert_select "input#competition_video_comment_description[name=?]", "competition_video_comment[description]"
      assert_select "input#competition_video_comment_approved[name=?]", "competition_video_comment[approved]"
      assert_select "input#competition_video_comment_is_private[name=?]", "competition_video_comment[is_private]"
      assert_select "input#competition_video_comment_has_video[name=?]", "competition_video_comment[has_video]"
      assert_select "input#competition_video_comment_video_id[name=?]", "competition_video_comment[video_id]"
    end
  end
end
