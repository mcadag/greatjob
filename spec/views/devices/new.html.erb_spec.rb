require 'spec_helper'

describe "devices/new" do
  before(:each) do
    assign(:device, stub_model(Device,
      :user_id => 1,
      :unique_indetifier => "MyString",
      :platform => "MyString",
      :active => false
    ).as_new_record)
  end

  it "renders new device form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", devices_path, "post" do
      assert_select "input#device_user_id[name=?]", "device[user_id]"
      assert_select "input#device_unique_indetifier[name=?]", "device[unique_indetifier]"
      assert_select "input#device_platform[name=?]", "device[platform]"
      assert_select "input#device_active[name=?]", "device[active]"
    end
  end
end
