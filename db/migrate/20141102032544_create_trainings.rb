class CreateTrainings < ActiveRecord::Migration
  def change
    create_table :trainings do |t|
      t.string :name
      t.date :start_date
      t.date :end_date
      t.string :company_name
      t.string :description
      t.string :details
      t.string :other_details

      t.timestamps
    end
  end
end
