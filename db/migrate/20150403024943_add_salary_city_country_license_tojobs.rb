class AddSalaryCityCountryLicenseTojobs < ActiveRecord::Migration
  def up
  	add_column :jobs, :city, :string
  	add_column :jobs, :country, :string
  	add_column :jobs, :salary, :string
  	add_column :jobs, :license, :string
  end

  def down
  end
end
