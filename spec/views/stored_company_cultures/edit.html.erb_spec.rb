require 'spec_helper'

describe "stored_company_cultures/edit" do
  before(:each) do
    @stored_company_culture = assign(:stored_company_culture, stub_model(StoredCompanyCulture,
      :name => "MyString"
    ))
  end

  it "renders the edit stored_company_culture form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stored_company_culture_path(@stored_company_culture), "post" do
      assert_select "input#stored_company_culture_name[name=?]", "stored_company_culture[name]"
    end
  end
end
