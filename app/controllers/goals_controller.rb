class GoalsController < ApplicationController
    before_filter :set_goal, only: [:show, :edit, :update, :destroy]
    # before_filter :authenticate_token!, :only => [:create, :update, :destroy]
    respond_to :json, :html
    before_filter :authenticate_token!

  def index
    # @user = User.find(params[:user_id]
    results = []
    @results = []
    @goals = Goal.where(user_id: params[:user_id]).page(params[:page])
    @goals.each do | goal |
      goal[:title] = goal.superlative.to_s() + " " + goal.adjective.to_s() + " " + goal.position.to_s()
      results << goal
    end
    @results = results
    respond_to do |format|
      format.html
      format.json { render json: results.as_json }
      end
  end

  def show
    #respond_with(@goal)
    @goals = Goal.where(user_id: params[:user_id],id:params[:id]).page(params[:page])
    render json: @goals.as_json
  end

  def new
    @goal = Goal.new
    respond_with(@goal)
  end

  def edit
  end

  def create
    goal = Goal.new
    goal.user_id = params[:user_id]
    goal.name = params[:goal][:name]
    goal.position = params[:goal][:position]
    goal.location = params[:goal][:location]
    goal.superlative = params[:goal][:superlative]
    goal.adjective = params[:goal][:adjective]
    goal.save
    goal_result = goal

    goal_result[:title] = goal.superlative.to_s() + " " + goal.adjective.to_s() + " " + goal.position.to_s()
    respond_to do |format|
      format.html { redirect_to '/profile', notice: 'Your Dream Job has been added.' }
      format.json { render :json => goal_result.as_json }
    end
  end

  def update
    @goal.update_attributes(params.require(:goal).permit!)
    @goal[:title] = @goal.superlative.to_s() + " " + @goal.adjective.to_s() + " " + @goal.position.to_s()
    render :json => @goal.as_json(:status => 201, :message => "Goal Updated"), status => 201
  end

  def destroy
    @goal.destroy
    render :json => {:message => "Deleted"}
  end

  def permission
    goal = Goal.where(id: params[:goal_id])
    goal.update_attributes(params[:goal])
    render :json => goal.as_json(:status => 201, :message => "Permission is set"), status => 201
  end

  private
    def set_goal
      @goal = Goal.find(params[:id])
    end

    def authenticate_token!
      auth_user = User.where(authentication_token: params[:authentication_token])
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

end
