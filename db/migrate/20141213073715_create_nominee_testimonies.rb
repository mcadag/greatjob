class CreateNomineeTestimonies < ActiveRecord::Migration
  def change
    create_table :nominee_testimonies do |t|
      t.integer :nominee_id
      t.integer :video_id
      t.string :description

      t.timestamps
    end
  end
end
