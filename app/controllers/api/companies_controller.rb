module Api
  class CompaniesController < ApplicationController
    include UsersHelper
    include CompaniesHelper
    before_filter :set_company, only: [:show, :edit, :update, :destroy]
    before_filter :authenticate_token!, :only => [:create, :update, :destroy, :index, :follow, :unfollow, :following, :followers, :managed]
    before_filter :set_followable!, :only => [:follow, :unfollow, :following, :followers]
    respond_to :json

      def index
        @companies = Company.all
        respond_with(@companies)
      end

      def show
        cultures = CompanyCulture.where(company_id: params[:id])
        jobs = CompanyDreamJob.where(company_id: params[:id])
        if @company.is_corporate? 
          branches = CompanyBranch.where(branch_id: params[:id])
          @company[:branches] = branches.as_json
        end
      
        @company[:cultures] = cultures.as_json
        @company[:jobs] = jobs.as_json
        render json: @company.as_json(:methods => [:payload_url])
      end

      def new
        @company = Company.new
        respond_with(@company)
      end

      def edit
      end

  def create
    @company = Company.new
    @company.name = params[:company][:name]
    @company.location = params[:company][:location]
    @company.payload = params[:company][:payload]
    @company.website  = params[:company][:website]
    @company.mobile_number = params[:company][:mobile_number]
    @company.email = params[:company][:email]
    @company.details = params[:company][:details]
    @company.user_id = @user.id
    @company.is_corporate = params[:company][:corporate]
    if @company.save
      if params[:company][:culture].present?
        params[:company][:culture].split(/, ?/).each do | culture |
          company_culture = CompanyCulture.new
          company_culture.name = culture
          company_culture.company_id = @company.id
          company_culture.user_id = @user.id
          company_culture.save
      end
    end
      if params[:company][:jobs].present?
        params[:company][:jobs].split(/, ?/).each do | job |
          dream_job = CompanyDreamJob.new
          dream_job.name = job
          dream_job.company_id = @company.id
          dream_job.user_id = @user.id
          dream_job.save
        end
      end
      if params[:company][:branches].present?
        params[:company][:branches].split(/, ?/).each do | branch_name |
          branch = CompanyBranch.new
          branch.name = branch_name
          branch.branch_id = @company.id
          branch.user_id = @user.id
          # corporate = Company.where(name: branch).first
          # branch.company_id = corporate.id
          branch.save
        end        
      end
    end
    render json: @company.as_json
  end

  def update
    @company.update_attributes(params.require(:company).permit!)
    respond_with(@company)
  end

  def destroy
    @company.destroy
    respond_with(@company)
  end

    def search
      results = []
      @companies = Company.includes(:companyCultures, :companyDreamJobs).where("name like ?", "%" + params[:q] + "%")
      @companies.each do | company |
        com = []
        @competitions = Competition.includes(:competitionQualifications).where(company_id: company.id)
        @competitions.each do |competition|
          competition_qualifications = CompetitionQualification.where(competition_id: competition.id)
          competition_qualifications.each do |q|
            com.push(q.name)
          end
        end
        @cultures = CompanyCulture.where(company_id: company.id)
        @cultures.each do |culture|
          com.push(culture.name)
        end
        @dream_jobs = CompanyDreamJob.where(company_id: company.id)
        @dream_jobs.each do |job|
          com.push(job.name)
        end
        culture_params = params[:cultures].split(",")
        job_params = params[:jobs].split(",")
        words = []
        culture_params.each do |word|
          words.push(word)
        end
        job_params.each do |word|
          words.push(word)
        end

        if words.any?
          results << company.as_json(:methods => [:payload_url]) if words.any? { |i| com.include?(i) }
        else
          results << company.as_json(:methods => [:payload_url])
        end
      end

      if @companies.count >= 1
        render json: results.as_json
      else
        render :json => {:message => "No Company Found with params:" + params[:q], :status => 404}
      end
    end

    def follow
      @user.follow!(@follow_company)
      render :json => {:message => "followed"}
    end

    def unfollow
      @user.unfollow!(@follow_company)
      render :json => {:message => "unfollowed"}
    end

    def following
    end

    def followers
      results = []
      followers = @follow_company.followers(User)
      followers.each do | user |
        results << UsersJSONResponseForWithNoAuth(user)
      end
      render :json => results
    end

    def managed
      results = []
      companies = Company.where(user_id: @user.id)
      companies.each do | company |
        results << CompanyJSONResponse(company)
      end
      render :json => results
    end

    def employees
      results = []
      employees = CompanyEmployees.all
      employees.each do |employee|
        results << employee
      end
      render :json => results
    end

    private
      def set_followable!
        follow_company = Company.select("id").where(id: params[:company_id])
        @follow_company = follow_company[0]
      end

      def set_company
        @company = Company.find(params[:id])
      end

      def authenticate_token!
        auth_user = User.where(authentication_token: params[:authentication_token])
        @user = auth_user[0]
        if auth_user.count >= 1
          true
        else
          render :json => {:message => "your authentication token is not valid"}
        end
      end
    end
  end
