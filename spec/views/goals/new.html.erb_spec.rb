require 'spec_helper'

describe "goals/new" do
  before(:each) do
    assign(:goal, stub_model(Goal,
      :name => "MyString",
      :user_id => 1,
      :position => "MyString"
    ).as_new_record)
  end

  it "renders new goal form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", goals_path, "post" do
      assert_select "input#goal_name[name=?]", "goal[name]"
      assert_select "input#goal_user_id[name=?]", "goal[user_id]"
      assert_select "input#goal_position[name=?]", "goal[position]"
    end
  end
end
