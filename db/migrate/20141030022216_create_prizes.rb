class CreatePrizes < ActiveRecord::Migration
  def change
    create_table :prizes do |t|
      t.string :name
      t.float :amount
      t.string :details
      t.integer :competition_id

      t.timestamps
    end
  end
end
