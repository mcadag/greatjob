class CreateCandidateProggresses < ActiveRecord::Migration
  def change
    create_table :candidate_proggresses do |t|
      t.boolean :apply, :default => 0
      t.boolean :initial_interview, :default => 0
      t.boolean :shotlisted, :default => 0
      t.boolean :final_interview, :default => 0
      t.boolean :hired, :default => 0
      t.boolean :rejected, :default => 0
      t.integer :cancidate_id

      t.timestamps
    end
  end
end
