module Api
  class CompetitionsController < ApplicationController
    include CompetitionsHelper
    include UsersHelper
    include NomineesHelper
  before_filter :set_competition, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_token!, :only => [:create, :update, :destroy, :suggested, :index]
  respond_to :json

  def index
  #I'll be refactoring this later on and will be moving
  #some codes to the model for reusability and cleanliness
    results = []
    Competition.find_each do |competition|
      results << competitionJSONResponse(competition)
    end
    render :json => results.as_json().sort_by { |hsh| hsh[:end_date] }

  end

  def show
    @competition = Competition.find(params[:id])
    render :json => competitionJSONResponse(@competition)
  end

  def new
    @competition = Competition.new
    respond_with(@competition)
  end

  def edit
  end

  def create
    @competition = Competition.new
    @competition.name = params[:competition][:name]
    @competition.location = params[:competition][:location]
    @competition.year = params[:competition][:year]
    @competition.superlative = params[:competition][:superlative]
    @competition.start_date = params[:competition][:start_date]
    @competition.end_date = params[:competition][:end_date]
    @competition.company_id = params[:competition][:company_id]
    @competition.user_id = params[:competition][:user_id]
    @competition.prize_id = params[:competition][:prize_id]
    @competition.poster = params[:competition][:poster]
    @competition.is_private = params[:competition][:is_private] if params[:competition][:is_private].present?
    @competition.save

    params[:qualification][:name].split(/, ?/).each do | qualification |
      @competition_qualification = CompetitionQualification.new
      @competition_qualification.name = qualification
      @competition_qualification.competition_id = @competition.id
      @competition_qualification.save
    end
     @competition[:title] = competitionTitle(@competition)

    if params[:competition][:company_id].present?
      recipient = Company.where(id: params[:competition][:company_id]).first
    else
      recipient = User.where(id: params[:competition][:user_id]).first
     end

    # create a new activity when a new competition is created.
    # notify all users for now that there is a new competition by the user or
    # the company.

    @competition.create_activity key: 'competition.create', 
                            owner: recipient, 
                       parameters: { resource_type: "competition",
                                       resource_id: @competition.id,
                                           message: "has created a new competition",
                                 competition_title: competitionTitle(@competition)
                                   }

    render :json => competitionJSONResponse(@competition)
  end

  def update
    @competition.update_attributes(params.require(:competition).permit!)
    render :json => @competition.as_json()
  end

  def destroy
    @competition.destroy
    render :json => @competition.as_json()
  end

  # refactor later
  def suggested
    results = []
    suggested = Competition.where(['end_date > ?', DateTime.now]).order("prize_id DESC")
    suggested.each do |competition|
      if QualificationsStringForUserID(@user.id).any?
        results << competitionJSONResponse(competition) if competition.is_private != true
      else
        results << competitionJSONResponse(competition) if competition.is_private != true
      end
    end
    render :json => results.as_json().sort_by { |hsh| hsh[:end_date] }
  end

  def top
    results = []
    trending_competitions = Competition.where(['end_date > ?', DateTime.now]).order("prize_id DESC")
    trending_competitions.each do |competition|
      nominees = Nominee.where(competition_id: competition.id)
      results << competitionJSONResponse(competition) if (competition.is_private != true) && (nominees.count >= 1)
    end

    render :json => results.as_json().sort_by { |hsh| hsh[:end_date] }
  end

  def search
    results = []
    search_results = Competition.where("name like ? AND superlative like ?", "%" + params[:name] + "%", "%" + params[:superlative] + "%",)
    search_results.each do |competition|
      puts competition
      words = []
      params_qualifications = params[:qualifications].split(",")
      params_qualifications.each do |word|
        words.push(word)
      end

      competitionTitle(competition)
      
      if words.any?
        results << competitionJSONResponse(competition) if words.any? { |i| competitionQualificationsString(competition).include?(i) }
      else
        results << competitionJSONResponse(competition)
      end
    end
      render json: results.as_json().sort_by { |hsh| hsh[:end_date] }
  end 

  def nominees
    results = []
    nominees = Nominee.where(competition_id: params[:id])
    nominees.each do |nominee|

      results << nomineeJSONResponse(nominee)
    end
    render :json => results.as_json(:include => { 
                                          :video => {:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url]}
                                          })
  end

  private
    def set_competition
      @competition = Competition.find(params[:id])
    end

    def authenticate_token!
      auth_user = User.where(authentication_token: params[:authentication_token])
      @user = auth_user[0]
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

    def star_count
      comments = Competition.where()
    end
end
end
