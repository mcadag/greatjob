require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe SmsgatewaysController do

  # This should return the minimal set of attributes required to create a valid
  # Smsgateway. As you add validations to Smsgateway, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) { {  } }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # SmsgatewaysController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET index" do
    it "assigns all smsgateways as @smsgateways" do
      smsgateway = Smsgateway.create! valid_attributes
      get :index, {}, valid_session
      assigns(:smsgateways).should eq([smsgateway])
    end
  end

  describe "GET show" do
    it "assigns the requested smsgateway as @smsgateway" do
      smsgateway = Smsgateway.create! valid_attributes
      get :show, {:id => smsgateway.to_param}, valid_session
      assigns(:smsgateway).should eq(smsgateway)
    end
  end

  describe "GET new" do
    it "assigns a new smsgateway as @smsgateway" do
      get :new, {}, valid_session
      assigns(:smsgateway).should be_a_new(Smsgateway)
    end
  end

  describe "GET edit" do
    it "assigns the requested smsgateway as @smsgateway" do
      smsgateway = Smsgateway.create! valid_attributes
      get :edit, {:id => smsgateway.to_param}, valid_session
      assigns(:smsgateway).should eq(smsgateway)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Smsgateway" do
        expect {
          post :create, {:smsgateway => valid_attributes}, valid_session
        }.to change(Smsgateway, :count).by(1)
      end

      it "assigns a newly created smsgateway as @smsgateway" do
        post :create, {:smsgateway => valid_attributes}, valid_session
        assigns(:smsgateway).should be_a(Smsgateway)
        assigns(:smsgateway).should be_persisted
      end

      it "redirects to the created smsgateway" do
        post :create, {:smsgateway => valid_attributes}, valid_session
        response.should redirect_to(Smsgateway.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved smsgateway as @smsgateway" do
        # Trigger the behavior that occurs when invalid params are submitted
        Smsgateway.any_instance.stub(:save).and_return(false)
        post :create, {:smsgateway => {  }}, valid_session
        assigns(:smsgateway).should be_a_new(Smsgateway)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Smsgateway.any_instance.stub(:save).and_return(false)
        post :create, {:smsgateway => {  }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested smsgateway" do
        smsgateway = Smsgateway.create! valid_attributes
        # Assuming there are no other smsgateways in the database, this
        # specifies that the Smsgateway created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Smsgateway.any_instance.should_receive(:update_attributes).with({ "these" => "params" })
        put :update, {:id => smsgateway.to_param, :smsgateway => { "these" => "params" }}, valid_session
      end

      it "assigns the requested smsgateway as @smsgateway" do
        smsgateway = Smsgateway.create! valid_attributes
        put :update, {:id => smsgateway.to_param, :smsgateway => valid_attributes}, valid_session
        assigns(:smsgateway).should eq(smsgateway)
      end

      it "redirects to the smsgateway" do
        smsgateway = Smsgateway.create! valid_attributes
        put :update, {:id => smsgateway.to_param, :smsgateway => valid_attributes}, valid_session
        response.should redirect_to(smsgateway)
      end
    end

    describe "with invalid params" do
      it "assigns the smsgateway as @smsgateway" do
        smsgateway = Smsgateway.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Smsgateway.any_instance.stub(:save).and_return(false)
        put :update, {:id => smsgateway.to_param, :smsgateway => {  }}, valid_session
        assigns(:smsgateway).should eq(smsgateway)
      end

      it "re-renders the 'edit' template" do
        smsgateway = Smsgateway.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Smsgateway.any_instance.stub(:save).and_return(false)
        put :update, {:id => smsgateway.to_param, :smsgateway => {  }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested smsgateway" do
      smsgateway = Smsgateway.create! valid_attributes
      expect {
        delete :destroy, {:id => smsgateway.to_param}, valid_session
      }.to change(Smsgateway, :count).by(-1)
    end

    it "redirects to the smsgateways list" do
      smsgateway = Smsgateway.create! valid_attributes
      delete :destroy, {:id => smsgateway.to_param}, valid_session
      response.should redirect_to(smsgateways_url)
    end
  end

end
