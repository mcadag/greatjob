module Api
  class VideosController < ApplicationController
  before_filter :set_video, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_token!, :only => [:create, :update, :destroy, :like, :unlike]
  respond_to :json

  # GET /Videos
  # GET /Videos.json
  def index
    @Videos = Video.where(user_id: params[:user_id]).where(:resource_type=>params[:resource_type]).where(:created_by => params[:created_by]).sort_by(&:created_at)
    render json: @Videos.as_json(
      :methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
  end
  
  def trending
    @Videos = Video.limit(9)
    render json: @Videos, :methods => [:payload_url, :payload_mobile_url, :payload_thumb_url]

  end
  def mine
    @videos = Video.where(user_id: params[:user_id]).all
    render json: @videos, :methods => [:payload_url, :payload_mobile_url, :payload_thumb_url]
  end

  # GET /Videos/1
  # GET /Videos/1.json
  def show
    @Video = Video.find(params[:id])
    render json: @Video, :methods => [:payload_url, :payload_mobile_url, :payload_thumb_url]
  end

  # GET /Videos/new
  # GET /Videos/new.json
  def new
    @Video = Video.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @Video }
    end
  end

  # GET /Videos/1/edit
  def edit
    @Video = Video.find(params[:id])
  end

  #POST
  def like
    @video = Video.find(params[:video_id])
    if @user.like!(@video)
      render json: @video.to_json()
    else
      render :json => {:message => "You already liked this video"}, :status => :bad_request
    end
  end

  #PUT
  def unlike
    @video = Video.find(params[:video_id])
    if @user.unlike!(@video)
      render json: @video.to_json(:methods => [:likers_count])
    else
      render :json => {:message => "You already unliked this video"}, :status => :bad_request
    end
  end


  # POST /Videos
  # POST /Videos.json
  def create
    video = Video.new
    video.payload = params[:video][:payload]
    video.resource_type = params[:video][:resource_type]
    video.resource_id = params[:video][:resource_id]
    video.title = params[:video][:title]
    video.name = params[:video][:name]
    video.description = params[:video][:description]
    video.stars = 0
    video.testimonies = 0
    video.comments_count = 0
    video.views_count = 0
    video.like_count = 0
    video.nominated_by = params[:video][:nominated_by] 
    video.user_id = params[:user_id]
    video.created_by = @user.id
      if video.save
        render json: video.to_json(:methods => [:payload_url,:payload_mobile_url,:payload_thumb_url], 
                                   :success => true, 
                                   :status => 201)
        else
          render json: video.errors, status: :unprocessable_entity
      end
  end

  # PUT /Videos/1
  # PUT /Videos/1.json
  def update
    @video.update_attributes(params.require(:video).permit!)
    render :json => @video.as_json(:status => 201, :message => "Updated"), status => 201
  end

  def updated_view_count
    @video = Video.find(params[:video_id])
    @video.views_count ||= 0
    @video.increment!(:views_count)
    render :json => @video.as_json(:status => 201, :message => "Updated"), status => 201
  end

  # DELETE /Videos/1
  # DELETE /Videos/1.json
  def destroy
    @video = Video.where(id: params[:id]).first
    if @video.user_id == @user.id
      @video.destroy(video_id)
      render :json => {:message => "Deleted"}
    else
      render :json => {:message => "You are not authorized to delete the resource"}, :status => :bad_request
    end
  end
  private
    def set_video
      @video = Video.find(params[:id])
    end

    def authenticate_token!
      auth_user = User.where(authentication_token: params[:authentication_token])
      if auth_user.count >= 1
        @user = auth_user[0]
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end
end
end
