require "spec_helper"

describe NotificationsMailer do
  describe "nominated" do
    let(:mail) { NotificationsMailer.nominated }

    it "renders the headers" do
      mail.subject.should eq("Nominated")
      mail.to.should eq(["to@example.org"])
      mail.from.should eq(["from@example.com"])
    end

    it "renders the body" do
      mail.body.encoded.should match("Hi")
    end
  end

end
