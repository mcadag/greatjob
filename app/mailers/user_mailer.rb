class UserMailer < ActionMailer::Base
  default from: "famous.dennis@famous.ph"
  
  def registration_confirmation(user)
    @user = user
    # attachments["rails.png"] = File.read("#{Rails.root}/public/images/rails.png")
    mail(:to => "#{user.name} <#{user.email}>", :subject =>  "Welcome to famous.ph, video-powered recruitment")
  end
end
