class StoredProfession < ActiveRecord::Base
  attr_accessible :name, :id
  belongs_to :job
end
