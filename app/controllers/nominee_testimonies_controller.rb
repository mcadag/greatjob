
  class NomineeTestimoniesController < ApplicationController
    before_filter :set_nominee_testimony, only: [:show, :edit, :update, :destroy]
    before_filter :authenticate_token!, :only => [:create, :update, :destroy]
    respond_to :json

    def index
      @nominee_testimonies = NomineeTestimony.all
      respond_with(@nominee_testimonies)
    end
  
    def show
      #respond_with(@nominee_testimony)

      render :json=> auth
    end
  
    def new
      @nominee_testimony = NomineeTestimony.new
      respond_with(@nominee_testimony)
    end
  
    def edit
    end
  
    def create
      @exist = NomineeTestimony.where(nominee_id: params[:nominee_id], user_id: @user.id)
      print params
      if @exist.count >= 1
          
          render :json => {result: "failed", message: "already voted."} 
      else
          @nominee_testimony = NomineeTestimony.new
          @nominee_testimony.user_id = @user.id 
          @nominee_testimony.nominee_id = params[:nominee_id]
          @nominee_testimony.video_id = params[:video_id]
          @nominee_testimony.description = params[:description]
          @success = @nominee_testimony.save
          #render :json=> nominee_testimony.as_json(:user_id=>nominee_testimony.user_id, :status=>201), :status=>201

          if @success
              @vote = Counter.where(nominee_id: params[:nominee_id]).first
              @vote ||= Counter.new(nominee_id: params[:nominee_id],votes: 0)
              @vote.votes +=1 
              @vote.save
          end
          render :json => {result: "success", message: "successfully voted."}
      end
    end
  
    def update
      nominee_testimony.update_attributes(params[:nominee_testimony])
      render :json => nominee_testimony.as_json(:status => 201, :message => "Updated"), status => 201
    end
  
    def destroy
      @nominee_testimony.destroy
      render :json => {:message => "Deleted"}
    end
  
    private
      def set_nominee_testimony
        @nominee_testimony = NomineeTestimony.find(params[:id])
      end
      
      def authenticate_token!
      #auth_user = User.where(authentication_token: params[:authentication_token])
      auth_user = User.select("id").where(authentication_token: params[:authentication_token])
     
      if auth_user.count >= 1
        @user = auth_user[0]
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

  end

