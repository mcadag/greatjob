class CandidatePreQualificationResource < ActiveRecord::Base
  attr_accessible :candidate_id, :promoted_resource_id, :promoted_resource_type, :resource_id, :resource_type
end
