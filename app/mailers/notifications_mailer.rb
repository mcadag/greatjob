class NotificationsMailer < ActionMailer::Base
  default from: "famous.dennis@famous.ph"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifications_mailer.nominated.subject
  #
  def nominated(user, nominated_by_user, competition, competition_title, video)
    @user = user
    @competition_title = competition_title
    @nominated_by_user = nominated_by_user
    @competition = competition
    @video = video
    mail(:to => "#{user.name} <#{user.email}>", :subject => "You are almost famous! Be the #{competition_title}.")
  end

  def refered(user_by, refered_by_user, job, company)
    @user_by = user_by
    @job = job
    @company = company
    if user_by.present?
      mail(:to => "#{company.name} <#{company.email}>", :subject => "Famous recommends #{user_by.name} to work as #{job.position}.", :body => "Dear #{company.name},<br/><br/>
   For your #{job.position} job openings, Famous highly recommends #{user_by.name}.
   Famous has 1.7 million more qualified skilled workers. Please email us at dennis@famous.ph for details.
    Visit www.famous.ph, the video-powered hiring platform, now!<br/><br/>
Regards,<br/>
The Famous Team",content_type: "text/html")
    else 
      mail(:to => "#{company.name} <#{company.email}>", :subject => "User Job Recomendation", :body => "Dear #{company.name},<br/><br/>For your #{job.position} job openings, #{refered_by_user.name} recommends someone to your job position.
      Famous has 1.7 million more qualified skilled workers. Please email us at dennis@famous.ph for details.Visit www.famous.ph, the video-powered hiring platform, now!<br/><br/>
      Regards,<br/>The Famous Team",content_type: "text/html")
    end

  end

  def testified(user, creator, testimony, video, video_thumb)
    @user = user
    @creator = creator
    @testimony = testimony
    @video = video
    @video_thumb = video_thumb
    @creator_profile = "http://famous.ph/users/" + creator.id.to_s
    @resource_url = "http://famous.ph/users/" + user.id.to_s + "/testimonies/" + @testimony.id.to_s
    @profile = "http://famous.ph/users/" + user.id.to_s
    mail(:to => "#{user.name} <#{@user.email}>", :subject => "A new testimonial has been posted to your profile.")
  end

  def new_skill_added(user, creator, interest, video, video_thumb)
    @user = user
    @creator = creator
    @interest = interest
    @video = video
    @video_thumb = video_thumb
    @creator_profile = "http://famous.ph/users/" + creator.id.to_s
    @resource_url = "http://famous.ph/users/" + user.id.to_s + "/interests/" + @interest.id.to_s
    @profile = "http://famous.ph/users/" + user.id.to_s
    mail(:to => "#{user.name} <#{@user.email}>", :subject => "A new skill has been posted to your profile.")
  end

  def new_award_added(user, creator, award, video, video_thumb)
    @user = user
    @creator = creator
    @award = award
    @video = video
    @video_thumb = video_thumb
    @creator_profile = "http://famous.ph/users/" + creator.id.to_s
    @resource_url = "http://famous.ph/users/" + user.id.to_s + "/awards/" + @award.id.to_s
    @profile = "http://famous.ph/users/" + user.id.to_s
    mail(:to => "#{user.name} <#{@user.email}>", :subject => "A new award has been posted to your profile")
  end

end
