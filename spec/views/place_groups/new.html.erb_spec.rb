require 'spec_helper'

describe "place_groups/new" do
  before(:each) do
    assign(:place_group, stub_model(PlaceGroup).as_new_record)
  end

  it "renders new place_group form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", place_groups_path, "post" do
    end
  end
end
