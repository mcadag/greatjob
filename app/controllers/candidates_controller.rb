  class CandidatesController < ApplicationController
    before_filter only: [:show, :edit, :update, :destroy]
    before_filter :require_login, :only => [:index, :edit, :update, :destroy]
    skip_before_filter :verify_authenticity_token
    respond_to :json, :html

   def hire
      @candidate = Candidate.where(user_id: params[:user_id], job_id: params[:job_id], company_id: params[:company_id]).limit(1)
      if @candidates.application_type == "hired"
          flash[:error] = "You already hired this guy"
          redirect_to candidates_path
      else
        if @candidates.update_attributes(:application_type => 'hired')
          flash[:error] = "Success"
          redirect_to candidates_path      
        end
      end
  end

  def update
    faye_publisher = Faye::Client.new('http://localhost:9292/faye') 
    @candidate = Candidate.find(params[:id])
    @user =  User.find(@candidate.user_id)
    @job = Job.find(@candidate.job_id)
    respond_to do |format|
      if @candidate.update_attribute(:application_type, params[:application_type])
        format.json { render json: @candidate.as_json() }
        @activity = @candidate.create_activity action: 'moved', parameters: {title: @user.name + ' moved to '+ params[:application_type] +  @job.position,
                                                                    resource_type: 'moved',
                                                                    resource_id:  @candidate.id,
                                                                    job_id: @job.id,
                                                              }, owner: @user
        jsonHash = {
           :trackable_type => "Candidate",
           :title =>  ' is ' +params[:application_type] + ' to ' + @job.position,
           :owner_name => @user.name, 
           :action => 'candidate.moved',
           :resource_id => @candidate.id
         }

       path = '/notifications/jobs/' + @job.id.to_s + '/candidates/' + @user.id.to_s

       faye_publisher.publish(path, JSON.generate(jsonHash))

      else
        format.html { render action: "edit" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

    def index
      if current_user.employer? 
        company = []

        #explicitly add user id to users company
        company << current_user.id
        
        #get user created companies
        #@companies = Company.where(user_id: current_user.company_ids);
        #@companies.each do | comp |
        #  company << comp
        #end

        #TODO add is admin feature
        #get companies where user belongs to
        @companybelongs =  CompanyEmployee.where(user_id: current_user.id).pluck(:company_id);
        @companybelongs.each do | companybelongs |
          company << companybelongs
        end

        #search jobs from user id
        jobresult = []
        @jobs = Job.where(company_id: company)
        @jobs.each do | job |
          jobresult << job
        end

        @jobresult = jobresult;

        #TODO add company id to candidates model
        #search candidates within the jobs id
        candidatesresult = []
        @candidates = Candidate.where(job_id: jobresult).pluck(:user_id);
        @candidates.each do | candidate |
          candidatesresult << candidate
        end
        
        if params[:q].present?
          @users = User.includes(:interests, :testimonies, :goals).where(
            'users.name LIKE ? OR interests.name LIKE ? OR testimonies.name LIKE ? OR testimonies.name LIKE ?',"%#{params[:q]}%",
            "%#{params[:q]}%", "%#{params[:q]}%", "%#{params[:q]}%") & User.where(id: candidatesresult)
        else
          @users =  User.where(id: candidatesresult).includes(:interests)
        end
        
        respond_to do |format|
          format.html
          format.json { render json: @users.as_json(:include => :interests) }
        end

      else
        # types are short_listed,interview,hired
        @applications = Candidate.where("application_type IS NULL OR application_type = :app_type AND user_id = :user_id",{app_type: ' ', user_id: current_user.id})
        @applications_listed = Candidate.where("application_type  = :app_type AND user_id = :user_id",{app_type: 'short_listed', user_id: current_user.id})
        @applications_interview = Candidate.where("application_type  = :app_type AND user_id = :user_id",{app_type: 'interview', user_id: current_user.id})
        @applications_hired = Candidate.where("application_type  = :app_type AND user_id = :user_id",{app_type: 'hired', user_id: current_user.id})
        
         respond_to do |format|
            format.html 
            format.json { render json: @applications.as_json(:include => [:job]) }
         end

      end
    end


  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def candidate_params
      params.require(:candidate).permit(:application_type)
    end

    def require_login
      unless current_user
        redirect_to new_user_registration_url, :alert => "Please login or signup"
      end
    end

  end
