class AddPayloadToCompanies < ActiveRecord::Migration
  def change
    add_attachment :companies, :payload
  end
end
