class AddVideoToCompetitioVideoComments < ActiveRecord::Migration
  def change
	add_attachment :competition_video_comments, :video
  end
end
