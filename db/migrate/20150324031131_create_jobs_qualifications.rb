class CreateJobsQualifications < ActiveRecord::Migration
  def change
    create_table :jobs_qualifications do |t|
      t.string :name
      t.integer :job_id

      t.timestamps
    end
  end
end
