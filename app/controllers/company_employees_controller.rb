class CompanyEmployeesController < ApplicationController
  include UsersHelper
  before_filter :set_company, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_token!, :only => [:create, :update, :destroy, :follow, :unfollow, :following, :followers, :managed]
  before_filter :set_followable!, :only => [:follow, :unfollow, :following, :followers]
  respond_to :json

  def index
    user = User.find(params[:user_id])
    @company_employee = user.company_employees.includes(:company,:user)
    respond_to do |format|
      format.json { render json: @company_employee.as_json(:include => [:company, :user]) }
      format.html
    end
  end
    
end