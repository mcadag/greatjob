class AddResourceTypeToVideos < ActiveRecord::Migration
  def change 
    add_column :videos, :resource_type, :string
    add_column :videos, :resource_id, :integer
  end
end
