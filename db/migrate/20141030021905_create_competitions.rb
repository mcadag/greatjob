class CreateCompetitions < ActiveRecord::Migration
  def change
    create_table :competitions do |t|
      t.string :name
      t.integer :user_id
      t.integer :company_id
      t.string :description
      t.integer :prize_id
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
