require 'spec_helper'

describe "competition_qualifications/new" do
  before(:each) do
    assign(:competition_qualification, stub_model(CompetitionQualification).as_new_record)
  end

  it "renders new competition_qualification form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", competition_qualifications_path, "post" do
    end
  end
end
