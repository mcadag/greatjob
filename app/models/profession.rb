class Profession < ActiveRecord::Base
	include PublicActivity::Model
  	tracked
	belongs_to :user
  	attr_accessible :job_title, :job_description, :job_details, :other, :other_details, :start_date, :end_date, :current_job, :location
  	scope :latest, order('end_date DESC')
end
