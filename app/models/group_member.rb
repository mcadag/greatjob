class GroupMember < ActiveRecord::Base

  scope :place_group_id , -> place_group_id  { where(:place_group_id => place_group_id) }

  belongs_to :place_group, :counter_cache => true
  belongs_to :user

  attr_accessible :place_group_id, :user_id, :confirmed
end
