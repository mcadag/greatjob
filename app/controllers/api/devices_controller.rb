module Api
  class DevicesController < ApplicationController
  # GET /devices
  # GET /devices.json
  before_filter :authenticate_token!, :only => [:create, :update, :destroy, :suggested, :index]
  def index
    @devices = Device.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @devices }
    end
  end

  # GET /devices/1
  # GET /devices/1.json
  def show
    @device = Device.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @device }
    end
  end

  # GET /devices/new
  # GET /devices/new.json
  def new
    @device = Device.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @device }
    end
  end

  # GET /devices/1/edit
  def edit
    @device = Device.find(params[:id])
  end

  # POST /devices
  # POST /devices.json
  def create
    @device = Device.new(device_params)
    @device.user_id = @user.id
    @device.active = 1
    current_devices = Device.where(:user_id => @user.id)
    current_registered_devices = Device.where(:unique_identifier => params[:unique_identifier])
    current_registered_devices.each do | device |
      device.destroy
    end
    current_devices.each do | device |
      device.destroy
    end
    @device.save
    render json: @device, status: :created
  end

  # PATCH/PUT /devices/1
  # PATCH/PUT /devices/1.json
  def update
    @device = Device.find(params[:id])

    respond_to do |format|
      if @device.update_attributes(device_params)
        format.html { redirect_to @device, notice: 'Device was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @device.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /devices/1
  # DELETE /devices/1.json
    def destroy
      @device = Device.find(params[:id])
      @device.destroy

      respond_to do |format|
        format.html { redirect_to devices_url }
        format.json { head :no_content }
      end
    end

    private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def device_params
      params.require(:device).permit(:active, :platform, :unique_identifier, :user_id, :unique_id)
    end

    def authenticate_token!
      auth_user = User.where(authentication_token: params[:authentication_token])
      @user = auth_user[0]
      if auth_user.count >= 1
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

  end
end