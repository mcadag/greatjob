module VideosHelper
	def videoMobileUrl(video_id)
		video = Video.where(id: video_id)
		return video.payload_mobile_url
	end
end
