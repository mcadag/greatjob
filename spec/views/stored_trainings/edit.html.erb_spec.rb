require 'spec_helper'

describe "stored_trainings/edit" do
  before(:each) do
    @stored_training = assign(:stored_training, stub_model(StoredTraining,
      :name => "MyString"
    ))
  end

  it "renders the edit stored_training form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stored_training_path(@stored_training), "post" do
      assert_select "input#stored_training_name[name=?]", "stored_training[name]"
    end
  end
end
