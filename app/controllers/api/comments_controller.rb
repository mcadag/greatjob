module Api
class CommentsController < ApplicationController
  include CompetitionVideoCommentsHelper
  before_filter :set_comment, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_token!, :only => [:create, :update, :destroy]
  respond_to :json

  def index
    @comments = Comment.where(video_id: params[:video_id]).order('created_at DESC')
    render :json => @comments.to_json(:include => { :user => { :only => [:name ,:id], :methods => [:avatar_url] } })
  end

  def show
    @comment = Comment.find(params[:id])
    render :json => @comment.as_json( :methods => [:video_mobile_url,:video_url,:video_thumb_url], 
                                       :success => true, 
                                       :status => 200)
  end

  def new
    @comment = Comment.new
    respond_with(@comment)
  end

  def edit
  end

  def create
    comment = Comment.new(params.require(:comment).permit!)
    comment.video_id = params[:video_id]
    comment.user_id = params[:user_id]
      if comment.save
        render :json => comment.to_json(:include => { :user => { :only => [:name, :id], :methods => [:avatar_url] } }) 
      else
        render json: comment.errors, status: :unprocessable_entity
      end
  end

  def update
    comment.update_attributes(params.require(:comment).permit!)
    render :json => comment.as_json(:status => 201, :message => "Updated"), status => 201
  end

  def destroy
    @comment.destroy
    render :json => {:message => "Deleted"}
  end

  private
    def set_comment
      @comment = Comment.find(params[:id])
    end

    def authenticate_token!
      auth_user = User.select("id").where(authentication_token: params[:authentication_token])
      if auth_user.count >= 1
        @user = auth_user[0]
        true
      else
        render :json => {:message => "your authentication token is not valid"}
      end
    end

  end
end