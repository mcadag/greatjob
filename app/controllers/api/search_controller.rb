module Api
	class SearchController < ApplicationController
	  respond_to :json

	  def index
	  	results = []
      results_set = []
          storedUsers_query = StoredUser.ransack(first_name_or_last_name_or_training_cont: params[:q])
          users_query = User.ransack(first_name_or_last_name_or_name_cont: params[:q])

          storedUsers = storedUsers_query.result(distinct: true).page(params[:page]).to_a.uniq
          users = users_query.result(distinct: true).page(params[:page]).to_a.uniq

          storedUsers.each do |storedUser|
            qualifiers = []
            storedUser[:title] = storedUser.first_name[0] + " " + storedUser.last_name
            storedUser[:goal] = storedUser.training
            storedUser[:description] = storedUser.last_name
            storedUser[:resource_type] = "User"
            storedUser[:stored_user] = true
            storedUser[:name] = storedUser.first_name[0] + " " + storedUser.last_name
            results << storedUser.as_json(:only => [:name, :id, :description, :resource_type, :title, :goal, :stored_user])
          end

          users.each do |user|
            if user.first_name.present? && user.last_name.present?
              user[:title] = user.first_name[0] + " " + user.last_name
              user[:description] = user.last_name
              user[:name] = user.first_name[0] + " " + user.last_name
            else
              user[:title] = user.name
              user[:description] = user.name
              user[:name] = user.name
            end
            user[:goal] = ""
            user[:resource_type] = "User"
            user[:stored_user] = false
            results << user.as_json(:only => [:name, :id, :description, :resource_type, :title, :goal, :stored_user])
          end

	  	render :json => results.as_json().sort_by { |hsh| hsh[:created_at] }.reverse
	  end

	  def competition
	  	results = []
      	params_qualifications = params[:q].split(" ")
      	params_qualifications.each do |word|
      	competitions = Competition.where("name like ? OR location like ? OR superlative like ? OR year like ? ", 
                                                      "%" + word + "%", 
                                                      "%" + word + "%",
                                                      "%" + word + "%",
                                                      "%" + word + "%")
      	competitions.each do |competition|
        	competition[:resource_type] = "Competition"
          competition[:title] = competition.superlative.to_s() + " " + competition.name.to_s() + " in  " + competition.location.to_s() + " for " + competition.year.to_s()
        	results << competition.as_json(:methods => [:poster_url], :only => [:name, :id, :description, :resource_type])
        end
        end
        render :json => results.as_json().sort_by { |hsh| hsh[:created_at] }.reverse
	  end

	  def people
      results = []
      results_set = []
      
      storedUsers_query = StoredUser.ransack(first_name_or_last_name_or_training_cont: params[:q])
      users_query = User.ransack(first_name_or_last_name_or_name_cont: params[:q])

      storedUsers = storedUsers_query.result.page(params[:page]).to_a.uniq
      users = users_query.result.page(params[:page]).to_a.uniq

          storedUsers.each do |storedUser|
            qualifiers = []
            storedUser[:title] = storedUser.first_name[0] + " " + storedUser.last_name
            storedUser[:goal] = storedUser.training
            storedUser[:description] = storedUser.last_name
            storedUser[:resource_type] = "User"
            storedUser[:name] = storedUser.first_name[0] + " " + storedUser.last_name
            results << storedUser.as_json(:only => [:name, :id, :description, :resource_type, :title, :goal])
          end

          users.each do |user|
            if user.first_name.present? && user.last_name.present?
              user[:title] = user.first_name[0] + " " + user.last_name
              user[:description] = user.last_name
              user[:name] = user.first_name[0] + " " + user.last_name
            else
              user[:title] = user.name
              user[:description] = user.name
              user[:name] = user.name
            end
            user[:goal] = ""
            user[:resource_type] = "User"
            results << user.as_json(:only => [:name, :id, :description, :resource_type, :title, :goal])
          end

          render :json => results.as_json().sort_by { |hsh| hsh[:created_at] }.reverse
	  end

    def latest
        results = []
        users = User.select("name, id, avatar_file_name, avatar_updated_at").where("avatar_content_type IS NOT NULL").order('created_at DESC').page(1).per(30)
          users.each do |user|
            goal = Goal.where(user_id: user.id).first
              if goal
                user[:title] = user.name + ", " + goal.position
                user[:description] = goal.position
              else
                user[:title] = user.name
                user[:description] = ""
              end
            user[:resource_type] = "User"
            results << user.as_json(:methods => [:avatar_url], :only => [:name, :id, :description, :resource_type, :title, :goal])
          end
        render :json => results.as_json().sort_by { |hsh| hsh[:created_at] }
    end

	  def company
	  	results = []
      	params_qualifications = params[:q].split(" ")
      	params_qualifications.each do |word|
	  	    companies.each do |company|
        		company[:resource_type] = "Company"
        		results << company.as_json(:methods => [:payload_url], :only => [:name, :id, :description, :resource_type])
        	end
        end
        render :json => results.as_json().sort_by { |hsh| hsh[:created_at] }.reverse
	  end

	end
end