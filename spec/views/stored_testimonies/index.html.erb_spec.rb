require 'spec_helper'

describe "stored_testimonies/index" do
  before(:each) do
    assign(:stored_testimonies, [
      stub_model(StoredTestimony,
        :name => "Name"
      ),
      stub_model(StoredTestimony,
        :name => "Name"
      )
    ])
  end

  it "renders a list of stored_testimonies" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
