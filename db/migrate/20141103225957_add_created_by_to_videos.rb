class AddCreatedByToVideos < ActiveRecord::Migration
  def change
  	add_column :videos, :created_by, :integer, :null => false
  end
end
