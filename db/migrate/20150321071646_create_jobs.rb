class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :position
      t.integer :company_id
      t.string :details
      t.integer :user_id
      t.string :description
      t.timestamps
    end

    add_attachment :jobs, :avatar
    add_attachment :jobs, :poster
    add_attachment :jobs, :banner

  end
end
