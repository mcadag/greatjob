class AddTrainingProviderToStoredUsers < ActiveRecord::Migration
  def change
    add_column :stored_users, :training_provider, :string
  end
end
