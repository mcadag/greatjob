require "spec_helper"

describe CompanyBranchesController do
  describe "routing" do

    it "routes to #index" do
      get("/company_branches").should route_to("company_branches#index")
    end

    it "routes to #new" do
      get("/company_branches/new").should route_to("company_branches#new")
    end

    it "routes to #show" do
      get("/company_branches/1").should route_to("company_branches#show", :id => "1")
    end

    it "routes to #edit" do
      get("/company_branches/1/edit").should route_to("company_branches#edit", :id => "1")
    end

    it "routes to #create" do
      post("/company_branches").should route_to("company_branches#create")
    end

    it "routes to #update" do
      put("/company_branches/1").should route_to("company_branches#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/company_branches/1").should route_to("company_branches#destroy", :id => "1")
    end

  end
end
