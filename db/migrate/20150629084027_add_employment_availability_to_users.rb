class AddEmploymentAvailabilityToUsers < ActiveRecord::Migration
  def change
    add_column :users, :employment_availability, :boolean, :default => true
  end
end
