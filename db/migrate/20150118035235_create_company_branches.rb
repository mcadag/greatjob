class CreateCompanyBranches < ActiveRecord::Migration
  def change
    create_table :company_branches do |t|
      t.string :name
      t.integer :comapny_id
      t.integer :branch_id
      t.integer :user_id

      t.timestamps
    end
  end
end
