require 'spec_helper'

describe "stored_company_cultures/index" do
  before(:each) do
    assign(:stored_company_cultures, [
      stub_model(StoredCompanyCulture,
        :name => "Name"
      ),
      stub_model(StoredCompanyCulture,
        :name => "Name"
      )
    ])
  end

  it "renders a list of stored_company_cultures" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
