require 'spec_helper'

describe "qualifications/new" do
  before(:each) do
    assign(:qualification, stub_model(Qualification).as_new_record)
  end

  it "renders new qualification form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", qualifications_path, "post" do
    end
  end
end
