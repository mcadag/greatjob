class AddConfirmedOnGroupMembers < ActiveRecord::Migration
  def change
    add_column :group_members, :confirmed, :boolean, default: false
  end
end
