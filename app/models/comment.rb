class Comment < ActiveRecord::Base
	include PublicActivity::Model
	tracked
	belongs_to :videos
	belongs_to :activities
	belongs_to :candidate
	belongs_to :user
	attr_accessible :candidate_id, :approved, :description, :details, :message, :nominee_id, :private, :user_id, :video_id, :video, :has_video

		has_attached_file :video,
		:storage => :s3,
		:s3_permissions => :private,
		:s3_credentials => "#{Rails.root}/config/aws.yml",
		:styles => {
    		:mobile => {:geometry => "400x300", 
    					:format => 'mp4'},
    		:thumb => { :geometry => "400x400#", 
    					:format => 'jpg', 
    					:time => 3 }
  		}, :processors => [:ffmpeg]
	do_not_validate_attachment_file_type :video

	def video_url
		video.url(:original)
	end

	def video_thumb_url
		video.url(:thumb)
	end

	def video_mobile_url
		video.url(:mobile)
	end

end
