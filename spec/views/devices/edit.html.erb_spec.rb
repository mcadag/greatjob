require 'spec_helper'

describe "devices/edit" do
  before(:each) do
    @device = assign(:device, stub_model(Device,
      :user_id => 1,
      :unique_indetifier => "MyString",
      :platform => "MyString",
      :active => false
    ))
  end

  it "renders the edit device form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", device_path(@device), "post" do
      assert_select "input#device_user_id[name=?]", "device[user_id]"
      assert_select "input#device_unique_indetifier[name=?]", "device[unique_indetifier]"
      assert_select "input#device_platform[name=?]", "device[platform]"
      assert_select "input#device_active[name=?]", "device[active]"
    end
  end
end
