require 'spec_helper'

describe "adjectives/new" do
  before(:each) do
    assign(:adjective, stub_model(Adjective).as_new_record)
  end

  it "renders new adjective form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", adjectives_path, "post" do
    end
  end
end
