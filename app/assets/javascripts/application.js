// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW. ui-bootstrap-tpls-0.12.0.min
// angular,angular-resource, angular-sanitize, videogular/videogular.min,videogular/controls.min,videogular/overlay-play.min, videogular/poster.min ,videogular/buffering.min

//= require jquery
//= require jquery_ujs
//= require dropzone.min
//= require jquery-fileupload/basic
//= require jquery-fileupload/vendor/tmpl
//= require video
//= require chosen-jquery
//= require bootstrap-select
//= require bootstrap-rating
//= require pnotify.custom.min
//= require jquery.bootstrap-duallistbox.min
//= require material
//= require jquery.photoset-grid.min
//= require underscore
//= require gmaps/google
//= require slick.min
