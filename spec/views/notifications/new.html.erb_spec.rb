require 'spec_helper'

describe "notifications/new" do
  before(:each) do
    assign(:notification, stub_model(Notification,
      :user => nil,
      :message => "MyText",
      :type => "",
      :topic_id => 1,
      :checked => false
    ).as_new_record)
  end

  it "renders new notification form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", notifications_path, "post" do
      assert_select "input#notification_user[name=?]", "notification[user]"
      assert_select "textarea#notification_message[name=?]", "notification[message]"
      assert_select "input#notification_type[name=?]", "notification[type]"
      assert_select "input#notification_topic_id[name=?]", "notification[topic_id]"
      assert_select "input#notification_checked[name=?]", "notification[checked]"
    end
  end
end
