
class CompanyBranchesController < ApplicationController
  # GET /company_branches
  # GET /company_branches.json
  def index
    @company_branches = CompanyBranch.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @company_branches }
    end
  end

  # GET /company_branches/1
  # GET /company_branches/1.json
  def show
    @company_branch = CompanyBranch.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @company_branch }
    end
  end

  # GET /company_branches/new
  # GET /company_branches/new.json
  def new
    @company_branch = CompanyBranch.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @company_branch }
    end
  end

  # GET /company_branches/1/edit
  def edit
    @company_branch = CompanyBranch.find(params[:id])
  end

  # POST /company_branches
  # POST /company_branches.json
  def create
    @company_branch = CompanyBranch.new(company_branch_params)

    respond_to do |format|
      if @company_branch.save
        format.html { redirect_to @company_branch, notice: 'Company branch was successfully created.' }
        format.json { render json: @company_branch, status: :created, location: @company_branch }
      else
        format.html { render action: "new" }
        format.json { render json: @company_branch.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company_branches/1
  # PATCH/PUT /company_branches/1.json
  def update
    @company_branch = CompanyBranch.find(params[:id])

    respond_to do |format|
      if @company_branch.update_attributes(company_branch_params)
        format.html { redirect_to @company_branch, notice: 'Company branch was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @company_branch.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company_branches/1
  # DELETE /company_branches/1.json
  def destroy
    @company_branch = CompanyBranch.find(params[:id])
    @company_branch.destroy

    respond_to do |format|
      format.html { redirect_to company_branches_url }
      format.json { head :no_content }
    end
  end

  private

    # Use this method to whitelist the permissible parameters. Example:
    # params.require(:person).permit(:name, :age)
    # Also, you can specialize this method with per-user checking of permissible attributes.
    def company_branch_params
      params.require(:company_branch).permit(:branch_id, :comapny_id, :name, :user_id)
    end
end
