class AddCreatedToUserIdtoTestimonies < ActiveRecord::Migration
  def change
  	add_column :testimonies, :create_to_email, :string
  	add_column :testimonies, :create_to_name, :string
  	add_column :testimonies, :create_to_id, :integer
  end
end
