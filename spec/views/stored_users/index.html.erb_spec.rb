require 'spec_helper'

describe "stored_users/index" do
  before(:each) do
    assign(:stored_users, [
      stub_model(StoredUser,
        :last_name => "Last Name",
        :first_name => "First Name",
        :middle_initial => "Middle Initial",
        :training => "Training",
        :mobile_number => 1,
        :brgy => "Brgy",
        :town => "Town",
        :province => "Province",
        :country => "Country",
        :gender => "Gender",
        :education => "Education",
        :education => "Education",
        :email => "Email",
        :age => 2
      ),
      stub_model(StoredUser,
        :last_name => "Last Name",
        :first_name => "First Name",
        :middle_initial => "Middle Initial",
        :training => "Training",
        :mobile_number => 1,
        :brgy => "Brgy",
        :town => "Town",
        :province => "Province",
        :country => "Country",
        :gender => "Gender",
        :education => "Education",
        :education => "Education",
        :email => "Email",
        :age => 2
      )
    ])
  end

  it "renders a list of stored_users" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Middle Initial".to_s, :count => 2
    assert_select "tr>td", :text => "Training".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Brgy".to_s, :count => 2
    assert_select "tr>td", :text => "Town".to_s, :count => 2
    assert_select "tr>td", :text => "Province".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "Gender".to_s, :count => 2
    assert_select "tr>td", :text => "Education".to_s, :count => 2
    assert_select "tr>td", :text => "Education".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
