require 'spec_helper'

describe "stored_company_cultures/show" do
  before(:each) do
    @stored_company_culture = assign(:stored_company_culture, stub_model(StoredCompanyCulture,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
