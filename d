[33mcommit 8fe9f718dcb506b7e23cea6a0fcbca6c0ae30657[m
Author: Angelo Paolo Obispo <angelopaolo.obispo@gmail.com>
Date:   Thu Nov 27 09:05:51 2014 +0800

    edited controllers_controller, added nominees action, edited nominees_controller, commented out line 41 in create action, edited routes.rb, added lines 17 and 18

[33mcommit c598b0433e31dc16782641c31b1bdd2a1f071250[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sat Nov 15 09:26:55 2014 +0800

    update

[33mcommit 04e61eb919176b122d13ea3a1be499c0fe2faf99[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 12 08:36:23 2014 +0800

    changed response 4040

[33mcommit 012d7237bca09b997941b0617f9e5c34c8367680[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 12 08:31:49 2014 +0800

    remove suspot log

[33mcommit f79dba934ade07ebed618e015501dbc826de708c[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 12 08:18:13 2014 +0800

    Added search

[33mcommit 6e1d11477a1d0ad7218c16645f45052d1c842280[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 12 08:13:58 2014 +0800

    fixed competition controller

[33mcommit 97d54ee7ff4c0799d04abd4d593902e3f2bb9059[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 12 08:13:45 2014 +0800

    refactor

[33mcommit c0ddacb6e467de7433908612429531e9ae5f462b[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sat Nov 8 10:28:41 2014 +0800

    Fixed create nomination added competition_id

[33mcommit 473bc43e73d0bfc3ee8aa946fdddff8283e92cf3[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sat Nov 8 10:13:36 2014 +0800

    Added nominees

[33mcommit 5cd399dd363a67efd9587cfd1350e522962bc7fd[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sat Nov 8 09:57:23 2014 +0800

    Added trending competition endpoint logic

[33mcommit 32e482c4ef8f92a908be8303e0cf6b9a959a166c[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Fri Nov 7 17:03:46 2014 +0800

    Fixed routes tranding and sugested

[33mcommit 59356311a0534f7888e97f490d8347800ce00de2[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Fri Nov 7 17:03:21 2014 +0800

    Added competiotion modules trending and suggested

[33mcommit a5ecce022d6df545a1fe6ac86806a6db87b0efd7[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Fri Nov 7 17:02:41 2014 +0800

    Add is_admin Flag

[33mcommit 6689854a012eef2a1bc9f8aec091f036c2ca30d7[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Fri Nov 7 16:51:40 2014 +0800

    Added competiotion trending and suggested

[33mcommit 6ea440176afdb843d3e470cde0eb3f0b00bbfcb2[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Fri Nov 7 15:07:15 2014 +0800

    Fixed interest

[33mcommit 5e996a46c36ad1eac0a6ef2ccccbba92b7e373ae[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 17:57:42 2014 +0800

    Return old responses

[33mcommit 75ac198b721dee8acbe3c943c191952a55a67695[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 16:54:29 2014 +0800

    Add User details

[33mcommit 4c5a8408a4c8d8c8e23defb080c9b22870256972[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 16:53:23 2014 +0800

    refactored

[33mcommit d635e37c96a150f3b32f85546c350e087949259c[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 12:22:35 2014 +0800

    Updated Video create

[33mcommit 952b1e15bf9c287aaa6550074e198e963e1294de[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 12:19:31 2014 +0800

    Updated videos show

[33mcommit 06126aee6ac0df32b034a48d16c28348a4963ec2[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 12:15:23 2014 +0800

    Added video show

[33mcommit b92dae3b736052490a641fda400d4a339bd81075[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 12:14:37 2014 +0800

    Added show on comments method

[33mcommit b467c23d722d1548a5bc533f81830b391f9ada70[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 12:13:24 2014 +0800

    Added show and authentication for awards

[33mcommit 9239da0575f8ccc06ba51370abfb4fd6b7115781[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 11:59:40 2014 +0800

    Added video url to index of comments

[33mcommit 89600d21bde57068c770b717f6f6b54d325f15c3[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 11:53:27 2014 +0800

    fixed has_video boolean

[33mcommit 323f202c59ae9b35bdf3bcd939e9e54dab74214c[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 11:49:14 2014 +0800

    Added Has Video boolean

[33mcommit 6783535d8a3a9d39b2973b9d679f4ceeaf1e60da[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 11:21:22 2014 +0800

    Updated destroy and update

[33mcommit c3b7c6fcff9a79af32b60108a0b4a797501fd23b[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 11:11:09 2014 +0800

    Fixed goal update

[33mcommit a8879c8edf9d7092bd13ba2cfced405ea881f4f8[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Thu Nov 6 11:07:26 2014 +0800

    Fixed goals delete

[33mcommit f16062321d4e0d5dc68828acf284a8522b77e6ae[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 19:55:48 2014 +0800

    renamed column

[33mcommit c8ee8334b956403dc7f4d1ca98ae1d9582700351[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 19:28:03 2014 +0800

    Fixed testimony create

[33mcommit 7600e6d89716edf665e58099079cc6bfd22fc64d[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 18:04:12 2014 +0800

    Added contact number in searchable params

[33mcommit b68d976af8a5041e496d93b968371c45aeaf4e97[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 17:49:48 2014 +0800

    Added me to routes

[33mcommit 03a5fcb79a71c8c28fdbc788d995aeed04caa1fc[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 17:49:38 2014 +0800

    Updated authentication check

[33mcommit efc8a03d3392f83beff96ab73bb9da131fbe113f[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 17:33:13 2014 +0800

    Added authentication for /me

[33mcommit a4c88eef943d7a6f5c9e8327ef0efb344e2935dd[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 17:16:41 2014 +0800

    Added /me to users

[33mcommit 3067e7e78d25b92e949cd7bc15841cb19bfc8d30[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 15:59:19 2014 +0800

    Added avatar link to search results

[33mcommit 1f66a96341130635cec2cbaafaebefadacd69e72[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 15:50:15 2014 +0800

    Updated solr search

[33mcommit 12dbef7be844a99a0844e91f7351e33a0b82b47d[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 15:49:48 2014 +0800

    Modified user search

[33mcommit 964b6726e1ba94b9165dd5d3c65e5b58f335fe48[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 15:38:25 2014 +0800

    refactored

[33mcommit a2bea3f0fbe99b6ea394fb72f851a4920714ccf5[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 15:37:55 2014 +0800

    Added bootstrap

[33mcommit cf6f8f5ea65689d1ed6f2a5833ba5e87e8643f9c[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 14:51:42 2014 +0800

    Add search to authentication before filter

[33mcommit 342dc883d1b611c251dca8c277e2c358f43a3c99[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 14:49:28 2014 +0800

    Add user email or name search

[33mcommit 81f01c29780f78232e1630fbb45ff9efdb599fc7[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 13:22:06 2014 +0800

    Setup awards

[33mcommit e205353fd8f1efcf2d2251aac90456e12f148a10[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 12:23:11 2014 +0800

    Added rails admin custom config

[33mcommit 66785354cb885c858c48431352c0ecfd4707a390[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 12:22:56 2014 +0800

    add fields to testymonies
    
    	create_to_name
    	create_to_id
    	create_to_email

[33mcommit 783433baee9a3e3046c5e863155f3a570a1684db[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 09:46:31 2014 +0800

    refactored

[33mcommit 7a9e6f49ebe938ce24df0b996457161994422b5b[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 09:46:15 2014 +0800

    Added attr_accs birthday to users

[33mcommit 9f6b0c5c85536418708f3562d64e7a30c4c65905[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Wed Nov 5 08:52:30 2014 +0800

    Added video comment urls

[33mcommit 837aca83453a90d05581751aa0e94201a5f0aff9[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Tue Nov 4 22:34:54 2014 +0800

    Added video Comments

[33mcommit 511592f887200b42b64e13a0903828279ba02321[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Tue Nov 4 21:50:12 2014 +0800

    Add user birthday

[33mcommit ed5cf67c261d5f24cd2b2d38305fb446d0fffc60[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Tue Nov 4 18:33:48 2014 +0800

    Added facebook_id to users

[33mcommit 3b935f258c159b670282feb3b30c86c47460484b[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Tue Nov 4 18:33:37 2014 +0800

    refactor

[33mcommit b809c1f3aa04d067b4dabc3ac79cbf13c1833b6b[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Tue Nov 4 09:10:06 2014 +0800

    Added params creator_id

[33mcommit 7dd7db3239a7b1d89a9dd771f71198935e98eb65[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Tue Nov 4 07:04:37 2014 +0800

    Added created by to video

[33mcommit da859a2c858b0db0c900adf0877d8f872810dba3[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Tue Nov 4 01:07:52 2014 +0800

    cleaned video index method

[33mcommit 23fcd248885e675ca76263bfd0e30cc3d026505b[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Tue Nov 4 00:59:40 2014 +0800

    Removed unneeded comments

[33mcommit 3d201683a0d9bf1366c66d20f8a2af9fb6e4c361[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Tue Nov 4 00:59:03 2014 +0800

    delete solr files

[33mcommit 8905f764879683d9e88b58f58c6e024f1db4709a[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Tue Nov 4 00:58:06 2014 +0800

    Added user goals

[33mcommit 55c5de7197e373590c3fe62d0ac84f4af65e17d5[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Tue Nov 4 00:44:33 2014 +0800

    Added more columns to video

[33mcommit e692df343b43219426b5677b7b137ed7ab2cccc8[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Tue Nov 4 00:43:46 2014 +0800

    Added gem jbuilder

[33mcommit b640bebe93e2a5edcba7c942470702005cd7e622[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Mon Nov 3 17:42:09 2014 +0800

    sold update

[33mcommit 6b535d91f9a730b182f233ee4f5dfdf65f3c2446[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Mon Nov 3 17:41:53 2014 +0800

    roll back on searchable

[33mcommit 1909b6191f2564f4aba85192132657152b2f717b[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Mon Nov 3 17:09:16 2014 +0800

    Added not null to payload params

[33mcommit c88525c99d875578806f4c12df3e08e2ffe87dc7[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Mon Nov 3 17:08:54 2014 +0800

    added searchable to user

[33mcommit 07ee6914e7f90c08ac5cf8f968254a2873701f29[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Mon Nov 3 14:21:31 2014 +0800

    changed file size of thumbnail and time of frame grab

[33mcommit d739dc2cf84f08f1b05108283e52479ece6715ff[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Mon Nov 3 13:58:53 2014 +0800

    placed views inside API folder

[33mcommit 0304e411de937e8095d5a564c582cb4e3544abe2[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Mon Nov 3 13:58:34 2014 +0800

    Added ffmpeg helper to video

[33mcommit 9b4d510bc85e35fcc47c057ed58b4b1e8978cdea[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 17:48:43 2014 +0800

    Added rails admin

[33mcommit 08c950d53a246e13cd558b138464c18b18af14ba[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 17:22:11 2014 +0800

    Find videos by user id and resource type

[33mcommit 9c00ac542efa79fb589fef6ae3e5b2690b0a1606[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 17:15:15 2014 +0800

    Fixed error 406 on uploads video and avatar

[33mcommit b3fcf31a068394435c91eed7394740078a4f5d69[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 14:10:36 2014 +0800

    Fixed upload video

[33mcommit 16963ff772c2dca1fa187c953da6490390de89f2[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 12:04:20 2014 +0800

    Added user_id to trainings

[33mcommit 80272de545993140eb4d7065ee7a92189ae1ca33[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 11:30:15 2014 +0800

    Added trainings

[33mcommit 6f9c8e9f7ed4be7a10df6f4ee1ff6db3025a35f7[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 11:14:23 2014 +0800

    Changed routes

[33mcommit a0836a113a677c93709e8ca42dc14ad9be798c4d[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 11:14:11 2014 +0800

    Added user search template

[33mcommit 2f9f777946cc29191afdd38ae261feb357cc0c64[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 11:13:58 2014 +0800

    added interests relationship

[33mcommit f5df79f6195d55f9b8f5a484ac65c74d13c9306e[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 11:13:47 2014 +0800

    updated videos controller

[33mcommit 2e8b7d7d1f2fcf9eb7e148ec9ff9d951fe9ba530[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 11:13:34 2014 +0800

    Added sunspot

[33mcommit eefd4d0531aa285fb461a33897679cf16378a8c5[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 09:35:27 2014 +0800

    Added create and show goals

[33mcommit a8b365e60135e949b505252a1c21f464e8603af3[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 09:26:52 2014 +0800

    Update interest, profession and add response to updating the avatar

[33mcommit dbeecfe4a0e6131400771c90a4bf5baa89fa1827[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 09:04:38 2014 +0800

    Fixed update on user and fixed profession index

[33mcommit aafe4c77ddf4c6ff59e0593da3a901705d8fbfe1[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 07:55:25 2014 +0800

    Added unicorn gem and rake

[33mcommit d440e225ed1368f4d4412391a2782a17c3c2c4ff[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 07:42:06 2014 +0800

    Added profession insert and show

[33mcommit acc52de03d5b66709fcd6338826a8d79a5f7bf41[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 07:41:33 2014 +0800

    use production env

[33mcommit 56601d2d8083dea302fc04afc54aefb7ea4f6a29[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 07:41:23 2014 +0800

    use production db

[33mcommit 739bf67085e09cb9e214764cc335b72ad32b693e[m
Author: Francis Martin Sevilla <francis@francissevilla.com>
Date:   Sun Nov 2 07:32:24 2014 +0800

    update rake
