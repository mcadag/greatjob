class CreateAwards < ActiveRecord::Migration
  def change
    create_table :awards do |t|
      t.string :name
      t.integer :user_id
      t.integer :video_id
      t.string :description

      t.timestamps
    end
  end
end
