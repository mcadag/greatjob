class AddCandidateToComment < ActiveRecord::Migration
  def change
    add_column :comments, :candidate_id, :integer
    add_index :comments, :candidate_id
  end
end
