class AddGroupMemberCountToPlaceGroups < ActiveRecord::Migration
  def change
    add_column :place_groups, :group_members_count, :integer
  end
end
