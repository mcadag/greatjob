class CreateGenderAndAges < ActiveRecord::Migration
  def change
    create_table :gender_and_ages do |t|
      t.string :name

      t.timestamps
    end
  end
end
