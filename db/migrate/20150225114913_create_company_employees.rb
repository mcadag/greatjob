class CreateCompanyEmployees < ActiveRecord::Migration
  def change
    create_table :company_employees do |t|
      t.integer :user_id
      t.integer :company_id
      t.boolean :current
      t.boolean :verified
      t.timestamps
    end
  end
end
