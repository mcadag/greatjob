class AddCommentCountToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :comment_count, :integer
  end
end
