require 'spec_helper'

describe "trainings/new" do
  before(:each) do
    assign(:training, stub_model(Training,
      :name => "MyString",
      :company_name => "MyString",
      :description => "MyString",
      :details => "MyString",
      :other_details => "MyString"
    ).as_new_record)
  end

  it "renders new training form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", trainings_path, "post" do
      assert_select "input#training_name[name=?]", "training[name]"
      assert_select "input#training_company_name[name=?]", "training[company_name]"
      assert_select "input#training_description[name=?]", "training[description]"
      assert_select "input#training_details[name=?]", "training[details]"
      assert_select "input#training_other_details[name=?]", "training[other_details]"
    end
  end
end
