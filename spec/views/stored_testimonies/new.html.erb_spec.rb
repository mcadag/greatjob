require 'spec_helper'

describe "stored_testimonies/new" do
  before(:each) do
    assign(:stored_testimony, stub_model(StoredTestimony,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new stored_testimony form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stored_testimonies_path, "post" do
      assert_select "input#stored_testimony_name[name=?]", "stored_testimony[name]"
    end
  end
end
