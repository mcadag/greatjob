class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.float :rate
      t.references :candidate

      t.timestamps
    end
    add_index :ratings, :candidate_id
  end
end
