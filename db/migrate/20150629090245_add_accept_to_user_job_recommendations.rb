class AddAcceptToUserJobRecommendations < ActiveRecord::Migration
  def change
    add_column :user_job_recommendations, :accepted, :boolean, :default => false
  end
end
