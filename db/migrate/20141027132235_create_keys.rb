class CreateKeys < ActiveRecord::Migration
  def change
    create_table :keys do |t|
    	t.string  :name
    	t.string  :value
    	t.string  :other
    	t.string  :flag_type
    	t.boolean :flag_value
      t.timestamps
    end
  end
end
