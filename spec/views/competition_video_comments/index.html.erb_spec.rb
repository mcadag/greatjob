require 'spec_helper'

describe "competition_video_comments/index" do
  before(:each) do
    assign(:competition_video_comments, [
      stub_model(CompetitionVideoComment,
        :payload => "",
        :name => "Name",
        :competition_id => 1,
        :user_id => 2,
        :message => "Message",
        :nominee_id => 3,
        :details => "Details",
        :description => "Description",
        :approved => false,
        :is_private => "",
        :has_video => false,
        :video_id => 4
      ),
      stub_model(CompetitionVideoComment,
        :payload => "",
        :name => "Name",
        :competition_id => 1,
        :user_id => 2,
        :message => "Message",
        :nominee_id => 3,
        :details => "Details",
        :description => "Description",
        :approved => false,
        :is_private => "",
        :has_video => false,
        :video_id => 4
      )
    ])
  end

  it "renders a list of competition_video_comments" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Message".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Details".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
  end
end
