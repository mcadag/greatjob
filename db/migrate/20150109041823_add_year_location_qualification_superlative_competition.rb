class AddYearLocationQualificationSuperlativeCompetition < ActiveRecord::Migration
  def up
  	add_column :competitions, :year, :integer
  	add_column :competitions, :location, :string
  	add_column :competitions, :superlative, :string
  end

end
