class WelcomesController < ApplicationController
  include CompetitionsHelper
  include UsersHelper
  include NomineesHelper
  include VideosHelper

  before_filter :set_welcome, only: [:show, :edit, :update, :destroy]
  before_filter :logged_in, only: [:index]
  respond_to :html, :xml, :json
  helper_method :get_video_for_competition

  def index
    respond_to do |format|
      format.html
    end
  end

  def show
    respond_with(@welcome)
  end

  def new
    @welcome = Welcome.new
    respond_with(@welcome)
  end

  def edit
  end

  def faqs
    @faq_recruit = Faq.where(category: 'recruiters').all
    @faq_seekers = Faq.where(category: 'jobseekers').all
    @faq_recommend = Faq.where(category: 'recommendation').all
    @faq_account = Faq.where(category: 'free_acc').all
    @faq_and_app = Faq.where(category: 'and_app').all
    @faq_find_job = Faq.where(category: 'find_job').all
    @faq_apply = Faq.where(category: 'application').all
    @faq_add_vid = Faq.where(category: 'add_video').all
    @faq_docu = Faq.where(category: 'document').all
   
    respond_to do |format|
      format.html
    end
  end


  def pricing
    respond_to do |format|
      format.html
    end
  end


  def create
    @welcome = Welcome.new(params[:welcome])
    @welcome.save
    respond_with(@welcome)
  end

  def update
    @welcome.update_attributes(params[:welcome])
    respond_with(@welcome)
  end

  def destroy
    @welcome.destroy
    respond_with(@welcome)
  end

    def get_video_for_competition(competition)
    nom = (Nominee.joins(:counter).where(competition_id: competition.id).order("votes DESC").limit(1))
    video = Video.where(id: nom.pluck(:video_id))
    competition[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
    if video.exists?
      pre_vid = JSON.parse(competition[:video])
      return pre_vid["payload_mobile_url"]
    else
      return
    end
    
  end


  private
    def set_welcome
      @welcome = Welcome.find(params[:id])
    end

    def logged_in
      if user_signed_in?
        redirect_to user_url(current_user.id)
      end
    end
end
