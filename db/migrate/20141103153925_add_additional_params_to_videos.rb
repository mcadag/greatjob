class AddAdditionalParamsToVideos < ActiveRecord::Migration
  def change
  	add_column :videos, :like_count, :integer
  	add_column :videos, :views_count, :integer
  	add_column :videos, :comments_count, :integer
  	add_column :videos, :description, :string
  	add_column :videos, :name, :string
  end
end
