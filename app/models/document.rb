class Document < ActiveRecord::Base
  attr_accessible :description, :name, :payload, :category
  has_attached_file :payload,
           :styles => { :medium => "x300", :thumb => "x100" },
           :storage => :s3,
           :s3_permissions => :private,
           :s3_credentials => "#{Rails.root}/config/aws.yml"
  do_not_validate_attachment_file_type :payload
  
  def payload_url
        payload.url(:medium)
  end

end
