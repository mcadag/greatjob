require 'spec_helper'

describe "stored_professions/show" do
  before(:each) do
    @stored_profession = assign(:stored_profession, stub_model(StoredProfession,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
