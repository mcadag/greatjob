class SearchController < ApplicationController
	respond_to :html
	
  def candidates
    #improve http://stackoverflow.com/questions/12400860/activerecord-includes-specify-included-columns
    @users = User.includes(:interests,:professions, :testimonies, :goals ,:awards).where(
            'users.name LIKE ? OR interests.name LIKE ? OR professions.job_title LIKE ? OR testimonies.name LIKE ? OR goals.position LIKE ? OR awards.name LIKE ?',"%#{params[:q]}%",
            "%#{params[:q]}%", "%#{params[:q]}%", "%#{params[:q]}%","%#{params[:q]}%","%#{params[:q]}%" ).page(params[:page])
    respond_to do |format|
          format.html
          format.json { render json: @users.as_json(:only => [:name, :last_name, :employment_availability],
            :include => [:interests, :goals => {:only => [:position] }]) }
    end
  end

	def index
	  	@results = []
	  	@results_set = []
	  	params_qualifications = []
      	params_qualifications = params[:q].split(" ") if params[:q]

      	params_qualifications.each do |word|
        	competitions = Competition.where("name like ? OR location like ? OR superlative like ? OR year like ? ", 
                                                                "%" + word + "%", 
                                                                "%" + word + "%",
                                                                "%" + word + "%",
                                                                "%" + word + "%")
        	users = User.where("name like ?", "%" + word + "%")
        	companies = Company.where("name like ?", "%" + word + "%")
        	testimonies = Testimony.where("name like ?", "%" + word + "%")
        	awards = Award.where("name like ?", "%" + word + "%")
        	interests = Interest.where("name like ?", "%" + word + "%")
        	professions = Profession.where("job_title like ?", "%" + word + "%")
        	goals = Goal.where("position like ?", "%" + word + "%")

        	competitions.each do |competition|
        		competition[:resource_type] = "Competition"
        		company = Company.where(id: competition.company_id)
        		user = User.where(id: competition.user_id).limit(1)
        		if !competition.company_id.nil?
        			competition[:description] = "sponsored by: " + company[0].name
    	        else
                  competition[:description] = "sponsored by: " + user.name
	            end

        		competition[:title] = competition.superlative.to_s() + " " + competition.name.to_s() + " in  " + competition.location.to_s() + " for " + competition.year.to_s()
        		@results << competition.as_json(:methods => [:poster_url], :only => [:name, :id, :description, :resource_type, :title])
        	end

        	users.each do |user|
        		qualifiers = []
        		goal = Goal.where(user_id: user.id).first
          		if goal
            		user[:title] = user.name + ", " + goal.position
            		user[:goal] = goal.position
            		user[:description] = goal.position
          		else
            		user[:title] = user.name
            		user[:goal] = nil
            		user[:description] = nil
          		end
        		user[:resource_type] = "User"
        		@results << user.as_json(:methods => [:avatar_url], :only => [:name, :id, :description, :resource_type, :title, :goal])
        	end

        	testimonies.each do |testimony|
        		qualifiers = []
        		user = User.where(id: testimony.user_id).first
        		if user.present?
        		goal = Goal.where(user_id: user.id).first
        		professions = Profession.where("job_title like ? AND user_id = ?", "%" + word + "%", user.id)
          		if goal
            		user[:title] = user.name + ", " + goal.position
            		user[:goal] = goal.position
          		else
            		user[:title] = user.name
            		user[:goal] = nil
          		end
          		user[:description] = testimony.name
        		user[:resource_type] = "User"
        		@results << user.as_json(:methods => [:avatar_url], :only => [:name, :id, :description, :resource_type, :title, :goal])
        	end
        	end

        	awards.each do |award|
        		qualifiers = []
        		user = User.where(id: award.user_id).first
        		if user.present?
        		goal = Goal.where(user_id: user.id).first
        		professions = Profession.where("job_title like ? AND user_id = ?", "%" + word + "%", user.id)
          		if goal
            		user[:title] = user.name + ", " + goal.position
            		user[:goal] = goal.position
          		else
            		user[:title] = user.name
            		user[:goal] = nil
          		end
          		user[:description] = award.name
        		user[:resource_type] = "User"
        		@results << user.as_json(:methods => [:avatar_url], :only => [:name, :id, :description, :resource_type, :title, :goal])
        	end
        	end

        	interests.each do |interest|
        		qualifiers = []
        		user = User.where(id: interest.user_id).first
        		if user.present?
        		goal = Goal.where(user_id: user.id).first
        		professions = Profession.where("job_title like ? AND user_id = ?", "%" + word + "%", user.id)
          		if goal
            		user[:title] = user.name + ", " + goal.position
            		user[:goal] = goal.position
          		else
            		user[:title] = user.name
            		user[:goal] = nil
          		end
          		user[:description] = interest.name
        		user[:resource_type] = "User"
        		@results << user.as_json(:methods => [:avatar_url], :only => [:name, :id, :description, :resource_type, :title, :goal])
        	end
        	end

        	professions.each do |profession|
        		qualifiers = []
        		user = User.where(id: profession.user_id).first
        		if user.present?
        		goal = Goal.where(user_id: user.id).first
        		professions = Profession.where("job_title like ? AND user_id = ?", "%" + word + "%", user.id)
          		if goal
            		user[:title] = user.name + ", " + goal.position
            		user[:goal] = goal.position
          		else
            		user[:title] = user.name
            		user[:goal] = nil
          		end
          		user[:description] = profession.job_title
        		user[:resource_type] = "User"
        		@results << user.as_json(:methods => [:avatar_url], :only => [:name, :id, :description, :resource_type, :title, :goal])
        	end
        	end

        	goals.each do |goal|
        		qualifiers = []
        		user = User.where(id: goal.user_id).first 
        		if user.present?
        			user_goal = Goal.where(user_id: user.id).first
          			if goal
            			user[:title] = user.name + ", " + user_goal.position
            			user[:goal] = user_goal.position
          			else
            			user[:title] = user.name
            			user[:goal] = nil
          			end
          			user[:description] = goal.position
        			user[:resource_type] = "User"
        			@results << user.as_json(:methods => [:avatar_url], :only => [:name, :id, :description, :resource_type, :title, :goal])
        		end
        	end

        	companies.each do |company|
        		qualifiers = []
        		company[:resource_type] = "Company"
        		company[:title] = company.name
        		qualifications = CompanyDreamJob.where(company_id: company.id)
        		qualifications.each do | qualification |
        			qualifiers << qualification.name
        		end
        		company[:description] = qualifiers
        		@results << company.as_json(:methods => [:payload_url], :only => [:name, :id, :description, :resource_type])
        	end
      	end
      	
		 @results_set = @results.uniq { |result| result['title'] }

	  	@results_set.sort_by { |hsh| hsh[:created_at] }.reverse

	  	respond_to do |format|
      		format.html
    	end
	end
end
