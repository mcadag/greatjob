module CompetitionsHelper
      def competitionJSONResponse(competition)
	     nom = (Nominee.joins(:counter).where(competition_id: competition.id).order("votes DESC").limit(1))

            video = Video.where(id: nom.pluck(:video_id))
            user = User.where(id: competition.user_id).limit(1)
            nominated_by_name = User.select('name').where(id: video.pluck(:created_by)).limit(1)
            nominated_by_id = User.select('id').where(id: video.pluck(:created_by)).limit(1)
            leading_nominee_by_name = User.select('name').where(id: video.pluck(:user_id)).limit(1)
            contest = Competition.where(id: nom.pluck(:competition_id)).limit(1)
            company = Company.where(id: competition.company_id)
            comments = CompetitionVideoComment.where(video_id: nom.pluck(:video_id), competition_id: competition.id)
            competition[:nominee_id] = nom.pluck(:nominee_id)[0]
            competition[:nominee_name] = leading_nominee_by_name.pluck(:name)[0]
            competition[:video] = video.as_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
            competition[:stars_count] = nom.pluck(:votes)[0]
            competition[:nominated_by_name] = nominated_by_name.pluck(:name)[0]
            competition[:nominated_by_id] = nominated_by_id.pluck(:id)[0]
            competition[:views_count] = 0
            competition[:comments_count] = comments.count
            competition[:poster_url] = :poster_url
            if !competition.company_id.nil?
                  competition[:sponsor_type] = "Company"
                  competition[:sponsor] = company.as_json(:methods => [:payload_url], :only => [:id, :name])[0]
            else
                  competition[:sponsor_type] = "User"
                  competition[:sponsor] = user.as_json(:methods => [:avatar_url], :only => [:id, :name])[0]
            end
            
            competition[:title] = competition.superlative.to_s() + " " + 
                                    competition.name.to_s() + " in " + 
                                    competition.location.to_s() + " for " + 
                                    competition.year.to_s()
            return competition
	end

      def competitionQualificationsString(competition)
            sq = []
            CompetitionQualification.where(competition_id: competition.id).each do | q |
                  sq << q.name
            end
            return sq
      end

      def competitionTitle(competition)
            return competition.superlative.to_s() + " " + competition.name.to_s() + " in  " + competition.location.to_s() + " for " + competition.year.to_s()
      end
end
