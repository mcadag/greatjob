class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?
  respond_to :json

  rescue_from CanCan::AccessDenied do | exception |
    redirect_to root_url, alert: exception.message
  end

  after_filter :store_location

  def store_location
    return unless request.get? 
    if (request.path != "/users/sign_in" &&
      request.path != "/users/sign_up" &&
      request.path != "/users/password/new" &&
      request.path != "/users/password/edit" &&
      request.path != "/users/confirmation" &&
      request.path != "/users/sign_out" &&
      !request.xhr?) # don't store ajax calls
      session[:previous_url] = request.fullpath 
    end
  end

  def after_sign_in_path_for(resource)
    # user_url(current_user.id)
    place_groups_url
  end

  def after_sign_up_path_for(resource)
    sign_in_url = new_user_session_url
    if request.referer == sign_in_url
      user_url(current_user.id)
    else
      session[:previous_url] || user_url(current_user.id)
    end
  end

  def error(status, code, message)
    render :json => {:response_type => "ERROR", :response_code => code, :message => message}.to_json, :status => status
  end

    protected

    def configure_permitted_parameters
        devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, 
                                                                :email, 
                                                                :password, 
                                                                :avatar, 
                                                                :contact_number,
                                                                :employer,
                                                                :goals) }
        devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, 
                                                                       :email, 
                                                                       :password, 
                                                                       :current_password, 
                                                                       :avatar, 
                                                                       :contact_number,
                                                                       :goals,
                                                                       :password_confirmation) }
        devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, 
                                                                :username, 
                                                                :email, 
                                                                :password, 
                                                                :remember_me, 
                                                                :contact_number,
                                                                :goals) }
    end
end
