require 'spec_helper'

describe "smsgateways/edit" do
  before(:each) do
    @smsgateway = assign(:smsgateway, stub_model(Smsgateway))
  end

  it "renders the edit smsgateway form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", smsgateway_path(@smsgateway), "post" do
    end
  end
end
