require 'spec_helper'

describe "stored_testimonies/show" do
  before(:each) do
    @stored_testimony = assign(:stored_testimony, stub_model(StoredTestimony,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
