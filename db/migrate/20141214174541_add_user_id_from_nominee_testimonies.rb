class AddUserIdFromNomineeTestimonies < ActiveRecord::Migration
  def change
    add_column :nominee_testimonies, :user_id, :integer
  end
end
