class CompaniesController < ApplicationController
  include UsersHelper
   include CompaniesHelper
  skip_before_filter :verify_authenticity_token
  before_filter :set_company, only: [:show, :edit, :update, :destroy]
  before_filter :set_followable!, :only => [:follow, :unfollow, :following, :followers]
  respond_to :json, :html

  def index
  #  @companies = current_user.companies
    @company_ids = CompanyEmployee.where(user_id: current_user.id).pluck(:company_id)
    @company_belong = Company.where(id: @company_ids)

    respond_to do |format| 
      format.json { render json: @company_belong.as_json() }
      format.html
    end
  end
  
  def show
    @company = Company.find(params[:id])
    @jober = @company.jobs
    # cultures = CompanyCulture.where(company_id: params[:id])
    # jobs = CompanyDreamJob.where(company_id: params[:id])

    # if @company.is_corporate? 
    #   branches = CompanyBranch.where(branch_id: params[:id])
    #   @company[:branches] = branches.as_json
    # end
      
    # @company[:cultures] = cultures.as_json
    # @company[:jobs] = jobs.as_json
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @company.as_json(
                           include: { jobs: {
                                          only: [:id, :position]} })
    }
    end
  end

  def new
    @company = Company.new
    # respond_with(@company)
    @all_stored_cultures = StoredCompanyCulture.all
    @company_culture = @company.stored_company_cultures.build

    respond_to do |format|
      format.html
    end
  end

  def edit
    @company = Company.find(params[:id])
  end

  def create
    @company = Company.new
    @company.name = params[:company][:name]
    @company.location = params[:company][:location]
    @company.payload = params[:company][:payload]
    @company.website  = params[:company][:website]
    @company.mobile_number = params[:company][:mobile_number]
    @company.email = params[:company][:email]
    @company.details = params[:company][:details]
    @company.user_id = current_user.id
    @company.is_corporate = params[:company][:corporate]

    @all_stored_cultures = StoredCompanyCulture.all
    @company_culture = @company.stored_company_cultures.build

    
    if @company.save

      company_emp = CompanyEmployee.new
      company_emp.company_id = @company.id
      company_emp.user_id = @company.user_id
      company_emp.save

      if params[:company][:culture].present?
        params[:company][:culture].split(/, ?/).each do | culture |
          company_culture = CompanyCulture.new
          company_culture.name = culture
          company_culture.company_id = @company.id
          company_culture.user_id = current_user.id
          company_culture.save
      end
    end
      if params[:company][:jobs].present?
        params[:company][:jobs].split(/, ?/).each do | job |
          dream_job = CompanyDreamJob.new
          dream_job.name = job
          dream_job.company_id = @company.id
          dream_job.user_id = current_user.id
          dream_job.save
        end
      end
      if params[:company][:branches].present?
        params[:company][:branches].split(/, ?/).each do | branch_name |
          branch = CompanyBranch.new
          branch.name = branch_name
          branch.branch_id = @company.id
          branch.user_id = current_user.id
          # corporate = Company.where(name: branch).first
          # branch.company_id = corporate.id
          branch.save
        end        
      end
    end
    # respond_to do |format| 
    #   format.json { render json: @companies.as_json() }
    #    format.html { redirect_to user_companies_path(current_user) }
    # end

    respond_to do |format|
      if @company.save
        flash[:success] = 'Company was successfully created.'
        format.html { render action: 'new'}
        format.json { render json: @company }
      else
        format.html { render action: 'new' }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end

  end

  def update
    @company = Company.find(params[:id])
    respond_to do |format|
      if @company.update_attributes(params.require(:company).permit!)
        flash[:success] = 'Company was successfully updated.'
        format.html { render action: 'new' }
        format.json { render json: @company }
      else
        format.html { render action: "edit" }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @company.destroy
    respond_with(@company)
  end

    def search
      results = []
      @companies = Company.includes(:companyCultures, :companyDreamJobs).where("name like ?", "%" + params[:q] + "%")
      @companies.each do | company |
        com = []
        @competitions = Competition.includes(:competitionQualifications).where(company_id: company.id)
        @competitions.each do |competition|
          competition_qualifications = CompetitionQualification.where(competition_id: competition.id)
          competition_qualifications.each do |q|
            com.push(q.name)
          end
        end
        @cultures = CompanyCulture.where(company_id: company.id)
        @cultures.each do |culture|
          com.push(culture.name)
        end
        @dream_jobs = CompanyDreamJob.where(company_id: company.id)
        @dream_jobs.each do |job|
          com.push(job.name)
        end
        culture_params = params[:cultures].split(",")
        job_params = params[:jobs].split(",")
        words = []
        culture_params.each do |word|
          words.push(word)
        end
        job_params.each do |word|
          words.push(word)
        end

        if words.any?
          results << company.as_json(:methods => [:payload_url]) if words.any? { |i| com.include?(i) }
        else
          results << company.as_json(:methods => [:payload_url])
        end
      end

      if @companies.count >= 1
        render json: results.as_json
      else
        render :json => {:message => "No Company Found with params:" + params[:q], :status => 404}
      end
    end

    def follow
      @user.follow!(@follow_company)
      render :json => {:message => "followed"}
    end

    def unfollow
      @user.unfollow!(@follow_company)
      render :json => {:message => "unfollowed"}
    end

    def following
    end

    def followers
      results = []
      followers = @follow_company.followers(User)
      followers.each do | user |
        results << UsersJSONResponseForWithNoAuth(user)
      end
      render :json => results
    end

    def managed
      results = []
      companies = Company.where(user_id: @user.id)
      companies.each do | company |
        results << CompanyJSONResponse(company)
      end
      render :json => results
    end

    def invite
      results = []
      CompaniesMailer.invite(current_user, company_accept_url(user_id: current_user.id.to_s), company_accept_url(user_id: current_user.id.to_s),"some company", current_user).deliver
      render :json => results
    end

    def accept_invite
      results = []
      current_admin = CompanyEmployee.where(:user_id => params[:user_id], :company_id => params[:company_id])
      employee = CompanyEmployee.new
      employee.user_id = params[:user_id]
      employee.company_id = params[:company_id]
      if employee.save
        render :json => results
      end
    end

    def decline_invite

    end
    private
      def set_followable!
        follow_company = Company.select("id").where(id: params[:company_id])
        @follow_company = follow_company[0]
      end

      def set_company
        @company = Company.find(params[:id])
      end

      def authenticate_token!
        auth_user = User.where(authentication_token: params[:authentication_token])
        @user = auth_user[0]
        if auth_user.count >= 1
          true
        else
          render :json => {:message => "your authentication token is not valid"}
        end
      end
    end