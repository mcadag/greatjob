require 'spec_helper'

describe "group_members/new" do
  before(:each) do
    assign(:group_member, stub_model(GroupMember,
      :place_group => nil,
      :user => nil
    ).as_new_record)
  end

  it "renders new group_member form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", group_members_path, "post" do
      assert_select "input#group_member_place_group[name=?]", "group_member[place_group]"
      assert_select "input#group_member_user[name=?]", "group_member[user]"
    end
  end
end
