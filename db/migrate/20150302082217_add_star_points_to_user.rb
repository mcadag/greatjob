class AddStarPointsToUser < ActiveRecord::Migration
  def change
    add_column :users, :star_points, :integer, :default => 0
  end
end
