class CreateReferrals < ActiveRecord::Migration
  def change
    create_table :referrals do |t|
      t.references :company
      t.references :job
      t.integer :created_by
      t.integer :created_to

      t.timestamps
    end
    add_index :referrals, :company_id
    add_index :referrals, :job_id
  end
end
