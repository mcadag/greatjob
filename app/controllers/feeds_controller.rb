class FeedsController < ApplicationController
	helper_method :get_video_for_result
	before_filter :require_login, only: [:create, :edit, :update, :destroy, :index]
	respond_to :html
	def index
			@results = []
	        testimonies = Testimony.where("video_id IS NOT NULL").order('created_at DESC').page(1).per(40)
	        testimonies.each do |testimony|
	          video = Video.where(id: testimony.video_id).first
	          creator = User.where(id: testimony.created_by_id).first
	          created_to_user = User.where(id: testimony.user_id).first
	          videoComments = CompetitionVideoComment.where(video_id: video.id)
	          comments = Comment.where(video_id: video.id)

	          testimony[:video] = video.as_json(:methods => [:payload_mobile_url, :payload_thumb_url, :payload_url, :comments_count], :except => [:comments_count])
	          testimony[:resource_type] = "testimony"
	          testimony[:created_to_user] = created_to_user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
	          testimony[:creator] = creator.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
	          testimony[:views_count] = video.views_count
              testimony[:comments_count] = comments.count + videoComments.count
              testimony[:facebook_share_url] = user_testimony_url(testimony.user_id, testimony.id)
	          @results << testimony if !testimony[:video].nil?
	        end
	
	        awards = Award.where("video_id IS NOT NULL").order('created_at DESC').page(1).per(5)
	        awards.each do |award|
	          video = Video.where(id: award.video_id).first
	          creator = User.where(id: award.created_by_id).first
	          created_to_user = User.where(id: award.user_id).first
	          videoComments = CompetitionVideoComment.where(video_id: video.id)
	          comments = Comment.where(video_id: video.id)

	          award[:video] = video.as_json(:methods => [:payload_mobile_url, :payload_thumb_url, :payload_url, :comments_count], :except => [:comments_count])
	          award[:resource_type] = "award"
	          award[:views_count] = video.views_count
              award[:comments_count] = comments.count + videoComments.count
	          award[:creator] = creator.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
	          award[:created_to_user] = created_to_user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
	          award[:facebook_share_url] = user_award_url(award.user_id, award.id)
	          @results << award if !award[:video].nil?
	        end
	
	        interests = Interest.where("video_id IS NOT NULL").order('created_at DESC').page(1).per(30)
	        interests.each do |interest|
	          video = Video.where(id: interest.video_id).first
	          creator = User.where(id: interest.created_by_id).first
	          created_to_user = User.where(id: interest.user_id).first
	          videoComments = CompetitionVideoComment.where(video_id: video.id)
	          comments = Comment.where(video_id: video.id)

	          interest[:video] = video.as_json(:methods => [:payload_mobile_url, :payload_thumb_url, :payload_url, :comments_count], :except => [:comments_count])
	          interest[:resource_type] = "interest"
	          interest[:facebook_share_url] = user_interest_url(interest.user_id, interest.id)
	          interest[:views_count] = video.views_count
              interest[:comments_count] = comments.count + videoComments.count
	          interest[:creator] = creator.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
	          interest[:created_to_user] = created_to_user.as_json(:methods => [:avatar_url],:except => [:authentication_token, :email])
	          @results << interest if !interest[:video].nil?
	        end

			@results.sort_by { |hsh| hsh[:created_at] }.reverse

	        respond_to do |format|
      			format.html
    		end
	end

	  def get_video_for_result(result)
    	video = Video.where(id: result.video_id)
    	result[:video] = video[0].to_json(:methods => [:payload_url, :payload_mobile_url, :payload_thumb_url])
    	if video.exists?
      		pre_vid = JSON.parse(result[:video])
      		return pre_vid["payload_mobile_url"]
    	else
      		return
    	end
  	  end

  	  private
  	      def require_login
		      unless current_user
        		redirect_to new_user_registration_url, :alert => "Please login or signup"
      		  end
    	  end

end
