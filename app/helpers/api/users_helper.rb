module Api::UsersHelper
	def QualificationsStringForUserID(user_id)
		sup = []
      Profession.where(user_id: @user.id).each do | q |
        sup << q.job_title
      end

      Award.where(user_id: @user.id).each do |q|
        sup << q.name
      end

      Goal.where(user_id: @user.id).each do |q|
        sup << q.name
      end

      Interest.where(user_id: @user.id).each do |q|
        sup << q.name
        sup << q.description
      end

      Training.where(user_id: @user.id).each do |q|
        sup << q.name
        sup << q.description
      end

      Testimony.where(user_id: @user.id).each do |q|
        sup << q.name
        sup << q.description
      end
      
      return sup
	end

  def UsersJSONResponseForWithNoAuth(user)
    return user.as_json(:methods => [:avatar_url], :only => [:name, :email, :address, :birthday, :contact_number, :id, :is_following])
  end

  def titleForFollowingActivity(user1, user2, word)
    return user1.name + " " + word + " " + user2.name
  end

  def createActivity(controller, action, recipient, owner, parameters)
    controller.create_activity action: action, parameters: parameters, recipient: recipient, owner: owner
  end
end
