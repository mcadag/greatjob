require 'spec_helper'

describe "stored_company_cultures/new" do
  before(:each) do
    assign(:stored_company_culture, stub_model(StoredCompanyCulture,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new stored_company_culture form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stored_company_cultures_path, "post" do
      assert_select "input#stored_company_culture_name[name=?]", "stored_company_culture[name]"
    end
  end
end
