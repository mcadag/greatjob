class CreateCounters < ActiveRecord::Migration
  def change
    create_table :counters do |t|
      t.integer :nominee_id
      t.integer :votes

      t.timestamps
    end
  end
end
