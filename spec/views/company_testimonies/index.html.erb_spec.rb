require 'spec_helper'

describe "company_testimonies/index" do
  before(:each) do
    assign(:company_testimonies, [
      stub_model(CompanyTestimony,
        :name => "Name",
        :description => "Description",
        :user_id => 1,
        :company_id => 2,
        :video_id => 3,
        :created_by => 4,
        :allowed => false,
        :private => false,
        :permission => "Permission"
      ),
      stub_model(CompanyTestimony,
        :name => "Name",
        :description => "Description",
        :user_id => 1,
        :company_id => 2,
        :video_id => 3,
        :created_by => 4,
        :allowed => false,
        :private => false,
        :permission => "Permission"
      )
    ])
  end

  it "renders a list of company_testimonies" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Permission".to_s, :count => 2
  end
end
