require 'spec_helper'

describe "qualifications/edit" do
  before(:each) do
    @qualification = assign(:qualification, stub_model(Qualification))
  end

  it "renders the edit qualification form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", qualification_path(@qualification), "post" do
    end
  end
end
