require 'spec_helper'

describe "prizes/edit" do
  before(:each) do
    @prize = assign(:prize, stub_model(Prize,
      :name => "MyString",
      :amount => "",
      :details => "MyString",
      :competition_id => 1
    ))
  end

  it "renders the edit prize form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", prize_path(@prize), "post" do
      assert_select "input#prize_name[name=?]", "prize[name]"
      assert_select "input#prize_amount[name=?]", "prize[amount]"
      assert_select "input#prize_details[name=?]", "prize[details]"
      assert_select "input#prize_competition_id[name=?]", "prize[competition_id]"
    end
  end
end
