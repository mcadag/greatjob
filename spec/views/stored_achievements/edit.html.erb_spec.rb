require 'spec_helper'

describe "stored_achievements/edit" do
  before(:each) do
    @stored_achievement = assign(:stored_achievement, stub_model(StoredAchievement,
      :name => "MyString"
    ))
  end

  it "renders the edit stored_achievement form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stored_achievement_path(@stored_achievement), "post" do
      assert_select "input#stored_achievement_name[name=?]", "stored_achievement[name]"
    end
  end
end
