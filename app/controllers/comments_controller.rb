class CommentsController < ApplicationController
  include CompetitionVideoCommentsHelper
  before_filter :all_comments, only: [:index, :create]
  respond_to :json, :html,:js

  def index
    @comments = Comment.all
    respond_to do |format|
      format.html
      format.json
    end
  end

  def show
    @comment = Comment.find(params[:id])
    render :json => @comment.as_json( :methods => [:video_mobile_url,:video_url,:video_thumb_url], 
                                       :success => true, 
                                       :status => 200)
  end 

  def new
    @comment = Comment.new
    respond_with(@comment)
  end

  def edit
  end

  def create
    @comment  = Comment.create(comment_params)
  end

  def update
    comment.update_attributes(params.require(:comment).permit!)
    render :json => comment.as_json(:status => 201, :message => "Updated"), status => 201
  end

  def destroy
    @comment.destroy
    render :json => {:message => "Deleted"}
  end

  private

    def all_comments
      @comments = Comment.all
    end
    
    def set_comment
      @comment = Comment.find(params[:id])
    end

    def comment_params
      params.require(:comment).permit(:candidate_id, :approved, :description, :details, :message, :nominee_id, :private, :user_id, :video_id, :video, :has_video)
    end

    def require_login
      unless current_user
        redirect_to new_user_registration_url, :alert => "Please login or signup"
      end
    end
  end